import {
    REGISTER_USER,
    REGISTER_SUCCESS, REGISTER_FAIL, OTP_SUCCESS, OTP_FAIL, OTP_USER, OTP_VERIFY_SUCCESS, OTP_VERIFY_USER, OTP_VERIFY_FAIL
} from "../actions/actionTypes";

const INITIAL_STATE = {
    response: '',
    isLoading: false,
    isSuccess: false,
  
}



export default (state = INITIAL_STATE, action) => {
    // alert(action.type)
    switch (action.type) {

        case REGISTER_SUCCESS:

            return { ...state, response: action.payload, isLoading: false, isSuccess: true, }

        case REGISTER_FAIL:
            return { ...state, response: action.payload, isLoading: false, isSuccess: false }

        case REGISTER_USER:

            return { ...state, response: '', isLoading: true, isSuccess: false }


       

        default:
            return state;

    }

};