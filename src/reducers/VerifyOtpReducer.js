import {
    OTP_VERIFY_SUCCESS, OTP_VERIFY_USER, OTP_VERIFY_FAIL
} from "../actions/actionTypes";

const INITIAL_STATE = {
    verifyOtpResponse: '',
    verifyOtpisSuccess: false,
    verifyOtpisLoading: false
}



export default (state = INITIAL_STATE, action) => {
    // alert(action.type)
    switch (action.type) {

        case OTP_VERIFY_SUCCESS:

            return { ...state, verifyOtpResponse: action.payload, verifyOtpisLoading: false, verifyOtpisSuccess: true, }

        case OTP_VERIFY_FAIL:
            return { ...state, verifyOtpResponse: action.payload, verifyOtpisLoading: false, verifyOtpisSuccess: false }

        case OTP_VERIFY_USER:

            return {
                ...state, verifyOtpResponse: '', verifyOtpisLoading: true, verifyOtpisSuccess: false,



            }


        default:
            return state;

    }

};