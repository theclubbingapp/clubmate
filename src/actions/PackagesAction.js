import {
    GET_PACKAGES_SUCCESS, GET_PACKAGES_FAIL, GET_PACKAGES,
  
  } from './actionTypes';
 
  import APIURLCONSTANTS from "../ApiUrlList";
  import axios from 'axios';
  const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  import ApiUtils from '../ApiUtils';
 
  
  export const getPackagesInit = () => {
    console.log('getPackagesInit')
    return {
      type: GET_PACKAGES,
      payload: true
    }
  };
  
  export const getPackagesSuccess = (response) => {
  console.log('getPackagesSuccess'+JSON.stringify(response))
    return {
      type: GET_PACKAGES_SUCCESS,
      payload: response
    }
  };
  
  export const getPAckagesFail = (error) => {
    console.log('getPAckagesFail'+error)
  
    return {
      type: GET_PACKAGES_FAIL,
      payload: error
    }
  };
  
  export function getPackages() {
    return function (dispatch) {
      dispatch(getPackagesInit())
      axios.post(APIURLCONSTANTS.GETPACKAGES, null, null)
        .then(ApiUtils.checkStatus)
        .then(res => dispatch(getPackagesSuccess(res.data.data)))
        .catch(error => {
          // alert(JSON.stringify(error))
          if (error.response) {
            // alert(JSON.stringify(error))
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            // console.log(error.response.data);
            // console.log(error.response.status);
            // console.log(error.response.headers);
            // alert(JSON.stringify(error.response)),
            dispatch(getPAckagesFail(error.response.data.message))
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            // console.log(error.request);
            dispatch(getPAckagesFail(error.request))
  
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
            dispatch(getPAckagesFail(error.message))
  
          }
          console.log(error.config);
          // dispatch(logInFailure(error))
        });
  
    }
  };
  
  
  