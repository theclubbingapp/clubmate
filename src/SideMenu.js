import PropTypes from "prop-types";
import React, { Component } from "react";

import { NavigationActions, DrawerActions, StackActions } from "react-navigation";
import {
  ScrollView,
  Image,
  Text,
  View, Alert,
  StyleSheet,
  Dimensions, LayoutAnimation,
  TouchableOpacity,
  Platform, AsyncStorage
} from "react-native";
import {
  connect
} from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  getStatusBarHeight,
  getBottomSpace,
  ifIphoneX,
  isIphoneX
} from "react-native-iphone-x-helper";
import { fonts, colors } from "./theme";
import applogo from "./assets/app_logo.png";
import friends from "./assets/friends.png";
import home from "./assets/home.png";
import logout from "./assets/logout.png";
import privacy from "./assets/privacy.png";
import profile from "./assets/my_profile.png";
import pairing from "./assets/pairing.png";
import pricing from "./assets/pricing.png";
import myentries from "./assets/myentries.png";
import brodcastlist from "./assets/brodcastlist.png";
import setting from "./assets/setting.png";
import contactus from "./assets/contactus.png";
import rateandshare from "./assets/rateandshare.png";
import key from "./assets/key.png";
import { logoutUser } from "./actions";
import keys from "./preference";
import firebase from 'react-native-firebase';
import EventEmitter from 'EventEmitter';
import APIURLCONSTANTS from './ApiUrlList'
import ApiUtils from './ApiUtils'
import axios from 'axios';
let topViewHeight = 0;
if (isIphoneX) {
  topViewHeight =
    (DEVICE_HEIGHT - (getBottomSpace() + getStatusBarHeight())) * 0.33;
} else {
  topViewHeight =
    Platform.OS == "ios"
      ? (DEVICE_HEIGHT - (getBottomSpace() + getStatusBarHeight())) * 0.33
      : DEVICE_HEIGHT * 0.33;
}

var CustomLayoutSpring = {
  duration: 400,
  create: {
    type: LayoutAnimation.Types.spring,
    property: LayoutAnimation.Properties.scaleXY,
    springDamping: 0.7,
  },
  update: {
    type: LayoutAnimation.Types.spring,
    springDamping: 0.7,
  },
};


var CustomLayoutLinear = {
  duration: 200,
  create: {
    type: LayoutAnimation.Types.linear,
    property: LayoutAnimation.Properties.opacity,
  },
  update: {
    type: LayoutAnimation.Types.curveEaseInEaseOut,
  },
};
let image = '';
class SideMenu extends Component {

  constructor(props) {

    super(props);
    this.state = {
      username: '',
      image: '',
      phone_number: '',
      mytoken: ''
    }
    this._emitter = new EventEmitter();
  }


  componentWillReceiveProps(nextProps) {
    // const image = global.profileImage;
    // if (image != null && image != '') {
    //   this.setState({
    //     image
    //   })
    // }
    // else {

    // }

    // this.getKeyReload();
    this.getImage();
  }



  getImage = () => {
    try {
      AsyncStorage.getItem(keys.token).then((value) => {
        this.setState({
          mytoken: value,
        })

        const formData = new FormData()
        formData.append('authtoken', value)
        console.log("token value sidemeny" + JSON.stringify(value))
        console.log('formData profile' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.GETPROFILE, formData, null)
          .then(ApiUtils.checkStatus)
          .then(res => {
            let mainData = res.data.data;
            if (mainData.image) {
              this.setState({
                image: mainData.image,
                username: mainData['full_name'],
                phone_number: mainData['phone_number']
              })
            }
            this.forceUpdate()
            // this._emitter.emit('reload');

            // if (mainData['full_name']) {
            //   this.setState({
            //     username: mainData['full_name']
            //   })
            // }
            // if (mainData['phone_number']) {
            //   this.setState({

            //   })
            // }

          })
          .catch(error => {
          });
      });
    } catch (err) {
      console.log("token getting" + JSON.stringify(err))

    }
  }

  navigateToScreen = route => () => {
    // LayoutAnimation.configureNext(CustomLayoutLinear);
    // LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

    const navigateAction = NavigationActions.navigate({
      routeName: route, params: { fromNavigator: true }
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.closeDrawer();
    // this.props.navigation.navigate('Profile',{ fromNavigator: true })
    // this.props.navigation.closeDrawer();
  };

  componentDidMount() {
    this._emitter.addListener('reload', this.handleAwesomeEvents, this);
    // this.getKey();
    this.getImage();
  }

  handleAwesomeEvents = (event) => {
    this.forceUpdate()
  };

  getKeyReload = async () => {
    console.log('key reload')
    try {
      const username = await AsyncStorage.getItem(keys.username);
      // const image = await AsyncStorage.getItem(keys.image);
      const phone_number = await AsyncStorage.getItem(keys.phone);
      this.setState({ username, phone_number });

    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  componentWillUnmount() {
    // this._emitter.removeAllListeners();
  }

  getKey = async () => {
    try {
      const username = await AsyncStorage.getItem(keys.username);
      const image = await AsyncStorage.getItem(keys.image);
      const phone_number = await AsyncStorage.getItem(keys.phone);
      this.setState({ username, image, phone_number });
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  navigateToPage = route => () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: route, params: { fromNavigator: true } })
      ]
    })
    this.props.navigation.dispatch(resetAction);
    this.props.navigation.closeDrawer();
  }

  logout = () => {
    console.log('logout')
    Alert.alert(
      'Logout',
      'Are you sure you want to logout?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        { text: 'Logout', onPress: () => AsyncStorage.clear(() => this.clearAll()) },
      ],
      { cancelable: true },
    );

  }

  clearAll = () => {
    this.storeItem(keys.isLoggedIn, 'false');
    if (Platform.OS != 'ios') {
      firebase.messaging().deleteToken();
    } else {
      firebase.iid().delete();
    }
    // firebase.iid().delete()
    this.props.logoutUser()
  }
  async storeItem(key, item) {
    let ref = this;
    try {
      //we want to wait for the Promise returned by AsyncStorage.setItem()
      //to be resolved to the actual value before returning the value
      var jsonOfItem = await AsyncStorage.setItem(key, item);
      return jsonOfItem;
    } catch (error) {
      console.log(error.message);
    }
  }


  navigateToScreenWithParam = route => () => {
    // LayoutAnimation.configureNext(CustomLayoutLinear);
    // LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    const navigateAction = NavigationActions.navigate({
      routeName: route,
      params: { fromNavigator: true }
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.closeDrawer();
  };

  shouldComponentUpdate(prevProps, prevState) {
    if (prevState.image == this.state.image) {
      return false
    }
    return true;
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View
            style={{
              ...ifIphoneX(
                {
                  paddingTop: getStatusBarHeight()
                  // paddingBottom: getBottomSpace(),
                },
                {
                  paddingTop: Platform.OS == "ios" ? getStatusBarHeight() : 0
                  // paddingBottom: getBottomSpace(),
                }
              )
            }}
          >
            <View
              style={{
                height: topViewHeight,
                justifyContent: "center",
                alignContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity onPress={this.navigateToScreenWithParam("MyProfile")} style={styles.iconsTouchable}>
                <Image
                  source={applogo}
                  style={styles.profilePic}
                  source={{ uri: this.state.image + '?t=' + Math.round(new Date().getTime() / 1000) }}
                // resizeMode="cover"
                />
                {/* <Image source={cross} style={styles.cross} resizeMode='contain'/> */}
              </TouchableOpacity>
              <View style={{ marginTop: DEVICE_HEIGHT * 0.2 * 0.08 }}>
                <Text
                  style={{
                    fontFamily: fonts.bold,
                    fontSize: DEVICE_HEIGHT * 0.03,
                    color: colors.white
                  }}
                >
                  {this.state.username}
                </Text>
              </View>
              <View style={{ marginTop: DEVICE_HEIGHT * 0.2 * 0.04 }}>
                <Text
                  style={{
                    fontFamily: fonts.normal,
                    fontSize: DEVICE_HEIGHT * 0.03 * 0.8,
                    color: colors.white
                  }}
                >
                  {this.state.phone_number}
                </Text>
              </View>
              <View />
            </View>
            <View style={{ paddingLeft: DEVICE_WIDTH * .05, paddingTop: 10 }}>
              <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.05) }}
                onPress={this.navigateToScreen("DashBoard")}>
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={home}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>Home</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.05) }}
                onPress={this.navigateToScreen("MyProfile")}>
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={profile}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>Profile</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.05) }}
                onPress={this.navigateToScreen("Friends")}
              >
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={friends}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>Clubbies</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.05) }}
                onPress={this.navigateToScreen("PairUp")}
              >
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={pairing}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>Pairing</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.05) }}
                onPress={this.navigateToScreen("Packages")}>
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={pricing}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>Pricing</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.05) }}
                onPress={this.navigateToScreen("Broadcast")}
              >
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={brodcastlist}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>Broadcast List</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity>
              {/* <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.05) }}
                onPress={this.navigateToScreen("BlockList")}
              >
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={friends}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>Block List</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity> */}
              <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.05) }}
                onPress={this.navigateToScreen("MyEntriesTab")}
              >
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={myentries}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>My Entries</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.05) }}
                onPress={this.navigateToScreen("PrivacyTerms")}>
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={privacy}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>Privacy/Terms</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.05) }}
                onPress={this.navigateToScreen("Settings")}>
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={setting}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>Settings</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.05) }}
                onPress={this.navigateToScreen("ContactUs")}>
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={contactus}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>Contact Us</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.05) }}
                onPress={this.navigateToScreen("RateShare")}>
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={rateandshare}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>Rate &amp; Share</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginBottom: (DEVICE_HEIGHT * 0.02) }}
                onPress={this.logout}>
                <View style={{ flexDirection: "row", alignContent: 'center', }}>
                  <Image
                    source={logout}
                    style={styles.navLogo}
                    resizeMode="contain"
                  />
                  <View style={{ marginLeft: DEVICE_WIDTH * .04, justifyContent: 'center', flexDirection: "column", height: DEVICE_HEIGHT * 0.03 }}>
                    <Text style={styles.navText}>Logout</Text>
                  </View>
                </View>
                <View style={{ marginLeft: (DEVICE_HEIGHT * 0.05) + 10, backgroundColor: colors.inputField, height: StyleSheet.hairlineWidth }}></View>
              </TouchableOpacity>


            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

// export default SideMenu;

const mapDispatchToProps = {
  // dispatchConfirmUserLogin: (number, password, deviceToken) => loginCall(number, password, deviceToken),
  logoutUser: () => logoutUser()
}

const mapStateToProps = state => ({
  profile: state.profile,
  login: state.login
})

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu)

const styles = StyleSheet.create({

  navText: {
    paddingTop: ((DEVICE_HEIGHT * 0.03) * .9) * .1,
    fontFamily: fonts.bold,
    fontSize: (DEVICE_HEIGHT * 0.03) * .9,
    color: colors.white
  },

  navLogo: {
    // backgroundColor:'black',
    tintColor: colors.white,
    alignSelf: 'center',
    height: DEVICE_HEIGHT * 0.03,
    width: DEVICE_HEIGHT * 0.03
  },
  iconsTouchable: {
    alignSelf: "center",
    alignContent: "center"
  },
  profilePic: {

    borderColor: colors.white,
    borderWidth: 5,
    height: DEVICE_HEIGHT * 0.12,
    width: DEVICE_HEIGHT * 0.12,
    borderRadius: (DEVICE_HEIGHT * 0.12) / 2,

    // width: DEVICE_HEIGHT * 0.14, height: DEVICE_HEIGHT * 0.14,
    // borderRadius: (DEVICE_HEIGHT * 0.14)/2
  },

  container: {
    flex: 1,
    backgroundColor: colors.primaryColor
  },
  navItemStyle: {
    padding: 10
  },
  navSectionStyle: {
    backgroundColor: "lightgrey"
  },
  sectionHeadingStyle: {
    paddingVertical: 10,
    paddingHorizontal: 5
  },
  footerContainer: {
    padding: 20,
    backgroundColor: "lightgrey"
  }
});
