const colors = {
  secondryColor: '#F6F6F6',
  primaryColor:"#593ad7",
  textColor:'#828282',
  white:'#ffffff',
  gray:'#d3d3d3',
  black:'#000',
  green:'green',
 
  
  
}

const fonts = {
  bold: 'Bariol-bold',
  light: 'Bariol-light',
  normal: 'Bariol',
}

export {
  colors,
  fonts
}