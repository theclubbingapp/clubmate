import React, { Component } from 'react';
import { Scene, Router } from 'react-native-router-flux';
import { View, StyleSheet } from "react-native";
import Splash from './components/screens/Splash';
import LoginScreen from "./components/screens/LoginScreen";
import RegisterScreen from "./components/screens/RegisterScreen";
import RegisterScreenSecond from "./components/screens/RegisterScreenSecond";
import IntroScreen from "./components/screens/IntroScreen";
import ForgotPassword from "./components/screens/ForgotPassword"
import ChangePasswordWithOtp from "./components/home/changePasswordWithOtp"
import OtpScreen from "./components/screens/OtpScreen"
import PrivacyTerms from "./components/home/PrivacyTerms"
import TermsPrivacy from "./components/home/TermsPrivacy"
import Addmembers from "./components/home/Addmembers"
const RouterComponent = () => {
	return (
		<Router
			navigationBarStyle={styles.navBar}>
			<Scene key="root" >
				<Scene key="Splash"
					component={Splash}
					hideNavBar={true}
					initial
					
				/>
				<Scene key="Addmembers"
					component={Addmembers}
					hideNavBar={true}
					
					
				/>
				<Scene
					key="IntroScreen"
					component={IntroScreen}
					hideNavBar={true}
				/>
				<Scene
					key="LoginScreen"
					component={LoginScreen}
					hideNavBar={true}
					
				/>
				<Scene
					key="ForgotPassword"
					component={ForgotPassword}
					hideNavBar={true}
				// initial
				/>
				<Scene
					key="ChangePasswordWithOtp"
					component={ChangePasswordWithOtp}
					hideNavBar={true}
				// initial
				/>

				<Scene
					key="RegisterScreen"
					component={RegisterScreen}
					hideNavBar={true}


				/>
				<Scene
					key="OtpScreen"
					component={OtpScreen}
					hideNavBar={true}
					
					
				// initial
				/>
				<Scene

					key="RegisterScreenSecond"
					component={RegisterScreenSecond}
					hideNavBar={true}

				/>
				<Scene

					key="PrivacyTerms"
					component={PrivacyTerms}
					hideNavBar={true}

				/>
				<Scene

					key="TermsPrivacy"
					component={TermsPrivacy}
					hideNavBar={true}

				/>
			</Scene>
		</Router>
	);
};


class RouterNav extends React.Component {
	render() {
		return (
			<RouterComponent />
		)
	}
}

const styles = StyleSheet.create({
	navBar: {
		backgroundColor: '#fff',//'#1a5fa2',
	},

});
export default RouterComponent; 