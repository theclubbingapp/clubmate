import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Text,
    TouchableOpacity,
    BackHandler,
    Keyboard,
    Alert,
    FlatList,
    AsyncStorage, ActivityIndicator,
    ScrollView,UIManager,LayoutAnimation,
    Platform,
    ImageBackground, KeyboardAvoidingView,
    Dimensions
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import {
    getStatusBarHeight,
    getBottomSpace,
    ifIphoneX
} from "react-native-iphone-x-helper";
import CardView from 'react-native-cardview'
import { fonts, colors } from "../../theme";
import * as Animatable from 'react-native-animatable';
const AnimatableFlatList = Animatable.createAnimatableComponent(FlatList);
import LoadingSpinner from '../common/loadingSpinner/index';
import LoadingViewUsers from "./loadingViewUsers";
import MyRegistrationItem from "./MyRegistrationItem";
import back from "../../assets/back.png";
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import DeviceInfo from 'react-native-device-info';
var CustomLayoutSpring = {
    duration: 400,
    create: {
        type: LayoutAnimation.Types.spring,
        property: LayoutAnimation.Properties.scaleXY,
        springDamping: 0.7,
    },
    update: {
        type: LayoutAnimation.Types.spring,
        springDamping: 0.7,
    },
};
class MyRegistration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // isFromDrawer: false,
            // avatarSource: null,
            data: '',
            isLoading: true,
            mytoken: '',
            // page: 1,
            refreshing: false,
        };
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentWillMount() {
        try {
            AsyncStorage.getItem(keys.token).then((value) => {
                console.log("token value" + JSON.stringify(value))
                this.setState({
                    mytoken: value,
                })

                let formData = new FormData()
                formData.append('authtoken', this.state.mytoken)
                formData.append('type', 'future')
                formData.append('timezone', DeviceInfo.getTimezone())
                this.setState({
                    isLoading: true
                })
                console.log('form dtaa' + JSON.stringify(formData))
                // console.log('current time' + JSON.stringify(DeviceInfo.getTimezone()))
                axios.post(APIURLCONSTANTS.GETALL_EVENT_BY_USERID, formData, null)
                    .then(ApiUtils.checkStatus)
                    .then(res => {
                        console.log('response dtaa' + JSON.stringify(res))
                        this.setState({
                            isLoading: false,

                        })
                        
                        if (res['data']['data'].length > 0) {
                            this.setState({
                                data: res['data']['data']
                            })
                        }

                    })
                    .catch(error => {
                        console.log('response error' + JSON.stringify(error))
                        this.setState({
                            isLoading: false
                        })

                    });

            });
        } catch (err) {
            console.log("token getting" + JSON.stringify(err))
            this.setState({ mytoken: '' })
        }


    }

    componentDidMount() {


    }

    componentDidUpdate() { }


    goToProfile = (item, index) => {
        if (item.group_type == 'Group') {
            const { navigation } = this.props;
            // navigation.navigate('OtherProfileDetails');
            this.props.screenProps.navigate('AddMembers', {
                members: item['mambers'],
                date: item.date,
                time: item.time,
                isFromDrawer: true,
                authtoken: this.state.mytoken,
                registration_id: item['event_id'],
                group_type: item.group_type,
                placeId: item['club_place_id'],
                from:'future'

            })
        }
        // else if (item['mambers'].length > 0) {
        //     const { navigation } = this.props;
        //     // navigation.navigate('OtherProfileDetails');
        //     navigation.navigate('AddMembers', {
        //         members: item['mambers'],
        //         isFromDrawer: true,
        //         authtoken: this.state.mytoken,
        //         registration_id: item['event_id'],
        //         group_type: item.group_type,
        //         placeId: item['club_place_id'],

        //     })
        // }
    }


    onClickBack = () => {
        this.props.navigation.goBack();
    };

    openDrawer = () => {
        this.props.navigation.openDrawer();
    };


    renderItem = ({ item, index }) => {
        return (
            <MyRegistrationItem
                myToken={this.state.mytoken}
                navigation={this.props.navigation}
                item={item}
                index={index}
                // onPress={this.goToProfile}
                onPress={() => this.goToProfile(item, index)}
                onLongPress={() => this.onLongPress(item, index)}

            />

        )

    }

    deleteItem = (item, index) => {
        let formData = new FormData();
        formData.append('authtoken', this.state.mytoken)
        formData.append('event_id', item.event_id)
        console.log('send otp' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.DELETE_ENTRY, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('resgestration otp' + JSON.stringify(res))
                LayoutAnimation.configureNext(CustomLayoutSpring);
                this.setState(state => {
                    const list = state.data.filter((item, j) => index !== j);
                    return {
                        data: list,
                    };
                });
            })
            .catch(error => {
                alert('Something went wrong!!')
                console.log('Something otp' + JSON.stringify(error))

            });
    }

    onLongPress = (item, index) => {
        Alert.alert(
            'Delete Entry',
            'Are you sure to delete this entry?',
            [
                {
                    text: 'Delete', onPress: () => {
                        this.deleteItem(item, index)
                    },

                },
                {
                    text: 'Cancel', onPress: () => {

                    },

                }
            ],
            { cancelable: false },
        );
    }
    renderPaginationFetchingView = () => (
        <LoadingViewUsers>
        </LoadingViewUsers>
    )

    onPaginate = () => {

        this.retriveNewsData()
    }

    retriveNewsData = () => {
        let mobileRecharge = ['Mobile Recharge', 'option 2', 'option 1', 'option 2', 'option 1', 'option 2', 'option 1', 'option 2', 'option 1', 'option 2'];
        this.setState({
            refreshing: false,
            isLoading: false,

        })
        this.setState({ mobileRecharge: [...this.state.mobileRecharge, ...mobileRecharge] }, () => {
        })
    }
    onEndReached = (isLoading) => {
        if (!isLoading) {
            this.setState({
                isLoading: true,
            })
            setTimeout(() => {
                this.onPaginate();
            }, 2000);

        }

    }

    paginationWaitingView = () => {
        return (
            <View style={styles.paginationView}>
                <LoadingSpinner height={DEVICE_HEIGHT * .03} width={DEVICE_WIDTH} text="Loading ..." />
            </View>
        )
    }
    renderFooter = (isLoading) => {
        if (this.state.page == 1 && isLoading) {

            return this.renderPaginationFetchingView()
        } else if (isLoading) {
            return this.paginationWaitingView()
        }
        else if (
            this.state.page >= this.state.allPages
        ) {
            return this.paginationAllLoadedView()
        }

        return null
    }

    onRefresh = () => {
        let formData = new FormData()
        formData.append('authtoken', this.state.mytoken)
        formData.append('type', 'future')
        formData.append('timezone', DeviceInfo.getTimezone())
        this.setState({
            refreshing: true,
        })
        console.log('form dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.GETALL_EVENT_BY_USERID, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('response dtaa' + JSON.stringify(res))
                this.setState({
                    refreshing: false,

                })
                if (res['data']['data'].length > 0) {
                    this.setState({
                        data: res['data']['data']
                    })
                }

            })
            .catch(error => {
                console.log('response error' + JSON.stringify(error))
                this.setState({
                    refreshing: false
                })

            });
    }

    retry = () => {
        let formData = new FormData()
        formData.append('authtoken', this.state.mytoken)
        formData.append('type', 'future')
        formData.append('timezone', DeviceInfo.getTimezone())
        this.setState({
            isLoading: true,
            data: ''
        })
        console.log('form dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.GETALL_EVENT_BY_USERID, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('response dtaa' + JSON.stringify(res))
                this.setState({
                    isLoading: false,

                })
                if (res['data']['data'].length > 0) {
                    this.setState({
                        data: res['data']['data']
                    })
                }

            })
            .catch(error => {
                console.log('response error' + JSON.stringify(error))
                this.setState({
                    isLoading: false
                })

            });
    }

    render() {
        return (
            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                
                <View style={{
                    flex: 1, ...ifIphoneX({
                        paddingBottom: getBottomSpace()
                    }, {
                            paddingBottom: getBottomSpace()

                        })
                }}>
                    {this.state.isLoading ?

                        <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                            <ActivityIndicator
                                style={{ alignSelf: 'center' }}
                                size={'large'}
                                color={colors.primaryColor}
                            ></ActivityIndicator>
                        </View>
                        :

                        <View style={{ flex: 1 }}>
                            {this.state.data == '' ?
                                <View style={{
                                    flex:1,
                                    alignSelf: 'center',
                                    justifyContent: 'center',
                                    alignSelf: 'center'
                                }}>
                                    <TouchableOpacity style={{ alignSelf: 'center' }} onPress={this.retry}>
                                        <Text  allowFontScaling={false} style={{ alignSelf: 'center', color: colors.primaryColor, fontSize: DEVICE_HEIGHT * .02 }}>
                                            No upcoming event found!
                                       </Text>
                                        <Text  allowFontScaling={false} style={{alignSelf: 'center',  marginTop: DEVICE_HEIGHT * .02, color: colors.primaryColor, fontSize: DEVICE_HEIGHT * .04 }}>
                                            REFRESH
                                  </Text>
                                    </TouchableOpacity>
                                </View>
                                :

                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={{ paddingBottom: 10, paddingTop: 10 }}
                                    ref={(ref) => { this.listView = ref; }}
                                    data={this.state.data}
                                    keyExtractor={(item, index) => `${index} - ${item}`}
                                    renderItem={this.renderItem}
                                    extraData={this.state}
                                    ListFooterComponent={() => this.renderFooter(this.state.isLoading)}
                                    // onEndReachedThreshold={0.1}
                                    // onEndReached={() => this.onEndReached(this.state.isLoading)}
                                    numColumns={1}
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                                />
                            }


                        </View>


                    }


                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({


    paginationView: {
        flex: 0,
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT * .05,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.1 + 20 : DEVICE_HEIGHT * 0.1,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        justifyContent: "center",
        // marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03
    },




    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.03,
        width: DEVICE_WIDTH * 0.03
    },

    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03,
        alignSelf: "center",
        alignContent: "center"
    }
});
const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode)
};

const mapStateToProps = state => ({
    login: state.login
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyRegistration);
