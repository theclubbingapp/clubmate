import React, { Component } from 'react';
import { createStackNavigator, createDrawerNavigator, createAppContainer, createMaterialTopTabNavigator } from 'react-navigation';
import { DrawerActions } from 'react-navigation';
import { View, Text, StyleSheet, Platform, TouchableOpacity, Image, StatusBar } from 'react-native';

import SentRequest from './SentRequest'
import ReceivedRequest from './ReceivedRequest'
import MyFriendList from './MyFriendList'
import OtherProfileDetails from './OtherProfileDetails'

import { fonts, colors } from "../../theme";


const StackNaviApp = createStackNavigator({
    OtherProfileDetails: { screen: OtherProfileDetails},
    Tabs: {
        screen: createMaterialTopTabNavigator({
            Sent: SentRequest,
            Received: ReceivedRequest,
            Friends: MyFriendList

        }, {
                tabBarOptions: {
                    activeTintColor: colors.primaryColor,
                    inactiveTintColor: colors.white,
                    style: {
                        backgroundColor: colors.gray,
                    },
                    indicatorStyle: {
                        backgroundColor: colors.primaryColor,
                    },
                }
            })
    }
},
    { initialRouteName: 'Tabs',headerMode: 'none', });
// export default StackNaviApp;
const App = createAppContainer(StackNaviApp);

class CustomNavigator extends React.Component {
    static router = StackNaviApp.router;
    render() {
      const { navigation } = this.props;
  
      return <StackNaviApp navigation={navigation} />;
    }
  }

// const Tabs = createMaterialTopTabNavigator({
//     Sent: SentRequest,
//     Received: ReceivedRequest,
//     Friends: MyFriendList
// }, {
//         tabBarOptions: {
//             activeTintColor: colors.primaryColor,
//             inactiveTintColor: colors.white,
//             style: {
//                 backgroundColor: colors.gray,
//             },
//             indicatorStyle: {
//                 backgroundColor: colors.primaryColor,
//             },
//         }
//     });



export default App; 