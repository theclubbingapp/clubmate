import React, { Component } from 'react';
import {
    Platform,
    Text, ScrollView,
    View, FlatList,
    TextInput, Animated, TouchableWithoutFeedback,
    StyleSheet, ImageBackground,
    TouchableOpacity, TouchableHighlight,
    ActivityIndicator,
    Image,
    Modal, Dimensions
} from 'react-native';

const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import { fonts, colors } from '../../theme'
import CardView from 'react-native-cardview'
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import moment from 'moment'
export default class MyRegistrationItem extends Component {
    state = {
        name: '',
        otherMembers: ['+99999999', '+9999999999', '+99999999999', '+99999999999', '+99999999999']
    }
    componentDidMount() {
        this.callPlaceDetails();
    }

    callPlaceDetails = () => {
        this.setState({
            isLoading: true
        })
        console.log('place details' + 'Strt')
        var Url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + this.props.item['club_place_id'] + "&fields=address_component,adr_address,alt_id,formatted_address,geometry,icon,id,name,permanently_closed,photo,place_id,plus_code,scope,type,url,user_ratings_total,utc_offset,vicinity&key=" + APIURLCONSTANTS.KEY + "&sessiontoken=" + this.props.myToken
        axios.get(Url)
            .then(res => {
                this.setState({
                    isLoading: false
                })
                console.log('place details' + JSON.stringify(res))
                if (res['data']['result'] != null && res['data']['result'] != undefined && res['data']['result'] != '') {
                    this.setState({
                        name: res['data']['result']['name'],
                    })
                    this.setState({
                        image: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + res['data']['result']['photos'][0]['photo_reference'] + "&key=" + APIURLCONSTANTS.KEY
                    })
                    

                }
            }).catch(err => {
                console.log('place details' + JSON.stringify(err))
                // this.refs.loading.close();
                this.setState({
                    isLoading: false
                })
            });
    }
    render() {
        const { navigation, item, index, onPress,onLongPress } = this.props;
        let _this = this;
        let date=moment(item.date, ["DD-MM-YYYY"]).format("ddd DD-MMM-YYYY")
        let time=moment(item.time, ["HH:mm"]).format("hh:mm A")
        let isPairup=''
        
        if(item['user_pairups'].length > 0){
            isPairup='Paired'
          }else{
            isPairup='Not-Paired'
          }
        
        // }
        return (
            <TouchableOpacity onLongPress={() => onLongPress(item, index)}  onPress={() => onPress(item, index)} style={[styles.touchable, { marginTop: index > 0 ? 10 : 0 }]}>
                <CardView
                    style={{ backgroundColor: colors.white, }}
                    cardElevation={2}
                    cardMaxElevation={2}
                    cornerRadius={5}>
                    <View style={styles.touchableview}>
                        <View style={styles.imageMainView}>
                            <Image source={{ uri: this.state.image }}
                                style={styles.checkImage}
                            />
                        </View>
                        <View style={styles.numberMainView}>
                            <View style={{ flexDirection: 'column' }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text  allowFontScaling={false} numberOfLines={2} style={[styles.placeName, { flex: 10 }]}>{this.state.name}</Text>
                                    {item['group_type'] == 'Group' ?
                                        <Text  allowFontScaling={false} style={[styles.dateText, { paddingLeft: DEVICE_WIDTH * .02, color: colors.primaryColor, paddingRight: DEVICE_WIDTH * .02, flex: 2 }]}>Group</Text>
                                        :
                                        null
                                        // <Text style={[styles.dateText, { paddingLeft: DEVICE_WIDTH * .02, color: colors.primaryColor, paddingRight: DEVICE_WIDTH * .02, flex: 2 }]}>Single</Text>
                                    }
                                    
                                </View>
                                <View style={{ flexDirection: 'column', justifyContent: 'space-between' }}>
                                    <Text  allowFontScaling={false} style={styles.dateText}>{date}</Text>
                                    <Text  allowFontScaling={false} style={styles.timeText}>{time}</Text>
                                    <Text  allowFontScaling={false} style={[styles.timeText,{color: colors.primaryColor}]}>{isPairup}</Text>
                                    
                                </View>
                            </View>
                        </View>
                        {/* <View style={styles.rupeeMainView}>
                            <Text style={styles.rupeeText}>{(index % 2) == 0 ? "Pending" : "Resolved"}</Text>
                        </View> */}
                    </View>
                </CardView>
            </TouchableOpacity>

        );
    }
}
const styles = StyleSheet.create({

    est: {
        fontFamily: fonts.bold,
        color: colors.primaryColor,
        fontSize: (DEVICE_HEIGHT * .02) * .7,
    },

    touchable: {
        marginHorizontal: 10,
    },
    touchableview: {
        paddingVertical: DEVICE_HEIGHT * .02 * .3,
        paddingHorizontal: 5,
        flex: 1,
        flexDirection: 'row',
        // height: DEVICE_HEIGHT * .12,
        overflow: 'hidden',
        borderRadius: (DEVICE_HEIGHT * .12) / 30,
        width: DEVICE_WIDTH - 20
    },

    imageMainView: {
        // flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        height: DEVICE_HEIGHT * .12,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 5
        },
        elevation: 3
    },
    checkImage: {
        borderColor: colors.primaryColor,
        borderWidth: 2,
        height: DEVICE_HEIGHT * 0.12,
        width: DEVICE_HEIGHT * 0.12,
        borderRadius: (DEVICE_HEIGHT * 0.12) / 2,
        // borderRadius:DEVICE_HEIGHT*.10/2,
        // height: DEVICE_HEIGHT * .10,
        // width: DEVICE_HEIGHT * .10,
        alignSelf: 'center',

    },
    numberMainView: {
        paddingHorizontal: DEVICE_WIDTH * .02,
        alignContent: 'center',
        justifyContent: 'center',
        flex: 8,
        height: DEVICE_HEIGHT * .12,
    },
    numberText: {
        textTransform: 'capitalize',
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .7,
    },
    othermembers: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .02) * .6,
    },
    othermembersNum: {
        fontFamily: fonts.bold,
        color: colors.primaryColor,
        fontSize: (DEVICE_HEIGHT * .02) * .6,
    },
    dateText: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .6,
    },
    placeName: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .9,
    },
    timeText: {
        // marginLeft: DEVICE_WIDTH * .1,
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .6,
    },
    rupeeMainView: {
        backgroundColor: colors.blueColor,
        flex: 3,
        borderRadius: (DEVICE_HEIGHT * .04) / 2,
        height: (DEVICE_HEIGHT * .05) * .9,
        alignContent: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        // paddingRight: 10
    },
    rupeeText: {
        fontFamily: fonts.bold,
        alignSelf: 'center',
        color: colors.white,
        fontSize: DEVICE_WIDTH * .04,
    }

});
