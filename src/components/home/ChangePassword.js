import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Text,
    TouchableOpacity,
    BackHandler,
    Keyboard,
    Alert,
    FlatList,
    AsyncStorage,
    ScrollView,
    Platform, ActivityIndicator,
    ImageBackground, KeyboardAvoidingView,
    Dimensions
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import {
    getStatusBarHeight,
    getBottomSpace,
    ifIphoneX
} from "react-native-iphone-x-helper";
import CardView from 'react-native-cardview'
import { fonts, colors } from "../../theme";
import * as Animatable from 'react-native-animatable';
const AnimatableFlatList = Animatable.createAnimatableComponent(FlatList);
import LoadingSpinner from '../common/loadingSpinner/index';
import LoadingViewUsers from "./loadingViewUsers";
import UserListItem from "./userListItem";
import back from "../../assets/back.png";
import { TextInputLayout } from 'rn-textinputlayout';
import BlueButton from "../common/BlueButton";
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFromDrawer: false,
            avatarSource: null,
            mobileRecharge: [],
            isLoading: false,
            page: 1,
            refreshing: false,
            opassword: '',
            cpassword: '',
            password: '',
            mytoken: this.props.navigation.state.params['mytoken'],
        };
    }

    componentWillMount() {
        this.mounted = false

    }

    componentDidMount() {


    }

    componentDidUpdate() { }




    onClickBack = () => {
        this.props.navigation.goBack();
    };





    onPaginate = () => {

        this.retriveNewsData()
    }

    changePassword = () => {

        const { opassword, cpassword, password } = this.state;

        if (opassword == '') {
            alert('Please enter old password');
            return;
        }
        if (password == '') {
            alert('Please enter new password');
            return;
        }

        if (password != '') {
            if (password.length < 6) {
                alert('New password must be of six character');
                return;
            }
        }
        if (cpassword == '') {
            alert('Please enter confirm password');
            return;
        }
        if (cpassword != '') {
            if (opassword.length < 6) {
                alert('Confirm password must be of six character');
                return;
            }
        }
        if (cpassword != password) {

            alert('New password and Confirm password mismatch');
            return;

        }



        let formData = new FormData();
        formData.append('authtoken', this.state.mytoken)
        formData.append('password', opassword)
        formData.append('newpassword', password)
        formData.append('compassword', cpassword)
        this.setState({
            isLoading: true
        })
        console.log('form dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.CHANGE_PASSWORD, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('response dtaa' + JSON.stringify(res))
                this.setState({
                    isLoading: false,

                })
                alert('Password changed successfully')

            })
            .catch(error => {
                console.log('response error' + JSON.stringify(error))
                this.setState({
                    isLoading: false
                })
                alert(error['response']['data']['msg'])
            });


    }

    render() {
        return (
            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                <View style={styles.headerBackground}>
                    <View style={styles.dashBoardRow}>
                        {this.state.isFromDrawer ? (
                            <TouchableOpacity
                                onPress={this.openDrawer}
                                style={styles.menuTouch}
                            >
                                <Image
                                    source={menu}
                                    style={styles.menuIcon}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        ) : (
                                <TouchableOpacity
                                    onPress={this.onClickBack}
                                    style={styles.menuTouch}
                                >
                                    <Image
                                        source={back}
                                        style={styles.menuIcon}
                                        resizeMode="contain"
                                    />
                                </TouchableOpacity>
                            )}
                        <View style={styles.dashBoardView}>
                            <Text  allowFontScaling={false} style={styles.header}>Change Password</Text>
                        </View>
                        <TouchableOpacity style={styles.notiTouch}>
                            {/* <Image
                                source={list}
                                style={{
                                    tintColor: colors.white,
                                    height: DEVICE_HEIGHT * 0.05,
                                    width: DEVICE_WIDTH * 0.05
                                }}
                                resizeMode="contain"
                            /> */}
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{
                    alignItems: 'center', alignContent: 'center',
                    flex: 1, ...ifIphoneX({
                        paddingBottom: getBottomSpace()
                    }, {
                            paddingBottom: getBottomSpace()

                        })
                }}>

                    <View>

                        <TextInputLayout
                            hintColor={colors.gray}
                            errorColor={colors.black}
                            focusColor={colors.primaryColor}
                            style={styles.mainInputLayout}
                        >
                            <TextInput allowFontScaling={false} 
                                //   editable={!isAuthenticating} 
                                //   selectTextOnFocus={!isAuthenticating}
                                // keyboardType='phone-pad'
                                onChangeText={(opassword) => this.setState({ opassword: String.prototype.trim.call(opassword) })}
                                secureTextEntry={true}
                                value={this.state.opassword}
                                style={styles.inputLayout}
                                placeholder={"Old Password"}
                                autoCorrect={false}
                                autoCapitalize={"none"}
                                returnKeyType={"done"}
                                placeholderTextColor={colors.gray}
                                underlineColorAndroid="transparent"
                            />
                        </TextInputLayout>

                    </View>
                    <View>

                        <TextInputLayout
                            hintColor={colors.gray}
                            errorColor={colors.black}
                            focusColor={colors.primaryColor}
                            style={styles.mainInputLayout}
                        >
                            <TextInput allowFontScaling={false} 
                                //   editable={!isAuthenticating} 
                                //   selectTextOnFocus={!isAuthenticating}
                                // keyboardType='phone-pad'
                                onChangeText={(password) => this.setState({ password: String.prototype.trim.call(password) })}
                                secureTextEntry={true}
                                value={this.state.password}
                                style={styles.inputLayout}
                                placeholder={"New Password"}
                                autoCorrect={false}
                                autoCapitalize={"none"}
                                returnKeyType={"done"}
                                placeholderTextColor={colors.gray}
                                underlineColorAndroid="transparent"
                            />
                        </TextInputLayout>

                    </View>
                    <View>

                        <TextInputLayout
                            hintColor={colors.gray}
                            errorColor={colors.black}
                            focusColor={colors.primaryColor}
                            style={styles.mainInputLayout}
                        >
                            <TextInput allowFontScaling={false} 
                                //   editable={!isAuthenticating} 
                                //   selectTextOnFocus={!isAuthenticating}
                                // keyboardType='phone-pad'
                                onChangeText={(cpassword) => this.setState({ cpassword: String.prototype.trim.call(cpassword) })}
                                secureTextEntry={true}
                                value={this.state.cpassword}
                                style={styles.inputLayout}
                                placeholder={"Confirm Password"}
                                autoCorrect={false}
                                autoCapitalize={"none"}
                                returnKeyType={"done"}
                                placeholderTextColor={colors.gray}
                                underlineColorAndroid="transparent"
                            />
                        </TextInputLayout>

                    </View>
                    <View style={{ marginTop: DEVICE_HEIGHT * .05 }}>
                        {this.state.isLoading ?
                            <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .06 }}>
                                <ActivityIndicator color={colors.primaryColor} />
                            </View> :
                            <BlueButton
                                onPress={this.changePassword}
                                style={{
                                    backgroundColor: colors.primaryColor,
                                    width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                                }}
                                textStyles={{ fontSize: DEVICE_HEIGHT * .03*.9, color: colors.white }}>
                                SUBMIT
                                      </BlueButton>
                        }
                    </View>

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    mainInputLayout: {
        width: DEVICE_WIDTH * .85,

    },

    inputLayout: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.03*.9,
        height: DEVICE_HEIGHT * 0.06,
        color: colors.black
    },
    paginationView: {
        flex: 0,
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT * .05,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.1 + 20 : DEVICE_HEIGHT * 0.1,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        justifyContent: "center",
        // marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03
    },




    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.03,
        width: DEVICE_WIDTH * 0.03
    },

    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03,
        alignSelf: "center",
        alignContent: "center"
    }
});
const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode)
};

const mapStateToProps = state => ({
    login: state.login
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChangePassword);
