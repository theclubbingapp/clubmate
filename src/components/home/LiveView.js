import React, { Component, PureComponent } from 'react';
import {
  StyleSheet, Text, View, TouchableOpacity,
  Image, Dimensions, Modal, Platform, NativeModules
} from 'react-native';

import { RtcEngine, AgoraView } from 'react-native-agora';
import {
  APPID,
} from './utils';
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
  getStatusBarHeight,
  getBottomSpace,
  ifIphoneX
} from "react-native-iphone-x-helper";
import { fonts, colors } from "../../theme";
const { Agora } = NativeModules;
import other_profile from "../../assets/other_profile.png";
if (!Agora) {
  throw new Error("Agora load failed in react-native, please check ur compiler environments");
}
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";

const {
  FPS30,
  FixedLandscape,
  Host,
  AudioProfileDefault,
  AudioScenarioDefault,
} = Agora;

const BtnEndCall = () => require('../../assets/btn_endcall.png');
const BtnMute = () => require('../../assets/btn_mute.png');
const BtnSpeaker = () => require('../../assets/btn_speaker.png');
const BtnSwitchCamera = () => require('../../assets/btn_switch_camera.png');
const BtnVideo = () => require('../../assets/btn_video.png');
const EnableCamera = () => require('../../assets/enable_camera.png');
const DisableCamera = () => require('../../assets/disable_camera.png');
const EnablePhotoflash = () => require('../../assets/enable_photoflash.png');
const DisablePhotoflash = () => require('../../assets/disable_photoflash.png');
const IconMuted = () => require('../../assets/icon_muted.png');
const IconSpeaker = () => require('../../assets/icon_speaker.png');

const { width, height } = Dimensions.get('window');

const safeTop = (top) => (isIphoneX(Platform, width, height) ?
  (top + 88) :
  (isIphoneXR(Platform, width, height) ? (top + 64) : top)
);

const styles = StyleSheet.create({

  profileImage: {
    borderColor: colors.primaryColor,
    borderWidth: 2,
    height: DEVICE_HEIGHT * 0.2 * .7,
    width: DEVICE_HEIGHT * 0.2 * .7,
    borderRadius: (DEVICE_HEIGHT * 0.2) * .7 / 2,
    // borderRadius:DEVICE_HEIGHT*.10/2,
    // height: DEVICE_HEIGHT * .10,
    // width: DEVICE_HEIGHT * .10,
    alignSelf: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: colors.gray
  },
  absView: {
    ...ifIphoneX({
      paddingBottom: getBottomSpace()
    }, {
        paddingBottom: getBottomSpace()

      }),
    backgroundColor: colors.gray, ...StyleSheet.absoluteFillObject,
    paddingTop: getStatusBarHeight(),
  },
  videoView: {
    padding: 5,
    flexWrap: 'wrap',
    flexDirection: 'row',
    zIndex: 100
  },
  localView: {
    flex: 1
  },
  remoteView: {
    width: (width - 40) / 3,
    height: (width - 40) / 3,
    margin: 5
  },
  bottomView: {
    width: DEVICE_WIDTH,
    alignSelf: 'center',
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  }
});

class OperateButton extends PureComponent {
  render() {
    const { onPress, source, style, imgStyle = { width: 50, height: 50 } } = this.props;
    return (
      <TouchableOpacity
        style={style}
        onPress={onPress}
        activeOpacity={.7}
      >
        <Image
          style={imgStyle}
          source={source}
        />
      </TouchableOpacity>
    )
  }
}

type Props = {
  channelProfile: Number,
  channelName: String,
  videoProfile: Number,
  clientRole: Number,
  // onCancel: Function,
  uid: Number,
  showVideo: boolean,
}

export default class AgoraComponent extends Component<Props> {
  state = {
    peerIds: [],
    joinSucceed: false,
    isSpeak: false,
    isMute: false,
    isCameraTorch: false,
    showVideo: false,
    hideButton: false,
    visible: false,
    selectedUid: undefined,
    myName: '',
    myImage: '',
    position: this.props.position,
  };

  componentWillMount() {
    console.log('this.props.channelProfile'+this.props.channelProfile)
    console.log('this.props.videoProfile'+this.props.videoProfile)
    console.log('this.props.clientRole'+this.props.clientRole)
    const config = {
      appid: '3036719609154a7caabf66b0b2988c3e',
      channelProfile: parseInt(this.props.channelProfile),
      videoProfile: this.props.videoProfile,
      clientRole: this.props.clientRole,
      videoEncoderConfig: {
        width: 360,
        height: 480,
        bitrate: 1,
        frameRate: FPS30,
        orientationMode: FixedLandscape,
      },
      clientRole: Host,
      audioProfile: AudioProfileDefault,
      audioScenario: AudioScenarioDefault
    };

    this.setState({
      myName: this.props.myName,
      myImage: this.props.myImage,
    })

    RtcEngine.on('firstRemoteVideoDecoded', (data) => {
      console.log('[RtcEngine] onFirstRemoteVideoDecoded', data);
    });
    RtcEngine.on('userJoined', (data) => {
      console.log('[RtcEngine] onUserJoined', data);
      const { peerIds } = this.state;
      if (peerIds.indexOf(data.uid) === -1) {
        this.setState({
          peerIds: [...peerIds, data.uid]
        })
      }
      console.log('insertCall length'+this.state.peerIds.length);
      if(this.state.peerIds.length>0){
        console.log('insertCall position'+this.state.position);
        if(this.state.position!='left'){
          console.log('insertCall left');
          this.insertCall()
        }
      }
    });
    RtcEngine.on('userOffline', (data) => {
      console.log('[RtcEngine] onUserOffline', data);
      this.setState({
        peerIds: this.state.peerIds.filter(uid => uid !== data.uid)
      })
    });
    RtcEngine.on('joinChannelSuccess', (data) => {
      console.log('[RtcEngine] onJoinChannelSuccess', data);
      RtcEngine.startPreview();
      this.setState({
        joinSucceed: true
      })
    });

    RtcEngine.on('audioVolumeIndication', (data) => {
      console.log('[RtcEngine] onAudioVolumeIndication', data);
    });
    RtcEngine.on('clientRoleChanged', (data) => {
      console.log("[RtcEngine] onClientRoleChanged", data);
    })
    RtcEngine.on('error', (data) => {
      if (data.error === 17) {
        RtcEngine.leaveChannel().then(_ => {
          RtcEngine.destroy();
          this.props.onCancel(data);
        });
      }
    })
    console.log("[CONFIG]", JSON.stringify(config));
    console.log("[CONFIG.encoderConfig", config.videoEncoderConfig);
    RtcEngine.init(config);
    RtcEngine.setEnableSpeakerphone(this.state.isSpeak);
  }

  componentDidMount() {
    RtcEngine.getSdkVersion((version) => {
      console.log('[RtcEngine] getSdkVersion', version);
    })

    console.log('[joinChannel] ' + this.props.channelName);
    RtcEngine.joinChannel(this.props.channelName, this.props.uid);
    RtcEngine.enableAudioVolumeIndication(500, 3);
  }

  insertCall = () => {
    console.log('insertCall api');
    let formData = new FormData()
    formData.append('friend_authtoken', this.props.authtoken)
    formData.append('authtoken', this.props.mytoken)
    console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
    axios.post(APIURLCONSTANTS.PREMIUM_INSERT_CALL, formData, null)
      .then(ApiUtils.checkStatus)
      .then(res => {
        

      })
      .catch(error => {
       
      });
  }


  componentWillUnmount() {
    if (this.state.joinSucceed) {
      RtcEngine.leaveChannel();
      RtcEngine.removeAllListeners();
      RtcEngine.destroy();
    }
  }

  handleCancel = () => {
    RtcEngine.leaveChannel();
    RtcEngine.removeAllListeners();
    RtcEngine.destroy();
    try {

      this.props.onCancel();
    }
    catch (e) {
      console.error(e.message);
    }

  }

  switchCamera = () => {
    RtcEngine.switchCamera();
  }

  toggleAllRemoteAudioStreams = () => {
    this.setState({
      isMute: !this.state.isMute
    }, () => {
      RtcEngine.muteLocalAudioStream(this.state.isMute);
    })
  }

  toggleSpeakerPhone = () => {
    this.setState({
      isSpeak: !this.state.isSpeak
    }, () => {
      // RtcEngine.setDefaultAudioRouteToSpeakerphone(this.state.isSpeak);
      RtcEngine.setEnableSpeakerphone(this.state.isSpeak);
    })
  }

  toggleCameraTorch = () => {
    this.setState({
      isCameraTorch: !this.state.isCameraTorch
    }, () => {
      RtcEngine.setCameraTorchOn(this.state.isCameraTorch).then(val => {
        console.log("setCameraTorch", val);
      })
    })
  }

  toggleVideo = () => {
    const showVideo = !this.state.showVideo
    this.setState({
      showVideo
    })
    if (showVideo) {
      RtcEngine.enableVideo()
      RtcEngine.startPreview()
    } else {
      RtcEngine.disableVideo()
      RtcEngine.stopPreview()
    }
  }

  toggleHideButtons = () => {
    this.setState({
      hideButton: !this.state.hideButton
    })
  }

  onPressVideo = (uid) => {
    this.setState({
      selectedUid: uid
    }, () => {
      this.setState({
        visible: true
      })
    })
  }

  buttonsView = ({ hideButton, isCameraTorch, showVideo, isMute, isSpeak }) => {
    console.log('is getting image' + this.state.myImage)
    if (!hideButton) {
      return (
        <View style={{
          width: DEVICE_WIDTH,
          position: 'absolute',
          ...ifIphoneX({
            bottom: getBottomSpace() + 20
          }, {
              bottom: getBottomSpace() + 20

            }),
        }}>

          <Text  allowFontScaling={false} style={{ marginTop: DEVICE_HEIGHT * .05, marginBottom: DEVICE_HEIGHT * .05, alignSelf: 'center', fontFamily: fonts.bold, fontSize: DEVICE_HEIGHT * .03, color: colors.primaryColor }}>{this.state.myName}</Text>
          <Text  allowFontScaling={false} style={{ marginBottom: DEVICE_HEIGHT * .05, alignSelf: 'center', fontFamily: fonts.bold, fontSize: DEVICE_HEIGHT * .03, color: colors.primaryColor }}>{this.state.peerIds.length > 0 ? 'Connected' : 'Connecting...'}</Text>
          {/* <OperateButton
            style={{ alignSelf: 'center', marginBottom: -10 }}
            onPress={this.handleCancel}
            imgStyle={{ width: 60, height: 60 }}
            source={BtnEndCall()}
          /> */}
          {/* <View style={styles.bottomView}> */}
          {/* <OperateButton
          onPress={this.toggleCameraTorch}
          imgStyle={{width: 40, height: 40}}
          source={isCameraTorch ? EnablePhotoflash() : DisablePhotoflash()}
        /> */}
          {/* <OperateButton
          onPress={this.toggleVideo}
          source={showVideo ? EnableCamera() : DisableCamera()}
        /> */}
          {/* </View> */}
          <View style={styles.bottomView}>
            <OperateButton
              onPress={this.toggleAllRemoteAudioStreams}
              source={isMute ? IconMuted() : BtnMute()}
            />
            {/* <OperateButton
          onPress={this.switchCamera}
          source={BtnSwitchCamera()}
        /> */}
            <OperateButton
              style={{ alignSelf: 'center', marginBottom: -10 }}
              onPress={this.handleCancel}
              imgStyle={{ width: 60, height: 60 }}
              source={BtnEndCall()}
            />
            <OperateButton
              onPress={this.toggleSpeakerPhone}
              source={isSpeak ? IconSpeaker() : BtnSpeaker()}
            />
          </View>
        </View>
      )
    }
  }

  agoraPeerViews = ({ visible, peerIds }) => {
    return (visible ?
      <View style={styles.videoView} /> :
      <View style={styles.videoView}>{
        peerIds.map((uid, key) => (
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => this.onPressVideo(uid)}
            key={key}>
            <AgoraView
              style={styles.remoteView}
              zOrderMediaOverlay={true}
              remoteUid={uid}
            />
          </TouchableOpacity>
        ))
      }</View>)
  }

  modalView = ({ visible }) => {
    return (
      <Modal
        visible={visible}
        presentationStyle={'fullScreen'}
        animationType={'slide'}
        onRequestClose={() => { }}
      >
        <TouchableOpacity
          activeOpacity={1}
          style={{ flex: 1 }}
          onPress={() => this.setState({
            visible: false
          })} >
          <AgoraView
            style={{ flex: 1 }}
            zOrderMediaOverlay={true}
            remoteUid={this.state.selectedUid}
          />
        </TouchableOpacity>
      </Modal>)
  }

  render() {
    if (!this.state.joinSucceed) {
      return (
        <View style={{ flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }}>
          <Text  allowFontScaling={false} >Connecting...</Text>
        </View>
      )
    }

    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={this.toggleHideButtons}
        style={styles.container}
      >
        {/* {this.state.showVideo ?
          <AgoraView style={styles.localView} showLocalVideo={this.state.showVideo} /> : null} */}
        <View style={styles.absView}>
          {/* <Text>channelName: {this.props.channelName}, peers: {this.state.peerIds.length}</Text> */}
          {/* {this.agoraPeerViews(this.state)} */}
          <View style={{ alignSelf: 'center', marginTop: DEVICE_HEIGHT / 3 }}>
            {this.state.myImage != '' ?
              <View >
                <Image source={{ uri: this.state.myImage }}
                  style={styles.profileImage}
                />
              </View>
              :
              <Image source={other_profile}
                style={[styles.profileImage]}
              />

            }
          </View>
          {this.buttonsView(this.state)}
        </View>
        {/* {this.modalView(this.state)} */}
      </TouchableOpacity>
    )
  }
}
