import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Text,
    TouchableOpacity, ActivityIndicator,
    BackHandler,
    Keyboard, RefreshControl,
    Alert,
    FlatList, Modal,
    AsyncStorage,
    ScrollView,
    Platform,
    ImageBackground, KeyboardAvoidingView,
    Dimensions
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import {
    getStatusBarHeight,
    getBottomSpace,
    ifIphoneX
} from "react-native-iphone-x-helper";
import { fonts, colors } from "../../theme";
import * as Animatable from 'react-native-animatable';
const AnimatableFlatList = Animatable.createAnimatableComponent(FlatList);
import back from "../../assets/back.png";
import chat from "../../assets/chat.png";
import block from "../../assets/block.png";
import other_profile from "../../assets/other_profile.png";
import BlueButton from "../common/BlueButton";
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
var ageCalculator = require('age-calculator');
let { AgeFromDateString, AgeFromDate } = require('age-calculator');
import ImageView from 'react-native-image-view';
import { CheckBox } from 'react-native-elements'
import list from "../../assets/list.png";
import FlashMessage from "react-native-flash-message";
class OtherProfileDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            event_id: this.props.navigation.state.params['event_id'],
            authtoken: this.props.navigation.state.params['authtoken'],
            mytoken: this.props.navigation.state.params['mytoken'],
            isFromDrawer: false,
            otherMembers: [],
            isLoading: false,
            response: '',
            maleFemale: '',
            phone_number: '',
            myPhoneNumber: '',
            myImage: '',
            friend_status: 0,
            pairup_status: 1,
            device_token: '',
            age: '',
            detailVisible: false,
            food: [],
            foodText: '',
            dance: [],
            danceText: '',
            typesOfDrink: [],
            typesOfDrinkText: '',
            intrestedIn: [],
            intrestedText: '',
            offeringADrink: [],
            requestingADrink: [],
            offering_a_drink: '',
            request_a_drink: '',
            bio: '',
            sendFriendRequsetLoading: false,
            acceptRequestLoading: false,
            rejectRequestLoading: false,
            removeFriendLoading: false,
            cancelRequestLoading: false,
            imagesToView: [],
            isImageViewVisible: false,
            otherUserImage: '',
            refreshing: false

        };
    }

    componentWillMount() {

        try {
            AsyncStorage.getItem(keys.phone).then((value) => {
                console.log("token value" + JSON.stringify(value))
                this.setState({
                    myPhoneNumber: value,
                })

            });
        } catch (err) {
            console.log("token getting" + JSON.stringify(err))
            this.setState({ myPhoneNumber: '' })
        }
        try {
            AsyncStorage.getItem(keys.image).then((value) => {
                console.log("token value" + JSON.stringify(value))
                this.setState({
                    myImage: value,
                })

            });
        } catch (err) {
            console.log("token getting" + JSON.stringify(err))
            this.setState({ myImage: '' })
        }


        this.mounted = false
        this.setState({
            isLoading: true
        })
        let formData = new FormData()
        formData.append('event_id', this.state.event_id)
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        console.log('form dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.OTHERUSER_PROFILE, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('response dtaa' + JSON.stringify(res))
                this.setState({
                    isLoading: false,
                })
                if (res['data']['data'] != '') {
                    this.setState({
                        response: res['data']['data']
                    })


                    if (res['data']['data']['mambers'] != null) {
                        this.setState({
                            otherMembers: res['data']['data']['mambers']
                        })
                    }

                    if (this.state.response != '') {
                        this.setState({
                            otherUserImage: this.state.response['image']
                        })
                    }
                    this.setState({
                        friend_status: res['data']['data']['friend_status'],
                        // pairup_status: res['data']['data']['pairUp_status'],
                        device_token: res['data']['data']['device_token'],

                    })


                    if (res['data']['data']['friend_status'] == 2 || res['data']['data']['friend_status'] == '2') {
                        console.log('Friend request is already sent')
                        this.refs.refProfiles.showMessage({
                            message: 'Friend request is already sent',
                            type: "info",
                            position: 'bottom'
                        });
                    } else if (res['data']['data']['friend_status'] == 4 || res['data']['data']['friend_status'] == '4') {
                        console.log('A friend request is already pending')
                        this.refs.refProfiles.showMessage({
                            message: 'A friend request is already pending',
                            type: "info",
                            position: 'bottom'
                        });
                    }


                    if (res['data']['data']['phone_number'] != null) {
                        this.setState({
                            phone_number: res['data']['data']['phone_number']
                        })
                    }
                    let gender = JSON.parse(res['data']['data']['gender']);
                    for (var i = 0; i < gender.length; i++) {
                        if (gender[i].isChecked) {
                            this.setState({
                                maleFemale: gender[i].value
                            })
                        }
                    }
                    if (res['data']['data']['food']) {
                        this.setState({
                            food: JSON.parse(res['data']['data']['food'])
                        })
                    }

                    if (this.state.food.length > 0) {
                        for (let i = 0; i < this.state.food.length; i++) {
                            if (this.state.food[i].isChecked) {
                                this.setState({
                                    foodText: this.state.food[i].value
                                })
                            }
                        }
                    }

                    if (res['data']['data']['dance']) {
                        this.setState({
                            dance: JSON.parse(res['data']['data']['dance'])
                        })
                    }
                    if (this.state.dance.length > 0) {
                        for (let i = 0; i < this.state.dance.length; i++) {
                            if (this.state.dance[i].isChecked) {
                                this.setState({
                                    danceText: this.state.dance[i].value == "Dance" ? 'Dancer' : 'Non-Dancer'
                                })
                            }
                        }
                    }

                    if (res['data']['data']['type_of_drink']) {
                        this.setState({
                            typesOfDrink: JSON.parse(res['data']['data']['type_of_drink'])
                        })
                    }




                    if (this.state.typesOfDrink.length > 0) {
                        let typeDrinkText = '';
                        for (let i = 0; i < this.state.typesOfDrink.length; i++) {
                            if (this.state.typesOfDrink[i].isChecked) {
                                if (i == 0) {
                                    typeDrinkText = this.state.typesOfDrink[i].value
                                } else if (i == this.state.typesOfDrink.length - 1) {

                                    if (typeDrinkText == '') {
                                        typeDrinkText = this.state.typesOfDrink[i].value
                                    } else {
                                        typeDrinkText = typeDrinkText + ", " + this.state.typesOfDrink[i].value
                                    }

                                } else {
                                    if (typeDrinkText == '') {
                                        typeDrinkText = this.state.typesOfDrink[i].value
                                    } else {
                                        typeDrinkText = typeDrinkText + ", " + this.state.typesOfDrink[i].value
                                    }

                                }
                            }
                        }
                        this.setState({
                            typesOfDrinkText: typeDrinkText
                        })

                    }

                    if (res['data']['data']['intrested_in']) {
                        this.setState({
                            intrestedIn: JSON.parse(res['data']['data']['intrested_in'])
                        })
                    }
                    if (this.state.intrestedIn.length > 0) {
                        let intrestedIn = '';
                        for (let i = 0; i < this.state.intrestedIn.length; i++) {
                            if (this.state.intrestedIn[i].isChecked) {
                                if (i == 0) {
                                    intrestedIn = this.state.intrestedIn[i].value
                                } else if (i == this.state.intrestedIn.length - 1) {
                                    if (intrestedIn == '') {
                                        intrestedIn = this.state.intrestedIn[i].value
                                    } else {
                                        intrestedIn = intrestedIn + ", " + this.state.intrestedIn[i].value
                                    }

                                } else {
                                    if (intrestedIn == '') {
                                        intrestedIn = this.state.intrestedIn[i].value
                                    } else {
                                        intrestedIn = intrestedIn + ", " + this.state.intrestedIn[i].value
                                    }

                                }


                            }
                        }
                        this.setState({
                            intrestedText: intrestedIn
                        })

                    }

                    if (res['data']['data']['offering_a_drink']) {
                        this.setState({
                            offeringADrink: JSON.parse(res['data']['data']['offering_a_drink'])
                        })
                    }

                    if (this.state.offeringADrink.length > 0) {
                        if (this.state.offeringADrink[0]['isChecked']) {
                            this.setState({
                                offering_a_drink: 'Offering Drinks'
                            })
                        }
                    }

                    if (this.state.offeringADrink.length > 1) {
                        if (this.state.offeringADrink[1]['isChecked']) {
                            this.setState({
                                request_a_drink: 'Requesting Drinks'
                            })
                        }
                    }

                    // for (let i = 0; i < this.state.offeringADrink.length; i++) {
                    //     if (this.state.offeringADrink[0]['isChecked']) {
                    //         this.setState({
                    //             offering_a_drink: 'Offerring a Drink'
                    //         })
                    //     }
                    //     if (this.state.offeringADrink[1]['isChecked']) {
                    //         this.setState({
                    //             offering_a_drink: 'Requesting a Drink'
                    //         })
                    //     }
                    // }
                    // if (res['data']['data']['request_a_drink']) {
                    //     this.setState({
                    //         requestingADrink: JSON.parse(res['data']['data']['request_a_drink'])
                    //     })
                    // }
                    if (res['data']['data']['bio']) {
                        this.setState({
                            bio: res['data']['data']['bio']
                        })
                    }


                    var days = String(res['data']['data'].dob).split('-');
                    let day = parseInt(days[0])
                    let month = parseInt(days[1]) - 1
                    let year = parseInt(days[2])
                    console.log('response dob' + JSON.stringify(year + '-' + month + '-' + day))
                    console.log('response response' + JSON.stringify(res['data']['data'].dob))
                    let ageFromString = new AgeFromDate(new Date(year, month, day)).age;
                    this.setState({
                        age: ageFromString + ' Years'
                    })

                    // if (this.state.friend_status == 2 || this.state.friend_status == '2') {
                    //     console.log('Friend request is already sent')
                    //     this.refs.refProfiles.showMessage({
                    //         message: 'Friend request is already sent',
                    //         type: "info",
                    //         position: 'bottom'
                    //     });
                    // } else if (this.state.friend_status == 4 || this.state.friend_status == '4') {
                    //     console.log('A friend request is already pending')
                    //     this.refs.refProfiles.showMessage({
                    //         message: 'A friend request is already pending',
                    //         type: "info",
                    //         position: 'bottom'
                    //     });
                    // }

                }



            })
            .catch(error => {
                console.log('response error' + JSON.stringify(error))
                this.setState({
                    isLoading: false
                })

            });
    }

    componentDidMount() {
        this.mounted = true
        console.log('componentDidMount')
        // if (this.state.friend_status == 2 || this.state.friend_status == '2') {
        //     console.log('Friend request is already sent')
        //     this.refs.refProfiles.showMessage({
        //         message: 'Friend request is already sent',
        //         type: "info",
        //         position: 'bottom'
        //     });
        // } else if (this.state.friend_status == 4 || this.state.friend_status == '4') {
        //     console.log('A friend request is already pending')
        //     this.refs.refProfiles.showMessage({
        //         message: 'A friend request is already pending',
        //         type: "info",
        //         position: 'bottom'
        //     });
        // }



    }

    componentDidUpdate() {

        // if (this.state.friend_status == 2 || this.state.friend_status == '2') {
        //     console.log('Friend request is already sent')
        //     this.refs.refProfiles.showMessage({
        //         message: 'Friend request is already sent',
        //         type: "info",
        //         position: 'bottom'
        //     });
        // } else if (this.state.friend_status == 4 || this.state.friend_status == '4') {
        //     console.log('A friend request is already pending')
        //     this.refs.refProfiles.showMessage({
        //         message: 'A friend request is already pending',
        //         type: "info",
        //         position: 'bottom'
        //     });
        // }
    }



    chat = () => {
        const { navigation } = this.props;
        // navigation.navigate('ChatScreen', { event_id:this.state.event_id,device_token: this.state.device_token, authtoken: this.state.authtoken, mytoken: this.state.mytoken, phone_number: this.state.phone_number, myPhoneNumber: this.state.myPhoneNumber, myImage: this.state.myImage, name: this.state.response['full_name'].toUpperCase() });
        //   return;
        if (this.state.friend_status == 1 || this.state.friend_status == '1') {
            navigation.navigate('ChatScreen', { from: 'profile', otherUserImage: this.state.otherUserImage, event_id: this.state.event_id, device_token: this.state.device_token, authtoken: this.state.authtoken, mytoken: this.state.mytoken, phone_number: this.state.phone_number, myPhoneNumber: this.state.myPhoneNumber, myImage: this.state.myImage, name: this.state.response['full_name'].toUpperCase() })

        } else {
            Alert.alert('Chat', 'Only friends can use this feature')
        }

    }

    refresh = () => {
        this.setState({
            refreshing: true,
            // response: ''
        })
        this.setState({
            isImageViewVisible: false,
        })
        let formData = new FormData()
        formData.append('event_id', this.state.event_id)
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        console.log('form dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.OTHERUSER_PROFILE, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                // console.log('response dtaa' + JSON.stringify(res))
                this.setState({
                    refreshing: false,
                })
                this.setState({
                    isImageViewVisible: false,
                })
                if (res['data']['data'] != '') {
                    this.setState({
                        response: res['data']['data']
                    })
                    if (res['data']['data']['mambers'] != null) {
                        this.setState({
                            otherMembers: res['data']['data']['mambers']
                        })
                    }
                    if (this.state.response != '') {
                        this.setState({
                            otherUserImage: this.state.response['image']
                        })
                    }
                    this.setState({
                        friend_status: res['data']['data']['friend_status'],
                        // pairup_status: res['data']['data']['pairUp_status'],
                        device_token: res['data']['data']['device_token'],

                    })
                    if (res['data']['data']['phone_number'] != null) {
                        this.setState({
                            phone_number: res['data']['data']['phone_number']
                        })
                    }
                    let gender = JSON.parse(res['data']['data']['gender']);
                    for (var i = 0; i < gender.length; i++) {
                        if (gender[i].isChecked) {
                            this.setState({
                                maleFemale: gender[i].value
                            })
                        }
                    }
                    if (res['data']['data']['food']) {
                        this.setState({
                            food: JSON.parse(res['data']['data']['food'])
                        })
                    }

                    if (this.state.food.length > 0) {
                        for (let i = 0; i < this.state.food.length; i++) {
                            if (this.state.food[i].isChecked) {
                                this.setState({
                                    foodText: this.state.food[i].value
                                })
                            }
                        }
                    }

                    if (res['data']['data']['dance']) {
                        this.setState({
                            dance: JSON.parse(res['data']['data']['dance'])
                        })
                    }
                    if (this.state.dance.length > 0) {
                        for (let i = 0; i < this.state.dance.length; i++) {
                            if (this.state.dance[i].isChecked) {
                                this.setState({
                                    danceText: this.state.dance[i].value == "Dance" ? 'Dancer' : 'Non-Dancer'
                                })
                            }
                        }
                    }

                    if (res['data']['data']['type_of_drink']) {
                        this.setState({
                            typesOfDrink: JSON.parse(res['data']['data']['type_of_drink'])
                        })
                    }



                    if (this.state.typesOfDrink.length > 0) {
                        let typeDrinkText = '';
                        for (let i = 0; i < this.state.typesOfDrink.length; i++) {
                            if (this.state.typesOfDrink[i].isChecked) {
                                if (i == 0) {
                                    typeDrinkText = this.state.typesOfDrink[i].value
                                } else if (i == this.state.typesOfDrink.length - 1) {
                                    typeDrinkText = typeDrinkText + ", " + this.state.typesOfDrink[i].value
                                } else {
                                    typeDrinkText = typeDrinkText + ", " + this.state.typesOfDrink[i].value
                                }


                            }
                        }
                        this.setState({
                            typesOfDrinkText: typeDrinkText
                        })

                    }

                    if (res['data']['data']['intrested_in']) {
                        this.setState({
                            intrestedIn: JSON.parse(res['data']['data']['intrested_in'])
                        })
                    }
                    if (this.state.intrestedIn.length > 0) {
                        let intrestedIn = '';
                        for (let i = 0; i < this.state.intrestedIn.length; i++) {
                            if (this.state.intrestedIn[i].isChecked) {
                                if (i == 0) {
                                    intrestedIn = this.state.intrestedIn[i].value
                                } else if (i == this.state.intrestedIn.length - 1) {
                                    intrestedIn = intrestedIn + ", " + this.state.intrestedIn[i].value
                                } else {
                                    intrestedIn = intrestedIn + ", " + this.state.intrestedIn[i].value
                                }


                            }
                        }
                        this.setState({
                            intrestedText: intrestedIn
                        })

                    }

                    if (res['data']['data']['offering_a_drink']) {
                        this.setState({
                            offeringADrink: JSON.parse(res['data']['data']['offering_a_drink'])
                        })
                    }



                    if (this.state.offeringADrink.length > 0) {
                        if (this.state.offeringADrink[0]['isChecked']) {
                            this.setState({
                                offering_a_drink: 'Offering Drinks'
                            })
                        }
                    }

                    if (this.state.offeringADrink.length > 1) {
                        if (this.state.offeringADrink[1]['isChecked']) {
                            this.setState({
                                request_a_drink: 'Requesting Drinks'
                            })
                        }
                    }
                    // if (res['data']['data']['request_a_drink']) {
                    //     this.setState({
                    //         requestingADrink: JSON.parse(res['data']['data']['request_a_drink'])
                    //     })
                    // }
                    if (res['data']['data']['bio']) {
                        this.setState({
                            bio: res['data']['data']['bio']
                        })
                    }


                    var days = String(res['data']['data'].dob).split('-');
                    let day = parseInt(days[0])
                    let month = parseInt(days[1]) - 1
                    let year = parseInt(days[2])
                    console.log('response dob' + JSON.stringify(year + '-' + month + '-' + day))
                    console.log('response response' + JSON.stringify(res['data']['data'].dob))
                    let ageFromString = new AgeFromDate(new Date(year, month, day)).age;
                    this.setState({
                        age: ageFromString + ' Years'
                    })

                }

            })
            .catch(error => {
                console.log('response error' + JSON.stringify(error))
                this.setState({
                    refreshing: false
                })

            });
    }

    retry = () => {
        this.setState({
            isLoading: true,
            response: ''
        })
        let formData = new FormData()
        formData.append('event_id', this.state.event_id)
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        console.log('form dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.OTHERUSER_PROFILE, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                // console.log('response dtaa' + JSON.stringify(res))
                this.setState({
                    isLoading: false,
                })
                if (res['data']['data'] != '') {
                    this.setState({
                        response: res['data']['data']
                    })
                    if (res['data']['data']['mambers'] != null) {
                        this.setState({
                            otherMembers: res['data']['data']['mambers']
                        })
                    }
                    if (this.state.response != '') {
                        this.setState({
                            otherUserImage: this.state.response['image']
                        })
                    }
                    this.setState({
                        friend_status: res['data']['data']['friend_status'],
                        // pairup_status: res['data']['data']['pairUp_status'],
                        device_token: res['data']['data']['device_token'],

                    })
                    if (res['data']['data']['phone_number'] != null) {
                        this.setState({
                            phone_number: res['data']['data']['phone_number']
                        })
                    }
                    let gender = JSON.parse(res['data']['data']['gender']);
                    for (var i = 0; i < gender.length; i++) {
                        if (gender[i].isChecked) {
                            this.setState({
                                maleFemale: gender[i].value
                            })
                        }
                    }
                    if (res['data']['data']['food']) {
                        this.setState({
                            food: JSON.parse(res['data']['data']['food'])
                        })
                    }

                    if (this.state.food.length > 0) {
                        for (let i = 0; i < this.state.food.length; i++) {
                            if (this.state.food[i].isChecked) {
                                this.setState({
                                    foodText: this.state.food[i].value
                                })
                            }
                        }
                    }

                    if (res['data']['data']['dance']) {
                        this.setState({
                            dance: JSON.parse(res['data']['data']['dance'])
                        })
                    }
                    if (this.state.dance.length > 0) {
                        for (let i = 0; i < this.state.dance.length; i++) {
                            if (this.state.dance[i].isChecked) {
                                this.setState({
                                    danceText: this.state.dance[i].value == "Dance" ? 'Dancer' : 'Non-Dancer'
                                })
                            }
                        }
                    }

                    if (res['data']['data']['type_of_drink']) {
                        this.setState({
                            typesOfDrink: JSON.parse(res['data']['data']['type_of_drink'])
                        })
                    }



                    if (this.state.typesOfDrink.length > 0) {
                        let typeDrinkText = '';
                        for (let i = 0; i < this.state.typesOfDrink.length; i++) {
                            if (this.state.typesOfDrink[i].isChecked) {
                                if (i == 0) {
                                    typeDrinkText = this.state.typesOfDrink[i].value
                                } else if (i == this.state.typesOfDrink.length - 1) {
                                    typeDrinkText = typeDrinkText + ", " + this.state.typesOfDrink[i].value
                                } else {
                                    typeDrinkText = typeDrinkText + ", " + this.state.typesOfDrink[i].value
                                }


                            }
                        }
                        this.setState({
                            typesOfDrinkText: typeDrinkText
                        })

                    }

                    if (res['data']['data']['intrested_in']) {
                        this.setState({
                            intrestedIn: JSON.parse(res['data']['data']['intrested_in'])
                        })
                    }
                    if (this.state.intrestedIn.length > 0) {
                        let intrestedIn = '';
                        for (let i = 0; i < this.state.intrestedIn.length; i++) {
                            if (this.state.intrestedIn[i].isChecked) {
                                if (i == 0) {
                                    intrestedIn = this.state.intrestedIn[i].value
                                } else if (i == this.state.intrestedIn.length - 1) {
                                    intrestedIn = intrestedIn + ", " + this.state.intrestedIn[i].value
                                } else {
                                    intrestedIn = intrestedIn + ", " + this.state.intrestedIn[i].value
                                }


                            }
                        }
                        this.setState({
                            intrestedText: intrestedIn
                        })

                    }

                    if (res['data']['data']['offering_a_drink']) {
                        this.setState({
                            offeringADrink: JSON.parse(res['data']['data']['offering_a_drink'])
                        })
                    }

                    if (this.state.offeringADrink.length > 0) {
                        if (this.state.offeringADrink[0]['isChecked']) {
                            this.setState({
                                offering_a_drink: 'Offering Drinks'
                            })
                        }
                    }

                    if (this.state.offeringADrink.length > 1) {
                        if (this.state.offeringADrink[1]['isChecked']) {
                            this.setState({
                                request_a_drink: 'Requesting Drinks'
                            })
                        }
                    }
                    // if (res['data']['data']['request_a_drink']) {
                    //     this.setState({
                    //         requestingADrink: JSON.parse(res['data']['data']['request_a_drink'])
                    //     })
                    // }
                    if (res['data']['data']['bio']) {
                        this.setState({
                            bio: res['data']['data']['bio']
                        })
                    }


                    var days = String(res['data']['data'].dob).split('-');
                    let day = parseInt(days[0])
                    let month = parseInt(days[1]) - 1
                    let year = parseInt(days[2])
                    console.log('response dob' + JSON.stringify(year + '-' + month + '-' + day))
                    console.log('response response' + JSON.stringify(res['data']['data'].dob))
                    let ageFromString = new AgeFromDate(new Date(year, month, day)).age;
                    this.setState({
                        age: ageFromString + ' Years'
                    })

                }

            })
            .catch(error => {
                console.log('response error' + JSON.stringify(error))
                this.setState({
                    isLoading: false
                })

            });
    }

    onClickBack = () => {
        this.props.navigation.goBack();
    };

    openDrawer = () => {
        this.props.navigation.openDrawer();
    };

    viewImage = () => {
        this.setState({
            isImageViewVisible: true,
            imagesToView: [{
                source: {
                    uri: this.state.response['image']
                }
            }],

        })
    }

    sendFriendRequset = () => {
        let formData = new FormData()
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        this.setState({
            sendFriendRequsetLoading: true
        })
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.SEND_FRIEND_REQUEST, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    sendFriendRequsetLoading: false
                })
                console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))
                // alert('Friend request sent')
                this.setState({
                    friend_status: 2

                })
                Alert.alert('Friend Request', 'Friend request has been sent')
            })
            .catch(error => {
                this.setState({
                    sendFriendRequsetLoading: false
                })
                Alert.alert('Friend Request', error['response']['data']['msg'])
            });
    }
    acceptRequest = () => {
        let formData = new FormData()
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        this.setState({
            acceptRequestLoading: true
        })
        axios.post(APIURLCONSTANTS.ACCEPT_REQUEST, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    acceptRequestLoading: false
                })
                console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))
                this.setState({
                    friend_status: 1

                })

                Alert.alert('Friend Request', 'Friend request accepted')

            })
            .catch(error => {
                this.setState({
                    acceptRequestLoading: false
                })
                console.log('error SEND_FRIEND_REQUEST' + JSON.stringify(error))
                Alert.alert('Friend Request', error['response']['data']['msg'])
            });
    }
    rejectRequest = () => {
        let formData = new FormData()
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        this.setState({
            rejectRequestLoading: true
        })
        axios.post(APIURLCONSTANTS.REJECT_REQUEST, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    rejectRequestLoading: false
                })
                console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))
                this.setState({
                    friend_status: 2

                })
                Alert.alert('Friend Request', 'Friend request rejected')

            })
            .catch(error => {
                this.setState({
                    rejectRequestLoading: true
                })
                console.log('error SEND_FRIEND_REQUEST' + JSON.stringify(error))
                Alert.alert('Friend Request', error['response']['data']['msg'])
            });
    }
    cancelRequest = () => {
        let formData = new FormData()
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        this.setState({
            cancelRequestLoading: true
        })

        axios.post(APIURLCONSTANTS.CANCEL_FRIEND_REQUEST, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))
                this.setState({
                    cancelRequestLoading: false
                })
                Alert.alert('Friend Request', 'Friend request cancelled')
                // alert(res['data']['msg'])
                this.setState({
                    friend_status: 0

                })

            })
            .catch(error => {
                this.setState({
                    cancelRequestLoading: false
                })
                console.log('error SEND_FRIEND_REQUEST' + JSON.stringify(error))
                Alert.alert('Friend Request', error['response']['data']['msg'])
            });
    }

    removeFriend = () => {
        let formData = new FormData()
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        this.setState({
            removeFriendLoading: true
        })
        axios.post(APIURLCONSTANTS.DELETE_FRIEND, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))
                
                this.setState({
                    removeFriendLoading: false
                })
                Alert.alert('Friend Request', 'Friend has been removed')
                // alert(res['data']['msg'])
                this.setState({
                    friend_status: 0
                })

            })
            .catch(error => {
                this.setState({
                    removeFriendLoading: false
                })
                console.log('error SEND_FRIEND_REQUEST' + JSON.stringify(error))
                Alert.alert('Friend Request', error['response']['data']['msg'])
            });
    }

    blockUser = () => {
        Alert.alert(
            'Block',
            'Are you sure?',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'Block', onPress: () =>

                        this._block()
                },
            ],
            { cancelable: false },
        );
    }

    frinedDialog = () => {
        Alert.alert(
            'Info',
            'You need to be friend to use this feature',
            [
                {
                    text: 'Ok',
                    onPress: () => console.log('Cancel Pressed'),

                },
            ],
            { cancelable: false },
        );
    }


    _block = () => {
        let formData = new FormData()
        formData.append('authtoken', this.state.mytoken)
        formData.append('friend_authtoken', this.state.authtoken)
        console.log('_block dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.BLOCK_USER, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.props.navigation.goBack()
            })
            .catch(error => {
                Alert.alert('Block', error['response']['data']['msg'])
            });
    }
    pairUpRequest = () => {
        let formData = new FormData()
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        formData.append('event_id', this.state.event_id)
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.SEND_PAIRUP, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    pairup_status: 2
                })
                Alert.alert(res['data']['msg'])
            })
            .catch(error => {
                alert(error['response']['data']['msg'])
            });
    }
    pairUpAcceptRequest = () => {
        let formData = new FormData()
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        formData.append('event_id', this.state.event_id)
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.ACCEPT_PAIRUP, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    pairup_status: 1
                })
                alert(res['data']['msg'])
            })
            .catch(error => {
                alert(error['response']['data']['msg'])
            });
    }

    pairUpRejectRequest = () => {
        let formData = new FormData()
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        formData.append('event_id', this.state.event_id)
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.REJECT_PAIRUP, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    pairup_status: 2
                })
                alert(res['data']['msg'])
            })
            .catch(error => {
                alert(error['response']['data']['msg'])
            });
    }

    pairUpCancelRequest = () => {
        let formData = new FormData()
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        formData.append('event_id', this.state.event_id)
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.CANCEL_PAIRUP_RERQUEST, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))
                alert(res['data']['msg'])
                this.setState({
                    pairup_status: 0

                })

            })
            .catch(error => {
                console.log('error SEND_FRIEND_REQUEST' + JSON.stringify(error))
                alert(error['response']['data']['msg'])
            });
    }

    removePairUp = () => {
        let formData = new FormData()
        formData.append('friend_authtoken', this.state.authtoken)
        formData.append('authtoken', this.state.mytoken)
        formData.append('event_id', this.state.event_id)
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.DELETE_FRIEND, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))
                alert(res['data']['msg'])
                this.setState({
                    pairup_status: 0
                })

            })
            .catch(error => {
                console.log('error SEND_FRIEND_REQUEST' + JSON.stringify(error))
                alert(error['response']['data']['msg'])
            });
    }

    openDeatils = () => {
        this.setState({
            detailVisible: true,
            isImageViewVisible: false
        })
    }

    render() {
        let _this = this;
        return (
            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                <View style={styles.headerBackground}>
                    <View style={styles.dashBoardRow}>
                        {this.state.isFromDrawer ? (
                            <TouchableOpacity
                                onPress={this.openDrawer}
                                style={styles.menuTouch}
                            >
                                <Image
                                    source={menu}
                                    style={styles.menuIcon}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        ) : (
                                <TouchableOpacity
                                    onPress={this.onClickBack}
                                    style={styles.menuTouch}
                                >
                                    <Image
                                        source={back}
                                        style={styles.menuIcon}
                                        resizeMode="contain"
                                    />
                                </TouchableOpacity>
                            )}
                        <View style={styles.dashBoardView}>
                            {/* <Text style={[styles.header]}>{this.state.response != '' ? this.state.response['full_name'].toUpperCase() : ''}</Text> */}
                        </View>
                        <TouchableOpacity onPress={this.openDeatils} style={styles.notiTouch}>
                            {/* <Text style={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>Details</Text> */}
                            <Image
                                source={list}
                                style={{
                                    tintColor: colors.white,
                                    height: DEVICE_HEIGHT * 0.05,
                                    width: DEVICE_WIDTH * 0.05
                                }}
                                resizeMode="contain"
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={{
                        zIndex: 99, flex: 1, alignSelf: 'center'
                    }}>

                        {this.state.response['image'] != '' ?
                            <TouchableOpacity style={{
                                height: DEVICE_HEIGHT * 0.3,
                                width: DEVICE_HEIGHT * 0.3,
                                
                            }} onPress={this.viewImage}>
                                <Image source={{ uri: this.state.response['image'] }}
                                    style={[styles.profileImage]}
                                />
                            </TouchableOpacity>
                            :
                            <Image source={other_profile}
                                style={[styles.profileImage]}
                            />
                        }

                    </View>
                    {/* <View style={{ backgroundColor:'white',width: DEVICE_WIDTH, height: DEVICE_HEIGHT * .1, zIndex: 99, flex: 1, alignSelf: 'center', }}>

                       

                    </View> */}



                </View>


                {this.state.isLoading ?
                    <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                        <ActivityIndicator
                            style={{ alignSelf: 'center' }}
                            size={'large'}
                            color={colors.primaryColor}
                        ></ActivityIndicator>
                    </View>
                    :

                    <View style={{
                        flex: 1, ...ifIphoneX({
                            paddingBottom: getBottomSpace()
                        }, {
                                paddingBottom: getBottomSpace()

                            })
                    }}>
                        {this.state.response == '' ?
                            <View style={{ flex: 1, height: DEVICE_HEIGHT, alignSelf: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                                <TouchableOpacity style={{ alignSelf: 'center' }} onPress={this.retry}>
                                    <Text  allowFontScaling={false} style={{ color: colors.primaryColor, fontSize: DEVICE_HEIGHT * .05 }}>
                                        Retry
                           </Text>
                                </TouchableOpacity>
                            </View>


                            :
                            <ScrollView contentContainerStyle={{}}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.refresh}
                                    />
                                }


                            >
                                <View style={{ alignSelf: 'center', justifyContent: 'center', alignContent: 'center' }}>
                                    <View style={{ marginTop: DEVICE_HEIGHT * 0.2 / 4, alignSelf: 'center', justifyContent: 'center', alignContent: 'center' }}>

                                        <View style={{ alignSelf: 'center', }}>
                                            <Text  allowFontScaling={false} style={{ fontFamily: fonts.bold, color: colors.textColor, fontSize: (DEVICE_HEIGHT * .04 * .8), }}>{this.state.response['full_name'].toUpperCase()}</Text>
                                        </View>
                                        <View style={{ marginTop: -DEVICE_HEIGHT * .05, flexDirection: 'row', justifyContent: 'space-between', width: DEVICE_WIDTH * .70 }}>
                                            {(this.state.friend_status == 1 || this.state.friend_status == '1') ?
                                                <TouchableOpacity onPress={this.blockUser}>
                                                    <Image
                                                        source={block}
                                                        style={{
                                                            shadowOpacity: 0.3,
                                                            shadowRadius: 3,
                                                            shadowColor: '#000',
                                                            shadowOffset: { width: 0, height: 5 },
                                                            elevation: 3,
                                                            // marginLeft: DEVICE_WIDTH * .05,
                                                            marginTop: DEVICE_HEIGHT * .03,
                                                            height: DEVICE_HEIGHT * 0.11,
                                                            width: DEVICE_WIDTH * 0.11
                                                        }}
                                                        resizeMode="contain"
                                                    />
                                                </TouchableOpacity>
                                                :
                                                <TouchableOpacity onPress={this.frinedDialog}>
                                                    <Image
                                                        source={block}
                                                        style={{
                                                            shadowOpacity: 0.3,
                                                            shadowRadius: 3,
                                                            shadowColor: '#000',
                                                            shadowOffset: { width: 0, height: 5 },
                                                            elevation: 3,
                                                            // marginLeft: DEVICE_WIDTH * .05,
                                                            marginTop: DEVICE_HEIGHT * .03,
                                                            height: DEVICE_HEIGHT * 0.11,
                                                            width: DEVICE_WIDTH * 0.11
                                                        }}
                                                        resizeMode="contain"
                                                    />
                                                </TouchableOpacity>}
                                            {/* <View style={{ alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, }}>
                                                <Text style={{alignSelf: 'center', fontFamily: fonts.bold, color: colors.textColor, fontSize: (DEVICE_HEIGHT * .02), }}>
                                                {this.state.age}
                                                </Text>
                                            </View> */}
                                            {(this.state.friend_status == 1 || this.state.friend_status == '1') ?
                                                <TouchableOpacity onPress={this.chat} >
                                                    <Image
                                                        source={chat}
                                                        style={{
                                                            shadowOpacity: 0.3,
                                                            shadowRadius: 3,
                                                            shadowColor: '#000',
                                                            shadowOffset: { width: 0, height: 5 },
                                                            elevation: 3,
                                                            // marginLeft: DEVICE_WIDTH * .05,
                                                            marginTop: DEVICE_HEIGHT * .03,
                                                            height: DEVICE_HEIGHT * 0.11,
                                                            width: DEVICE_WIDTH * 0.11
                                                        }}
                                                        resizeMode="contain"
                                                    />
                                                </TouchableOpacity>
                                                :
                                                <TouchableOpacity onPress={this.frinedDialog}>
                                                    <Image
                                                        source={chat}
                                                        style={{
                                                            shadowOpacity: 0.3,
                                                            shadowRadius: 3,
                                                            shadowColor: '#000',
                                                            shadowOffset: { width: 0, height: 5 },
                                                            elevation: 3,
                                                            marginLeft: DEVICE_WIDTH * .05,
                                                            marginTop: DEVICE_HEIGHT * .03,
                                                            height: DEVICE_HEIGHT * 0.11,
                                                            width: DEVICE_WIDTH * 0.11
                                                        }}
                                                        resizeMode="contain"
                                                    />
                                                </TouchableOpacity>}
                                        </View>


                                        <View style={{ alignSelf: 'center', marginTop: -DEVICE_HEIGHT * .08, }}>
                                            <View style={{ alignSelf: 'center', }}>
                                                <Text allowFontScaling={false}  style={{ alignSelf: 'center', fontFamily: fonts.bold, color: colors.textColor, fontSize: (DEVICE_HEIGHT * .02), }}>
                                                    {this.state.age + ', ' + this.state.maleFemale}
                                                </Text>
                                            </View>
                                            {/* <Text style={{ alignSelf: 'center', fontFamily: fonts.bold, color: colors.textColor, fontSize: (DEVICE_HEIGHT * .02) }}>
                                                {this.state.maleFemale}
                                            </Text> */}

                                            {this.state.offering_a_drink != '' || this.state.request_a_drink != '' ?
                                                <Text  allowFontScaling={false} style={{ alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontFamily: fonts.bold, color: colors.textColor, fontSize: (DEVICE_HEIGHT * .02 * .9) }}>
                                                    {this.state.offering_a_drink}{this.state.request_a_drink}
                                                </Text>
                                                :
                                                null
                                            }
                                        </View>
                                    </View>
                                    <View style={{ alignSelf: 'center', justifyContent: 'center', alignContent: 'center', marginTop: DEVICE_HEIGHT * .02 }}>
                                        {this.state.friend_status == 0 || this.state.friend_status == '0' ?
                                            <View style={{ marginTop: DEVICE_HEIGHT * .03, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                                                {this.state.sendFriendRequsetLoading ?
                                                    <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                                                        <ActivityIndicator color={colors.primaryColor} />
                                                    </View>
                                                    :
                                                    <BlueButton
                                                        onPress={this.sendFriendRequset}
                                                        style={{
                                                            // borderRadius: 5,
                                                            backgroundColor: colors.primaryColor,
                                                            width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                                                        }}
                                                        textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                                        SEND REQUEST
                                       </BlueButton>
                                                }
                                            </View>
                                            :
                                            null}
                                        {this.state.friend_status == 4 || this.state.friend_status == '4' ?
                                            <View style={{ marginTop: DEVICE_HEIGHT * .03, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                                                {this.state.acceptRequestLoading ?
                                                    <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                                                        <ActivityIndicator color={colors.primaryColor} />
                                                    </View>

                                                    :
                                                    <BlueButton
                                                        onPress={this.acceptRequest}
                                                        style={{
                                                            // borderRadius: 5,
                                                            backgroundColor: colors.white,
                                                            width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                                                        }}
                                                        textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor }}>
                                                        ACCEPT REQUEST
</BlueButton>
                                                }
                                                {this.state.rejectRequestLoading ?
                                                    <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                                                        <ActivityIndicator color={colors.primaryColor} />
                                                    </View>

                                                    :
                                                    <BlueButton
                                                        onPress={this.rejectRequest}
                                                        style={{
                                                            // borderRadius: 5,
                                                            backgroundColor: colors.primaryColor,
                                                            width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                                                        }}
                                                        textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                                        REJECT REQUEST
</BlueButton>}
                                            </View>
                                            :
                                            null}
                                        {this.state.friend_status == 1 || this.state.friend_status == '1' ?
                                            <View style={{ marginTop: DEVICE_HEIGHT * .03, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                                                {this.state.removeFriendLoading ?
                                                    <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                                                        <ActivityIndicator color={colors.primaryColor} />
                                                    </View>
                                                    :
                                                    <BlueButton
                                                        onPress={this.removeFriend}
                                                        style={{
                                                            // borderRadius: 5,
                                                            backgroundColor: colors.primaryColor,
                                                            width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                                                        }}
                                                        textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                                        REMOVE FRIEND
</BlueButton>
                                                }
                                            </View> :
                                            null
                                        }
                                        {this.state.friend_status == 2 || this.state.friend_status == '2' ?
                                            <View style={{ marginTop: DEVICE_HEIGHT * .03, width: DEVICE_WIDTH * .85, alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                                                {this.state.cancelRequestLoading ?
                                                    <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                                                        <ActivityIndicator color={colors.primaryColor} />
                                                    </View>
                                                    :
                                                    <BlueButton
                                                        onPress={this.cancelRequest}
                                                        style={{
                                                            // borderRadius: 5,
                                                            backgroundColor: colors.primaryColor,
                                                            width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .04
                                                        }}
                                                        textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                                        CANCEL REQUEST
</BlueButton>
                                                }

                                            </View>
                                            :
                                            null}




                                        <View style={{ paddingLeft: DEVICE_WIDTH * .03, backgroundColor: 'rgba(255, 255, 255, 1)', marginTop: DEVICE_HEIGHT * .02 }}>

                                            <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * .03, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>



                                                <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                                        <Text  allowFontScaling={false} style={styles.labelText}>Food</Text>
                                                    </View>
                                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'center', justifyContent: 'center' }}>

                                                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle]}>
                                                            {this.state.foodText}
                                                        </Text>


                                                    </View>


                                                </View>
                                                <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>

                                            </View>



                                            <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * .03, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                                                <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                                        <Text  allowFontScaling={false} style={styles.labelText}>Dance</Text>
                                                    </View>
                                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'center', justifyContent: 'center' }}>

                                                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle]}>
                                                            {this.state.danceText}
                                                        </Text>

                                                    </View>


                                                </View>
                                                <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>



                                            </View>
                                            <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * .03, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                                                <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                                        <Text  allowFontScaling={false} style={styles.labelText}>Type Of Drinks</Text>
                                                    </View>
                                                    <View style={{ minHeight: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'center', justifyContent: 'center' }}>

                                                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle]}>
                                                            {this.state.typesOfDrinkText}
                                                        </Text>


                                                    </View>

                                                </View>
                                                <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>

                                            </View>


                                            <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * .03, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                                                <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                                        <Text  allowFontScaling={false} style={styles.labelText}>Intrested In</Text>
                                                    </View>
                                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'center', justifyContent: 'center' }}>

                                                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle]}>
                                                            {this.state.intrestedText}
                                                        </Text>


                                                    </View>

                                                </View>
                                                <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>

                                            </View>







                                            {this.state.bio != '' ?

                                                <View style={{ padding: DEVICE_HEIGHT * .04 }}>
                                                    <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle]}>
                                                        {this.state.bio}
                                                    </Text>

                                                </View> :
                                                null

                                            }


                                        </View>

                                        {this.state.otherMembers.length > 0 ?
                                            <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle,
                                            { alignSelf: 'center', fontSize: (DEVICE_HEIGHT * .03) * 1, color: colors.black, fontFamily: fonts.bold }, { marginTop: DEVICE_HEIGHT * .04 }]}>
                                                Other Members
                                            </Text> :
                                            null


                                        }

                                    </View>


                                    {this.state.otherMembers.length > 0 ?

                                        <ScrollView
                                            showsHorizontalScrollIndicator={false}
                                            horizontal={true}
                                            contentContainerStyle={{ paddingHorizontal: DEVICE_WIDTH * .08 }}
                                            style={{
                                                width: null,
                                                marginBottom: DEVICE_HEIGHT * .05,
                                                marginTop: DEVICE_HEIGHT * .03,
                                                // width: DEVICE_WIDTH * .90,
                                                // alignSelf: 'center'
                                            }}
                                        >

                                            {this.state.otherMembers.map(function (item, i) {
                                                return <TouchableOpacity
                                                    activeOpacity={.9}
                                                    style={{
                                                        borderRadius: 5, overflow: 'hidden', justifyContent: 'center',
                                                        alignItems: 'center', alignSelf: 'center',
                                                        marginRight: i == _this.state.otherMembers.length - 1 ? 0 : DEVICE_WIDTH * .05, backgroundColor: colors.primaryColor, width: DEVICE_WIDTH * .20, height: DEVICE_HEIGHT * .13
                                                    }}>
                                                    <View style={{ alignItems: 'center', alignSelf: 'center', }}>
                                                        {item.images != '' ?
                                                            <Image source={{ uri: item.images }}
                                                                style={styles.otherProfile}
                                                            />
                                                            :
                                                            <Image source={other_profile}
                                                                style={styles.otherProfile}
                                                            />
                                                        }
                                                    </View>
                                                    <View style={{ alignSelf: 'center', }}>
                                                        <Text  allowFontScaling={false} style={{ fontFamily: fonts.bold, color: colors.white, fontSize: (DEVICE_HEIGHT * .02) }}>{item.member_name}</Text>
                                                    </View>
                                                    <View style={{ alignSelf: 'center', }}>
                                                        <Text  allowFontScaling={false} style={{ fontFamily: fonts.bold, color: colors.white, fontSize: (DEVICE_HEIGHT * .02) }}>{item.member_age} Years</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            })}
                                        </ScrollView>

                                        :
                                        null
                                    }

                                </View>

                            </ScrollView>
                        }


                    </View>


                }


                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.detailVisible}
                    onRequestClose={() => {
                        this.setState({
                            detailVisible: false
                        })
                    }}>
                    <View style={{
                        height: DEVICE_HEIGHT,
                        backgroundColor: colors.primaryColor,
                        ...ifIphoneX(
                            {
                                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                            },
                            {
                                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                                paddingTop:
                                    Platform.OS == "ios"
                                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                                        : 0
                            }
                        )
                    }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: DEVICE_WIDTH, height: DEVICE_HEIGHT * .06, backgroundColor: colors.primaryColor }}>
                            <Text  allowFontScaling={false} style={{ textAlign: 'center', flexGrow: 10, alignSelf: 'center', fontFamily: fonts.bold, color: colors.white, fontSize: DEVICE_HEIGHT * .04 }}>
                                Details
                         </Text>
                            <TouchableHighlight style={{ right: 5, position: 'absolute', alignSelf: 'center', paddingRight: 10 }} onPress={() => {
                                this.setState({
                                    detailVisible: false
                                })
                            }}>
                                <Text  allowFontScaling={false} style={{
                                    alignSelf: 'center',
                                    marginLeft: DEVICE_WIDTH * .02,
                                    fontFamily: fonts.normal,
                                    fontSize: DEVICE_HEIGHT * 0.03,
                                    // height: DEVICE_HEIGHT * 0.06,
                                    color: colors.white
                                }}>
                                    Close

                                </Text>
                            </TouchableHighlight>

                        </View>
                        <View style={{ flex: 1, backgroundColor: 'rgba(255, 255, 255, 1)', }}>

                            <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * .03, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>



                                <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                        <Text  allowFontScaling={false} style={styles.labelText}>Food</Text>
                                    </View>
                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'center', justifyContent: 'center' }}>

                                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle]}>
                                            {this.state.foodText}
                                        </Text>


                                    </View>


                                </View>
                                <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>

                            </View>



                            <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * .03, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                                <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                        <Text  allowFontScaling={false} style={styles.labelText}>Dance</Text>
                                    </View>
                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'center', justifyContent: 'center' }}>

                                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle]}>
                                            {this.state.danceText}
                                        </Text>

                                    </View>


                                </View>
                                <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>



                            </View>
                            <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * .03, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                                <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                        <Text  allowFontScaling={false} style={styles.labelText}>Type Of Drinks</Text>
                                    </View>
                                    <View style={{ minHeight: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'center', justifyContent: 'center' }}>

                                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle]}>
                                            {this.state.typesOfDrinkText}
                                        </Text>


                                    </View>

                                </View>
                                <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>

                            </View>


                            <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * .03, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                                <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                        <Text  allowFontScaling={false} style={styles.labelText}>Intrested In</Text>
                                    </View>
                                    <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, alignContent: 'center', justifyContent: 'center' }}>

                                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle]}>
                                            {this.state.intrestedText}
                                        </Text>


                                    </View>

                                </View>
                                <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>

                            </View>








                            <View style={{ padding: DEVICE_HEIGHT * .04 }}>
                                <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle]}>
                                    {this.state.bio}
                                </Text>

                            </View>

                        </View>
                    </View>
                </Modal>

                <ImageView
                    images={this.state.imagesToView}
                    imageIndex={0}
                    isVisible={this.state.isImageViewVisible}
                />
                <FlashMessage ref="refProfiles" />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    labelText: {
        fontFamily: fonts.bold, color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03 * .8), alignSelf: 'flex-start'
    },

    textInput: {
        paddingHorizontal: DEVICE_HEIGHT * 0.01,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: colors.gray,
        textAlignVertical: "top",
        marginTop: DEVICE_HEIGHT * 0.04,
        alignSelf: "center",
        width: DEVICE_WIDTH * 0.85,
        height: DEVICE_HEIGHT * 0.15,
        // backgroundColor: colors.inputField,
        fontSize: DEVICE_HEIGHT * 0.02,
        fontFamily: fonts.normal,
        color: colors.inputText
    },

    modelDatePickerStringStyle: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.03 * .8,
        color: colors.textColor,
    },
    otherProfile: {
        borderWidth: 2,
        borderColor: colors.white,
        height: DEVICE_HEIGHT * 0.2 * .3,
        width: DEVICE_HEIGHT * 0.2 * .3,
        borderRadius: (DEVICE_HEIGHT * 0.2) * .3 / 2,
        alignSelf: 'center',

    },

    profileImage: {
        zIndex: 1,
        // marginTop: DEVICE_HEIGHT * .02,
        position: 'absolute',
        ...ifIphoneX(
            {
                top: (DEVICE_HEIGHT * 0.3) - (DEVICE_HEIGHT * 0.2 / 2) - (DEVICE_HEIGHT * 0.07) - (getStatusBarHeight() + DEVICE_HEIGHT * 0.02)
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02,
                top:
                    Platform.OS == "ios"
                        ? (DEVICE_HEIGHT * 0.3) - (DEVICE_HEIGHT * 0.2 / 2) - (DEVICE_HEIGHT * 0.07) - (getStatusBarHeight() + DEVICE_HEIGHT * 0.02)
                        : (DEVICE_HEIGHT * 0.3) - (DEVICE_HEIGHT * 0.2 / 2) - (DEVICE_HEIGHT * 0.07) - (DEVICE_HEIGHT * 0.02)
            }
        ),
        borderColor: colors.white,
        borderWidth: 4,
        height: DEVICE_HEIGHT * 0.2,
        width: DEVICE_HEIGHT * 0.2,
        borderRadius: (DEVICE_HEIGHT * 0.2) / 2,
        // borderRadius:DEVICE_HEIGHT*.10/2,
        // height: DEVICE_HEIGHT * .10,
        // width: DEVICE_HEIGHT * .10,
        alignSelf: 'center',

    },


    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.3 : DEVICE_HEIGHT * 0.3,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        marginBottom: DEVICE_HEIGHT * .1,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        justifyContent: "center",
        // marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03
    },




    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.03,
        width: DEVICE_WIDTH * 0.03
    },

    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03,
        alignSelf: "center",
        alignContent: "center"
    }
});
const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode)
};

const mapStateToProps = state => ({
    login: state.login
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OtherProfileDetails);
