import React, { Component } from 'react';
import {
    Platform,
    Text, ScrollView,
    View, FlatList,
    TextInput, Animated, TouchableWithoutFeedback,
    StyleSheet, ImageBackground,
    TouchableOpacity, TouchableHighlight,
    ActivityIndicator,
    Image,
    Modal, Dimensions
} from 'react-native';

const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import { fonts, colors } from '../../theme'
import CardView from 'react-native-cardview'
import BlueButton from '../common/BlueButton';
import { Rating, AirbnbRating } from 'react-native-elements';
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import applogo from "../../assets/app_logo.png";
const CARD_HEIGHT = DEVICE_HEIGHT / 3;
const CARD_WIDTH = DEVICE_WIDTH * .85;
export default class CrouselItem extends Component {
    state = {
        lat: '',
        long: '',
        distance: '',
        image: '',
        rating: 0,
        user_ratings_total: '',
        otherMembers: ['+99999999', '+9999999999', '+99999999999', '+99999999999', '+99999999999']
    }
    componentDidMount() {
        this.callPlaceDetails();

    }
    Capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    callPlaceDetails = () => {
        this.setState({
            isLoading: true
        })
        // console.log('place details' + 'Strt')
        var Url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + this.props.item['place_id'] + "&fields=address_component,adr_address,alt_id,formatted_address,geometry,icon,id,name,permanently_closed,photo,place_id,plus_code,scope,type,url,user_ratings_total,utc_offset,vicinity,rating&key=" + APIURLCONSTANTS.KEY + "&sessiontoken=" + this.props.myToken
        axios.get(Url)
            .then(res => {
                this.setState({
                    isLoading: false
                })
                // console.log('place details' + JSON.stringify(res['data']['result']))
                if (res['data']['result'] != null && res['data']['result'] != undefined && res['data']['result'] != '') {
                    this.setState({
                        lat: res['data']['result']['geometry']['location']['lat'],
                        lng: res['data']['result']['geometry']['location']['lng'],
                        rating: res['data']['result']['rating'],
                        user_ratings_total: res['data']['result']['user_ratings_total'],
                    })
                    if (res['data']['result']['photos']) {
                        if (res['data']['result']['photos'].length > 0) {
                            this.setState({
                                image: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + res['data']['result']['photos'][0]['photo_reference'] + "&key=" + APIURLCONSTANTS.KEY
                            })
                        }
                    }

                  
                        this.getDistance();
                    // }


                }
            }).catch(err => {
                // this.refs.loading.close();
                this.setState({
                    isLoading: false
                })
            });
    }

    getDistance = () => {
        this.setState({
            isLoading: true
        })
        var Url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + this.props.latitude + "," + this.props.longitude + "&destinations=" + this.state.lat + "," + this.state.lng + "&mode=driving&language=en-EN&sensor=false&key=" + APIURLCONSTANTS.KEY
        // console.log('place details' + Url)

        axios.get(Url)
            .then(res => {
                this.setState({
                    isLoading: false
                })
                // console.log('place distance' + JSON.stringify(res['data']['rows'][0]['elements'][0]['distance']))
                if (res['data']['rows'][0]['elements'] != null && res['data']['rows'][0]['elements'] != undefined && res['data']['rows'][0]['elements'] != '') {
                    // console.log('place distance running')
                    this.setState({
                        distance: res['data']['rows'][0]['elements'][0]['distance']['text']
                        //  + ' ' + res['data']['rows'][0]['elements'][0]['duration']['text']
                    })

                }
            }).catch(err => {
                // console.log('place distance' + JSON.stringify(err))
                // this.refs.loading.close();
                this.setState({
                    isLoading: false
                })
            });

    }
    render() {
        const { item, index, onPress } = this.props;
        // let _this = this;
        // let itemType = item.types[0]
        // let days = String(itemType).split('_');
        // let typeText = '';
        // if (days != null && days != '') {
        //     for (let i = 0; i < days.length; i++) {
        //         typeText = typeText + days[i] + ' '
        //     }
        // }

        return (

            <View style={{
                width: CARD_WIDTH,
                // height: CARD_HEIGHT,

                minHeight: CARD_HEIGHT,
                justifyContent: 'center',
                alignContent: 'center'

            }}>
                <TouchableOpacity
                    // onPress={onPress(item)}
                    onPress={() => onPress(item)}
                    style={styles.popular}>

                    {item['photos'] ?
                        <Image
                            source={{ uri: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + item['photos'][0]['photo_reference'] + "&key=" + APIURLCONSTANTS.KEY }}
                            style={{ height: CARD_HEIGHT / 1.5, width: CARD_WIDTH }}
                            resizeMode="cover"
                        />
                        :
                        <Image
                            source={{ uri: item['icon'] }}
                            style={{ height: CARD_HEIGHT / 1.5, width: CARD_WIDTH }}
                            resizeMode="cover"
                        />
                    }


                    <View style={{
                        marginLeft: DEVICE_WIDTH * .02,
                        marginTop: -DEVICE_HEIGHT * .03,
                        zIndex: 100,
                        shadowOpacity: 0.3,
                        shadowRadius: 3,
                        shadowColor: '#000',
                        shadowOffset: { width: 0, height: 5 },
                        elevation: 3,
                        justifyContent: 'center',
                        height: DEVICE_HEIGHT * .05,
                        width: DEVICE_HEIGHT * .05,
                        borderRadius: DEVICE_HEIGHT * .05 / 2,
                        backgroundColor: colors.white

                    }}>

                        <Image
                            source={{ uri: item['icon'] }}
                            resizeMode={'contain'}
                            style={styles.roundCircleImage}>
                        </Image>
                    </View>

                    <View style={{ paddingHorizontal: DEVICE_WIDTH * .02, paddingTop: DEVICE_HEIGHT * .01 }}>
                        <Text  allowFontScaling={false} style={{ fontFamily: fonts.bold, color: colors.textColor, fontSize: DEVICE_HEIGHT * .02 }}>
                            {item['name']}
                        </Text>
                        <Text  allowFontScaling={false} numberOfLines={1} style={{ marginTop: DEVICE_HEIGHT * .01, fontFamily: fonts.light, color: colors.textColor, fontSize: DEVICE_HEIGHT * .02 }}>
                            {item['vicinity']}
                        </Text>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View style={{ flexDirection: 'row' }}>
                                <Rating
                                    style={{ marginTop: DEVICE_HEIGHT * .01 * .4, }}
                                    imageSize={(DEVICE_HEIGHT * .03) * .6}
                                    readonly
                                    startingValue={this.state.rating}
                                />
                                {(this.state.user_ratings_total != '' && this.state.user_ratings_total != null && this.state.user_ratings_total != undefined) ?
                                    <Text allowFontScaling={false} 
                                        numberOfLines={1}
                                        style={[styles.totalRate, { marginLeft: DEVICE_WIDTH * .01 }]}>{"( " + this.state.user_ratings_total + " )"}</Text>

                                    :
                                    null
                                }
                                
                            </View>
                            <Text allowFontScaling={false} 
                                    numberOfLines={1}
                                    style={styles.distanceTime}>
                                    {this.state.distance}
                                    
                        </Text>



                        </View>

                        {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Rating
                                type='custom'
                                ratingColor={colors.blueColor}
                                ratingBackgroundColor={colors.gray}
                                // ratingColor='#000'
                                style={{ alignSelf: 'flex-start' }}
                                imageSize={(DEVICE_HEIGHT * .02 * .8)}
                                readonly
                            />
                            <Image style={{
                                height: DEVICE_HEIGHT * .02 * .8,
                                width: DEVICE_HEIGHT * .01
                            }} source={bookmark}>
                            </Image>
                        </View> */}
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    distanceTime: {
        paddingRight:DEVICE_WIDTH*.05,
        marginTop: DEVICE_HEIGHT * .01 * .4,
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .5,
    },
    totalRate: {
        marginTop: DEVICE_HEIGHT * .01 * .2,
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .6,
    },
    popular: {
        paddingBottom: DEVICE_HEIGHT * .02,

        alignContent: 'center',
        // justifyContent: 'center',
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 5 },
        elevation: 3,
        minHeight: DEVICE_HEIGHT * .25,
        width: CARD_WIDTH, borderRadius: 5,
        backgroundColor: colors.white,
        overflow: 'hidden',
        zIndex: 99,
    },
    est: {
        fontFamily: fonts.bold,
        color: colors.primaryColor,
        fontSize: (DEVICE_HEIGHT * .02) * .7,
    },

    touchable: {
        marginHorizontal: 10,
    },
    touchableview: {
        paddingVertical: DEVICE_HEIGHT * .02 * .3,
        justifyContent: 'flex-start',
        paddingHorizontal: 5,
        flex: 1,
        flexDirection: 'row',
        // height: DEVICE_HEIGHT * .12,
        flexGrow: 1,
        overflow: 'hidden',
        borderRadius: (DEVICE_HEIGHT * .12) / 30,
        width: DEVICE_WIDTH - 20
    },

    imageMainView: {
        // backgroundColor:'yellow',
        // flex: 3,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        height: DEVICE_HEIGHT * .12,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 5
        },
        elevation: 3
    },
    checkImage: {
        borderColor: colors.primaryColor,
        borderWidth: 2,
        height: DEVICE_HEIGHT * 0.12,
        width: DEVICE_HEIGHT * 0.12,
        borderRadius: (DEVICE_HEIGHT * 0.12) / 2,
        // borderRadius:DEVICE_HEIGHT*.10/2,
        // height: DEVICE_HEIGHT * .10,
        // width: DEVICE_HEIGHT * .10,
        alignSelf: 'center',

    },
    logoImage: {
        // borderColor: colors.primaryColor,
        // borderWidth: 2,
        height: DEVICE_HEIGHT * 0.12,
        width: DEVICE_HEIGHT * 0.12,
        // borderRadius: (DEVICE_HEIGHT * 0.12) / 2,
        // borderRadius: (DEVICE_HEIGHT * 0.08) / 2,
        // borderRadius:DEVICE_HEIGHT*.10/2,
        // height: DEVICE_HEIGHT * .10,
        // width: DEVICE_HEIGHT * .10,
        alignSelf: 'center',

    },
    numberMainView: {
        // backgroundColor:'green',
        paddingRight: DEVICE_WIDTH * .15,
        paddingLeft: DEVICE_WIDTH * .02,
        // paddingHorizontal: 
        alignContent: 'center',
        justifyContent: 'center',
        // flex: 7,
        // height: DEVICE_HEIGHT * .12,
    },
    numberText: {
        paddingRight: DEVICE_WIDTH * .05,
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .9,
    },
    vicinity: {
        paddingRight: DEVICE_WIDTH * .05,
        marginTop: DEVICE_HEIGHT * .01 * .4,
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .7,
    },
    othermembers: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .02) * .6,
    },
    othermembersNum: {
        fontFamily: fonts.bold,
        color: colors.primaryColor,
        fontSize: (DEVICE_HEIGHT * .02) * .6,
    },
    dateText: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .6,
    },
    distanceTime: {
        marginTop: DEVICE_HEIGHT * .01 * .4,
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .5,
    },
    totalRate: {
        marginTop: DEVICE_HEIGHT * .01 * .2,
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .6,
    },
    timeText: {
        marginLeft: DEVICE_WIDTH * .01,
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .6,
    },
    rupeeMainView: {
        backgroundColor: colors.blueColor,
        flex: 3,
        borderRadius: (DEVICE_HEIGHT * .04) / 2,
        height: (DEVICE_HEIGHT * .05) * .9,
        alignContent: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        // paddingRight: 10
    },
    rupeeText: {
        fontFamily: fonts.bold,
        alignSelf: 'center',
        color: colors.white,
        fontSize: DEVICE_WIDTH * .04,
    }

});
