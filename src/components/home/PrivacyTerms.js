import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Text,
    TouchableOpacity,
    BackHandler,
    Keyboard,
    Alert,
    FlatList,
    AsyncStorage,
    ScrollView,
    Platform,
    ImageBackground, KeyboardAvoidingView,
    Dimensions
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import {
    getStatusBarHeight,
    getBottomSpace,
    ifIphoneX
} from "react-native-iphone-x-helper";
import CardView from 'react-native-cardview'
import { fonts, colors } from "../../theme";
import * as Animatable from 'react-native-animatable';
const AnimatableFlatList = Animatable.createAnimatableComponent(FlatList);
import { createStackNavigator, createDrawerNavigator, createAppContainer, createMaterialTopTabNavigator } from 'react-navigation';

import LoadingSpinner from '../common/loadingSpinner/index';
import LoadingViewUsers from "./loadingViewUsers";
import UserListItem from "./userListItem";
import back from "../../assets/back.png";
import Tabs from './FriendTab'
import OtherProfileDetails from './OtherProfileDetails'
import PrivacyPolicy from './PrivacyPolicy'
import TermCondition from './TermCondition'
import MyFriendList from './MyFriendList'
let initialRootName='';
const StackNaviApp = createStackNavigator({

    // OtherProfileDetails: { screen: OtherProfileDetails },
    Tabs: {
        screen: createMaterialTopTabNavigator({

            Privacy: {
                screen: (props) => <PrivacyPolicy screenProps={props.screenProps} />
            },
            Terms: {
                screen: (props) => <TermCondition screenProps={props.screenProps} />
            },


        }, 
        {
            // initialRouteName: this.props.checkPageOpen==0?'Terms':'Privacy',
            tabBarOptions: {
                activeTintColor: colors.primaryColor,
                inactiveTintColor: colors.textColor,
                style: {
                    backgroundColor: colors.white,
                },
                indicatorStyle: {
                    backgroundColor: colors.primaryColor,
                },
            }
            })
    }
},
    { initialRouteName: 'Tabs', headerMode: 'none', });


const App = createAppContainer(StackNaviApp);

class PrivacyTerms extends Component {

    static router = {
        ...StackNaviApp.router,
        getStateForAction: (action, lastState) => {
            // check for custom actions and return a different navigation state.
            return StackNaviApp.router.getStateForAction(action, lastState);
        },
    };

    constructor(props) {
        super(props);
        this.state = {
            isFromDrawer: false,
            avatarSource: null,
            mobileRecharge: [],
            isLoading: true,
            page: 1,
            refreshing: false,
            checkPageOpen:0
        };
    }

    componentWillMount() {
        
        if(this.props.checkPageOpen!=undefined){
            this.setState({
                checkPageOpen:this.props.checkPageOpen
            })
            // initialRootName=this.props.checkPageOpen==0?'Terms':'Privacy'
        }
        console.log('this.props.checkPageOpen'+initialRootName)
        
        this.mounted = false
        // alert('componentWillMount')
        this.setState({
            //   isFromDrawer: this.props.navigation.state.params["fromNavigator"]
        });
    }

    componentDidMount() {
        this.mounted = true
        // this.props.navigation.navigate('Terms')
        setTimeout(() => {
            this.setState({
                isLoading: false,
                mobileRecharge: ['Mobile Recharge', 'option 2', 'option 1', 'option 2', 'option 1', 'option 2', 'option 1', 'option 2', 'option 1', 'option 2'],
                page: this.state.page + 1
            })
        }, 1000);

    }

    componentDidUpdate() { }



    goToProfile = () => {
        const { navigation } = this.props;
        navigation.navigate('OtherProfileDetails');
    }


    onClickBack = () => {
        this.props.navigation.goBack();
    };

    openDrawer = () => {
        this.props.navigation.openDrawer();
    };


    renderItem = ({ item, index }) => {
        return (
            <UserListItem
                navigation={this.props.navigation}
                item={item}
                index={index}
                onPress={this.goToProfile}

            />
        )

    }
    renderPaginationFetchingView = () => (
        <LoadingViewUsers>
        </LoadingViewUsers>
    )

    onPaginate = () => {

        this.retriveNewsData()
    }

    retriveNewsData = () => {
        let mobileRecharge = ['Mobile Recharge', 'option 2', 'option 1', 'option 2', 'option 1', 'option 2', 'option 1', 'option 2', 'option 1', 'option 2'];
        this.setState({
            refreshing: false,
            isLoading: false,

        })
        this.setState({ mobileRecharge: [...this.state.mobileRecharge, ...mobileRecharge] }, () => {
        })
    }
    onEndReached = (isLoading) => {
        if (!isLoading) {
            this.setState({
                isLoading: true,
            })
            setTimeout(() => {
                this.onPaginate();
            }, 2000);

        }

    }

    paginationWaitingView = () => {
        return (
            <View style={styles.paginationView}>
                <LoadingSpinner height={DEVICE_HEIGHT * .03} width={DEVICE_WIDTH} text="Loading ..." />
            </View>
        )
    }
    renderFooter = (isLoading) => {
        if (this.state.page == 1 && isLoading) {

            return this.renderPaginationFetchingView()
        } else if (isLoading) {
            return this.paginationWaitingView()
        }
        else if (
            this.state.page >= this.state.allPages
        ) {
            return this.paginationAllLoadedView()
        }

        return null
    }

    onRefresh = () => {
        console.log('onRefresh()')
        if (this.mounted) {
            this.setState({
                page: 1,//should be initial page number
                mobileRecharge: [],
                refreshing: true,
            }, () => {
                // let mobileRecharge=['Mobile Recharge', 'option 2','option 1', 'option 2','option 1', 'option 2','option 1', 'option 2','option 1', 'option 2'];
                // this.setState({
                //     mobileRecharge:mobileRecharge
                // })
                this.onPaginate();

            });



        }
    }
    render() {
        const { navigation } = this.props;
        return (
            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                <View style={styles.headerBackground}>
                    <View style={styles.dashBoardRow}>
                        {this.state.isFromDrawer ? (
                            <TouchableOpacity
                                onPress={this.openDrawer}
                                style={styles.menuTouch}
                            >
                                <Image
                                    source={menu}
                                    style={styles.menuIcon}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        ) : (
                                <TouchableOpacity
                                    onPress={this.onClickBack}
                                    style={styles.menuTouch}
                                >
                                    <Image
                                        source={back}
                                        style={styles.menuIcon}
                                        resizeMode="contain"
                                    />
                                </TouchableOpacity>
                            )}
                        <View style={styles.dashBoardView}>
                            <Text  allowFontScaling={false} style={styles.header}>Privacy/Terms</Text>
                        </View>
                        <TouchableOpacity onPress={this.goToHistory} style={styles.notiTouch}>
                            {/* <Image
                                source={list}
                                style={{
                                    tintColor: colors.white,
                                    height: DEVICE_HEIGHT * 0.05,
                                    width: DEVICE_WIDTH * 0.05
                                }}
                                resizeMode="contain"
                            /> */}
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{
                    flex: 1, ...ifIphoneX({
                        paddingBottom: getBottomSpace()
                    }, {
                            paddingBottom: getBottomSpace()

                        })
                }}>


                    <App screenProps={navigation} checkPageOpen={this.state.checkPageOpen}/>


                    {/* <Tabs /> */}





                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({


    paginationView: {
        flex: 0,
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT * .05,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.1 + 20 : DEVICE_HEIGHT * 0.1,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        justifyContent: "center",
        // marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03
    },




    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.03,
        width: DEVICE_WIDTH * 0.03
    },

    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03,
        alignSelf: "center",
        alignContent: "center"
    }
});
const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode)
};

const mapStateToProps = state => ({
    login: state.login
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PrivacyTerms);
