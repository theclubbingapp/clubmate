import React, { Component } from 'react';
import {
    Platform,
    Text, ScrollView,
    View, FlatList,
    TextInput, Animated, TouchableWithoutFeedback,
    StyleSheet, ImageBackground,
    TouchableOpacity, TouchableHighlight,
    ActivityIndicator,
    Image,
    Modal, Dimensions
} from 'react-native';

const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import { fonts, colors } from '../../theme'
import CardView from 'react-native-cardview'
import BlueButton from '../common/BlueButton';
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import moment from 'moment'
export default class SentPairUpListItem extends Component {
    state = {
        otherMembers: ['+99999999', '+9999999999', '+99999999999', '+99999999999', '+99999999999'],
        name: 'Loading....',
        isLoading: false,
    }
    componentDidMount() {
        this.callPlaceDetails();
    }


    cancelRequest = () => {
        let formData = new FormData()
        formData.append('friend_authtoken', this.props.item.friend_authtoken)
        formData.append('authtoken', this.props.myToken)
        formData.append('event_id', this.props.item.event_id)
        this.setState({
            isLoading: true
        })
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.CANCEL_PAIRUP_RERQUEST, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    isLoading: false
                })
                this.props.onPress(this.props.item, this.props.index)
            })
            .catch(error => {
                this.setState({
                    isLoading: false
                })
            });
    }

    callPlaceDetails = () => {
        this.setState({
            isLoading: true
            // 
        })
        var Url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + this.props.item['club_place_id'] + "&fields=address_component,adr_address,alt_id,formatted_address,geometry,icon,id,name,permanently_closed,photo,place_id,plus_code,scope,type,url,user_ratings_total,utc_offset,vicinity&key=" + APIURLCONSTANTS.KEY + "&sessiontoken=" + this.props.myToken
        axios.get(Url)
            .then(res => {
                this.setState({
                    isLoading: false
                })
                console.log('place details' + JSON.stringify(res['data']['result']))
                if (res['data']['result'] != null && res['data']['result'] != undefined && res['data']['result'] != '') {
                    this.setState({
                        name: res['data']['result']['name'],
                        // image: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + res['data']['result']['photos'][0]['photo_reference'] + "&key=" + APIURLCONSTANTS.KEY
                    })
                    this.setState({
                        // name: res['data']['result']['name'],
                        image: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + res['data']['result']['photos'][0]['photo_reference'] + "&key=" + APIURLCONSTANTS.KEY
                    })

                }
            }).catch(err => {
                // this.refs.loading.close();
                this.setState({
                    isLoading: false
                })
            });
    }

    render() {
        const { navigation, item, index, onPress, onPressImage } = this.props;
        let _this = this;
        let maleFemale = '';

        if (item.gender) {
            let array = JSON.parse(item.gender);
            for (var i = 0; i < array.length; i++) {
                if (array[i].isChecked) {
                    maleFemale = array[i].value;
                }
            }
        }

        let date=moment(item.date, ["DD-MM-YYYY"]).format("ddd DD-MMM-YYYY")
        let time=moment(item.time, ["HH:mm"]).format("hh:mm A")

        return (
            <View style={[styles.touchable, { marginTop: index > 0 ? 10 : 0 }]}>
                <CardView
                    style={{ backgroundColor: colors.white, }}
                    cardElevation={2}
                    cardMaxElevation={2}
                    cornerRadius={5}>
                    <View style={styles.touchableview}>
                        <TouchableOpacity onPress={() => onPressImage(item, index)} style={styles.imageMainView}>
                            <Image source={{ uri: item.image }}
                                style={styles.checkImage}
                            // resizeMode='contain'
                            />
                        </TouchableOpacity>
                        <View style={styles.numberMainView}>
                            <Text allowFontScaling={false}  numberOfLines={2} style={[styles.placeName]}>{this.state.name}</Text>
                            <View style={{ flexDirection: 'column' }}>
                                <Text  allowFontScaling={false} style={styles.numberText}>{item.full_name}</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                    {/* <Text style={styles.dateText}>{'D.O.B '+item.dob}</Text> */}
                                    <Text allowFontScaling={false}  style={styles.timeText}>{maleFemale}</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'column', justifyContent: 'space-between' }}>
                                <Text  allowFontScaling={false} style={styles.dateText}>{date}</Text>
                                <Text  allowFontScaling={false} style={styles.timeText}>{time}</Text>
                            </View>
                        </View>

                        {this.state.isLoading ?
                            <View style={{ position: 'absolute', bottom: DEVICE_HEIGHT * .01, right: DEVICE_WIDTH * .03, width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .04 }}>
                                <ActivityIndicator color={colors.primaryColor} />
                            </View>
                            :
                            <TouchableOpacity onPress={this.cancelRequest} style={{ borderRadius:5,paddingHorizontal:DEVICE_WIDTH*.02,paddingVertical:DEVICE_HEIGHT*.01,backgroundColor:colors.primaryColor,position: 'absolute', bottom: DEVICE_HEIGHT * .01, right: DEVICE_WIDTH * .03 }}>
                                <Text allowFontScaling={false}  style={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>Cancel</Text>
                            </TouchableOpacity>
                        }
                        {/* <View style={{ alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                            <BlueButton
                                onPress={() => onPress(item, index)}

                                style={{
                                    backgroundColor: colors.primaryColor,
                                    width: DEVICE_WIDTH * .30, height: DEVICE_HEIGHT * .04
                                }}
                                textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                CANCEL PAIR UP
                           </BlueButton>

                        </View> */}
                    </View>
                </CardView>
            </View>

        );
    }
}
const styles = StyleSheet.create({

    est: {
        fontFamily: fonts.bold,
        color: colors.primaryColor,
        fontSize: (DEVICE_HEIGHT * .02) * .7,
    },

    touchable: {
        marginHorizontal: 10,
    },
    touchableview: {
        paddingVertical: DEVICE_HEIGHT * .02 * .3,
        paddingHorizontal: 5,
        flex: 1,
        flexDirection: 'row',
        // height: DEVICE_HEIGHT * .12,
        overflow: 'hidden',
        borderRadius: (DEVICE_HEIGHT * .12) / 30,
        width: DEVICE_WIDTH - 20
    },

    imageMainView: {
        // flex: 2,
        alignSelf:'center',
        alignItems: 'center',
        justifyContent: 'center',
        height: DEVICE_HEIGHT * .12,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 5
        },
        elevation: 3
    },
    checkImage: {
        borderColor: colors.primaryColor,
        borderWidth: 2,
        height: DEVICE_HEIGHT * 0.12,
        width: DEVICE_HEIGHT * 0.12,
        borderRadius: (DEVICE_HEIGHT * 0.12) / 2,
        // borderRadius:DEVICE_HEIGHT*.10/2,
        // height: DEVICE_HEIGHT * .10,
        // width: DEVICE_HEIGHT * .10,
        alignSelf: 'center',

    },
    placeName: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .9,
    },
    numberMainView: {
        paddingHorizontal: DEVICE_WIDTH * .02,
        alignContent: 'center',
        justifyContent: 'center',
        flex: 9,
        // height: DEVICE_HEIGHT * .12,
    },
    numberText: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .9,
    },
    othermembers: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .02) * .6,
    },
    othermembersNum: {
        fontFamily: fonts.bold,
        color: colors.primaryColor,
        fontSize: (DEVICE_HEIGHT * .02) * .6,
    },
    dateText: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .6,
    },
    timeText: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .6,
    },
    rupeeMainView: {
        backgroundColor: colors.blueColor,
        flex: 3,
        borderRadius: (DEVICE_HEIGHT * .04) / 2,
        height: (DEVICE_HEIGHT * .05) * .9,
        alignContent: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        // paddingRight: 10
    },
    rupeeText: {
        fontFamily: fonts.bold,
        alignSelf: 'center',
        color: colors.white,
        fontSize: DEVICE_WIDTH * .04,
    }

});
