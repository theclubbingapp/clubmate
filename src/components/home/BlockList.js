import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Text,
    TouchableOpacity,
    BackHandler,
    Keyboard,
    Alert,
    FlatList, ActivityIndicator,
    AsyncStorage,
    ScrollView,
    Platform,
    ImageBackground, KeyboardAvoidingView,
    Dimensions
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import {
    getStatusBarHeight,
    getBottomSpace,
    ifIphoneX
} from "react-native-iphone-x-helper";
import { fonts, colors } from "../../theme";
import * as Animatable from 'react-native-animatable';
const AnimatableFlatList = Animatable.createAnimatableComponent(FlatList);
import LoadingSpinner from '../common/loadingSpinner/index';
import LoadingViewUsers from "./loadingViewUsers";
import BlockListItem from "./BlockListItem";
import back from "../../assets/back.png";
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
class BlockList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFromDrawer: false,
            avatarSource: null,
            userData: [],
            isLoading: false,
            page: 1,
            refreshing: false,
            response: '',
            name: '',
            place: '',
            city: '',
            mytoken: '',
        };
        this.getToken();
    }

    async getToken() {


        try {
            AsyncStorage.getItem(keys.token).then((value) => {
                console.log("token value" + JSON.stringify(value))
                this.setState({
                    mytoken: value,
                })
                this.setState({
                    isLoading: true
                })
                let formData = new FormData()
                formData.append('authtoken', this.state.mytoken)
                console.log('form dtaa' + JSON.stringify(formData))
                axios.post(APIURLCONSTANTS.BLOCK_LIST, formData, null)
                    .then(ApiUtils.checkStatus)
                    .then(res => {
                        console.log('response dtaa' + JSON.stringify(res))
                        this.setState({
                            isLoading: false,

                        })

                        if (res['data']['data'].length > 0) {
                            this.setState({
                                userData: res['data']['data']
                            })
                        }

                    })
                    .catch(error => {
                        console.log('response error' + JSON.stringify(error))
                        this.setState({
                            isLoading: false
                        })

                    });

            });
        } catch (err) {
            console.log("token getting" + JSON.stringify(err))
            this.setState({ mytoken: '' })
        }





    }


    componentDidMount() {
        this.mounted = true

    }

    componentDidUpdate() { }

    gotoProfile = (item, index) => {
        // const { navigation } = this.props;
        // alert(JSON.stringify(this.props))
        // return;

        this.props.navigation.navigate('OtherProfileDetails',
            {
                event_id: null,
                authtoken: item.friend_authtoken, mytoken: this.state.mytoken
            });

    }

    // goToProfile = (item, index) => {
    //     // alert(JSON.stringify(item))
    //     const { navigation } = this.props;
    //     navigation.navigate('OtherProfileDetails', { event_id: item.event_id, authtoken: item.authtoken, mytoken: this.state.mytoken });
    // }


    onClickBack = () => {
        this.props.navigation.goBack();
    };

    openDrawer = () => {
        this.props.navigation.openDrawer();
    };


    renderItem = ({ item, index }) => {
        return (
            <BlockListItem
                navigation={this.props.navigation}
                item={item}
                index={index}
                mytoken={this.state.mytoken}
                onPress={() => this.unblockRequest(item, index)}
                onPressImage={() => this.gotoProfile(item, index)}

            />
            // null
        )

    }
    unblockRequest = (item, index) => {
        this.removeFromList(item, index);
        // let formData = new FormData()
        // formData.append('friend_authtoken', item.friend_authtoken)
        // formData.append('authtoken', this.state.mytoken)
        // // formData.append('requestid', item.request_id)
        // console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        // axios.post(APIURLCONSTANTS.UNBLOCK_BLOCK_USER, formData, null)
        //     .then(ApiUtils.checkStatus)
        //     .then(res => {
               
        //     })
        //     .catch(error => {

        //     });

    }
    removeFromList(item, index) {
        this.setState({
            userData: this.state.userData.filter(function (data, i) {
                return i !== index
            })
        });
    }

    renderPaginationFetchingView = () => (
        <LoadingViewUsers>
        </LoadingViewUsers>
    )

    onPaginate = () => {
        this.retriveNewsData()
    }

    retriveNewsData = () => {
        // let mobileRecharge = ['Mobile Recharge', 'option 2', 'option 1', 'option 2', 'option 1', 'option 2', 'option 1', 'option 2', 'option 1', 'option 2'];
        // this.setState({
        //     refreshing: false,
        //     isLoading: false,

        // })
        let formData = new FormData()
        formData.append('club_place_id', this.state.placeId)
        formData.append('authtoken', this.state.mytoken)
        this.setState({
            isLoading: true,
        })
        axios.post(APIURLCONSTANTS.EVENTUSERS, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    isLoading: false
                })

            })
            .catch(error => {
                this.setState({
                    isLoading: false
                })

            });
        // this.setState({ mobileRecharge: [...this.state.mobileRecharge, ...mobileRecharge] }, () => {
        // })
    }
    onEndReached = (isLoading) => {
        if (!isLoading) {
            this.onPaginate();
        }

    }

    paginationWaitingView = () => {
        return (
            <View style={styles.paginationView}>
                <LoadingSpinner height={DEVICE_HEIGHT * .03} width={DEVICE_WIDTH} text="Loading ..." />
            </View>
        )
    }
    renderFooter = (isLoading) => {
        if (this.state.page > 1 && !isLoading) {
            return this.paginationWaitingView()
        }

        return null
    }

    onRefresh = () => {
        console.log('onRefresh()')
        if (this.mounted) {
            this.setState({
                page: 1,//should be initial page number
                userData: [],
                refreshing: true,
            }, () => {
                // let mobileRecharge=['Mobile Recharge', 'option 2','option 1', 'option 2','option 1', 'option 2','option 1', 'option 2','option 1', 'option 2'];
                // this.setState({
                //     mobileRecharge:mobileRecharge
                // })
                this.onPaginate();

            });



        }
    }


    retry = () => {
        this.setState({
            isLoading: true,
            userData: []
        })
        let formData = new FormData()
        formData.append('authtoken', this.state.mytoken)

        console.log('form dtaa' + JSON.stringify(formData))

        axios.post(APIURLCONSTANTS.BLOCK_LIST, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('response dtaa' + JSON.stringify(res))
                this.setState({
                    isLoading: false,

                })

                if (res['data']['data'].length > 0) {
                    this.setState({
                        userData: res['data']['data']
                    })
                }

            })
            .catch(error => {
                console.log('response error' + JSON.stringify(error))
                this.setState({
                    isLoading: false
                })

            });

    }

    render() {
        return (
            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                <View style={styles.headerBackground}>
                    <View style={styles.dashBoardRow}>
                        {this.state.isFromDrawer ? (
                            <TouchableOpacity
                                onPress={this.openDrawer}
                                style={styles.menuTouch}
                            >
                                <Image
                                    source={menu}
                                    style={styles.menuIcon}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        ) : (
                                <TouchableOpacity
                                    onPress={this.onClickBack}
                                    style={styles.menuTouch}
                                >
                                    <Image
                                        source={back}
                                        style={styles.menuIcon}
                                        resizeMode="contain"
                                    />
                                </TouchableOpacity>
                            )}
                        <View style={styles.dashBoardView}>
                            <Text  allowFontScaling={false}  style={styles.header}>BLOCK LIST</Text>
                        </View>
                        <TouchableOpacity onPress={this.goToHistory} style={styles.notiTouch}>
                            {/* <Image
                                source={list}
                                style={{
                                    tintColor: colors.white,
                                    height: DEVICE_HEIGHT * 0.05,
                                    width: DEVICE_WIDTH * 0.05
                                }}
                                resizeMode="contain"
                            /> */}
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{
                    flex: 1, ...ifIphoneX({
                        paddingBottom: getBottomSpace()
                    }, {
                            paddingBottom: getBottomSpace()

                        })
                }}>

                    {this.state.isLoading ?
                        <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                            <ActivityIndicator
                                style={{ alignSelf: 'center' }}
                                size={'large'}
                                color={colors.primaryColor}
                            ></ActivityIndicator>
                        </View>

                        :
                        <View style={{ flex: 1 }}>
                            {this.state.userData.length == 0 ?
                                <View style={{
                                    flex:1,
                                    alignSelf: 'center',
                                    justifyContent: 'center',
                                    alignSelf: 'center'
                                }}>
                                    <TouchableOpacity style={{ alignSelf: 'center' }} onPress={this.retry}>
                                        <Text  allowFontScaling={false} style={{ alignSelf: 'center', color: colors.primaryColor, fontSize: DEVICE_HEIGHT * .02 }}>
                                            You have not blocked any friend yet.
                                       </Text>
                                        <Text  allowFontScaling={false} style={{alignSelf: 'center',  marginTop: DEVICE_HEIGHT * .02, color: colors.primaryColor, fontSize: DEVICE_HEIGHT * .04 }}>
                                            REFRESH
                                  </Text>
                                    </TouchableOpacity>
                                </View>
                                :
                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={{ paddingBottom: 10, paddingTop: 10 }}
                                    ref={(ref) => { this.listView = ref; }}
                                    data={this.state.userData}
                                    keyExtractor={(item, index) => `${index} - ${item}`}
                                    renderItem={this.renderItem}
                                    extraData={this.state}
                                    // ListFooterComponent={() => this.renderFooter(this.state.isLoading)}
                                    // onEndReachedThreshold={0.1}
                                    // onEndReached={() => this.onEndReached(this.state.isLoading)}
                                    numColumns={1}
                                // refreshing={this.state.refreshing}
                                // onRefresh={this.onRefresh}
                                />
                            }
                        </View>
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({


    paginationView: {
        flex: 0,
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT * .05,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.1 + 20 : DEVICE_HEIGHT * 0.1,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        justifyContent: "center",
        // marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03
    },




    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.03,
        width: DEVICE_WIDTH * 0.03
    },

    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03,
        alignSelf: "center",
        alignContent: "center"
    }
});
const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode)
};

const mapStateToProps = state => ({
    login: state.login
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BlockList);
