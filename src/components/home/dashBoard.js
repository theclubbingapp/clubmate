import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Text,
    TouchableOpacity,
    BackHandler,
    Keyboard,
    Alert,
    FlatList, Animated,
    AsyncStorage,
    ScrollView, Slider,
    Platform, LayoutAnimation,
    ImageBackground, KeyboardAvoidingView,
    Dimensions, UIManager, Modal
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import MapView, { PROVIDER_GOOGLE, Callout, Marker } from 'react-native-maps';
import { SearchBar } from 'react-native-elements';
import LoadingSpinner from '../common/loadingSpinner/index';
import * as Animatable from 'react-native-animatable';
// AIzaSyAMAYUS0xQnsn-cyX1UkEJ9dFE9MEVHtpg
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import {
    getStatusBarHeight,
    getBottomSpace,
    ifIphoneX
} from "react-native-iphone-x-helper";
import BlueButton from '../common/BlueButton'
import { fonts, colors } from "../../theme";
import menu from "../../assets/menu.png";
import app_logo from "../../assets/app_logo.png";
import my_location from "../../assets/my_location.png";
import center from "../../assets/center.png";
import SearchListItem from './searchListItem'
import CrouselItem from './crouselItem'
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import list from "../../assets/list.png";
import marker from "../../assets/marker.png";
import pin from "../../assets/blue_marker.png";
import Loading from 'react-native-whc-loading'
// import GPSState from 'react-native-gps-state'   
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import firebase from 'react-native-firebase';
import RNExitApp from 'react-native-exit-app';
const CARD_HEIGHT = DEVICE_HEIGHT / 3;
const CARD_WIDTH = DEVICE_WIDTH * .85;

MyTouchable = Animatable.createAnimatableComponent(TouchableOpacity);
MyScroll = Animatable.createAnimatableComponent(ScrollView);
AnimatedTouchableOpacity = Animatable.createAnimatableComponent(TouchableOpacity);
import Carousel, { Pagination } from 'react-native-snap-carousel';
const AnimatedCrousal = Animated.createAnimatedComponent(Carousel);
var CustomLayoutSpring = {
    duration: 400,
    create: {
        type: LayoutAnimation.Types.spring,
        property: LayoutAnimation.Properties.scaleXY,
        springDamping: 0.7,
    },
    update: {
        type: LayoutAnimation.Types.spring,
        springDamping: 0.7,
    },
};



class Dashboard extends Component {

    constructor(props) {

        super(props);
        this.index = 0;
        this.animation = new Animated.Value(0);
        this._map = null;
        this.state = {
            radiusLoading: false,
            isRadiusSearch: true,
            isSubmitclicked: false,
            isFromDrawer: true,
            sliderValue: 10,
            search: '',
            modalVisible: false,
            radiusModelVisible: false,
            searchText: '',
            placeSearchResult: [],
            markers: [],
            isShow: true,
            latitude: 0,
            longitude: 0,
            error: null,
            isLoadingSearch: false,
            isLoadingRadiusSearch: false,

            pageToken: '',
            mytoken: '',
            region: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            goToFriendScreen: this.props.gotoPriend,
            next_page_token: ''
        };
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        }
        this.onRegionChange = this.onRegionChange.bind(this);
        this.fitToMarkersToMap = this.fitToMarkersToMap.bind(this);
        this.getToken();
        // this.onClickMarker = this.onClickMarker.bind(this);

        // try {
        //     AsyncStorage.getItem(keys.token).then((value) => {
        //         console.log("token value" + JSON.stringify(value))
        //         this.setState({
        //             mytoken: value,

        //         })

        //     });
        // } catch (err) {
        //     console.log("token getting" + JSON.stringify(err))
        //     this.setState({ mytoken: '' })
        // }

        if (Platform.OS == 'android') {
            this.checkGps();
        }




        // try {
        //     AsyncStorage.getItem(keys.gotoFriendScreen).then((value) => {
        //         console.log("token value" + JSON.stringify(value))
        //         if(value=='true'){
        //             this.props.navigation.navigate('MyProfile')
        //         }

        //     });
        // } catch (err) {
        //     console.log("token getting" + JSON.stringify(err))
        //     this.setState({ mytoken: '' })
        // }


        this.goToScreen();

    }
    handleSegmentChange = (role) => {
        this.setState({
            role: role
        })
    }

    get isBroadcaster() {
        return this.state.role === 1
    }






    async goToScreen() {
        let myToken = await AsyncStorage.getItem(keys.token);
        const { navigation } = this.props
        if (global.gotoFriendScreen) {

            if (global.type == 'frienRequest') {
                // var myToken = await AsyncStorage.getItem(keys.token);
                navigation.navigate('OtherProfileDetails',
                    {
                        event_id: global.event_id,
                        authtoken: global.user_authtoken,
                        mytoken: myToken
                    });
                global.gotoFriendScreen = false;

            } else
                if (global.type == 'pair') {
                    // var myToken = await AsyncStorage.getItem(keys.token);
                    navigation.navigate('PairUp',
                        {
                            event_id: global.event_id,
                            authtoken: global.user_authtoken,
                            mytoken: myToken
                        });
                    global.gotoFriendScreen = false;
                }


        }
        else if (global.gotoChatScreen) {
            if (global.type == 'chat') {
                navigation.navigate('ChatScreen',
                    {
                        authtoken: global.user_authtoken,
                        mytoken: this.state.mytoken,
                        phone_number: global.phone_number,
                        myPhoneNumber: global.myPhoneNumber,
                        myImage: global.myImage,
                        otherUserImage: global.otherUserImage,
                        from: 'noti',
                        name: global.name,
                        device_token: global.device_token,
                        event_id: global.event_id
                    })
                global.gotoChatScreen = false;
            }
        }
        else {
            if (global.type == 'broadcast') {
                navigation.navigate('Broadcast')
                global.gotoChatScreen = false;
                global.gotoFriendScreen = false;
            }

        }
        global.type = ''
        global.gotoFriendScreen = false
        global.gotoChatScreen = false
    }

    async getToken() {
        // AsyncStorage.getItem("userData").then((value) => {

		// 	if (value) {

		// 		if (JSON.parse(value).type == 'driver') {
		// 			console.log("recipient_address add...................................", item.recipient_address);
		// 			Actions.GeoLocationExampleScreen({ destination: item.recipient_address, ship_data:item });
		// 		}
		// 		else if (JSON.parse(value).type == 'customer') {

		// 			Actions.MapScreen({ shipmentId: item._id, from: 'Dashboard' });
		// 		}



		// 	}

		// }).done();
        let myToken = await AsyncStorage.getItem(keys.token);
        this.setState({
            mytoken: myToken,

        })

        if (this.state.mytoken == null || this.state.mytoken == '') {
            this.getToken();
        }

        // console.log('my token is valid' + this.state.mytoken)
    }


    openModel = () => {
        this.setState({
            modalVisible: true
        })
    }


    onRegionChange(region) {
        this.setState({ region });
    }

    resolveData = (data) => {
        var type = '';
        if (data != null) {
            type = data['type'];
            // console.log('datas notofiaction' + JSON.stringify(data.type))
            if (type == 'frienRequest') {
                global.gotoFriendScreen = true
                global.requestid = data['requestid']
                global.id = data['id']
                global.event_id = null
                global.user_authtoken = data['user_authtoken']
                global.type = 'frienRequest'
            }
            else if (type == 'pair') {
                global.gotoFriendScreen = true
                global.requestid = data['requestid']
                global.id = data['id']
                global.event_id = data['event_id']
                global.user_authtoken = data['user_authtoken']
                global.type = 'pair'
            }
            else if (type == 'chat') {
                global.type = 'chat'
                global.gotoFriendScreen = false
                global.gotoChatScreen = true
                global.phone_number = data['phone_number']
                global.myPhoneNumber = data['myPhoneNumber']
                global.myImage = data['myImage']
                global.otherUserImage = data['otherUserImage']
                global.name = data['name']
                global.user_authtoken = data['myToken']
                // global.user_authtoken = data['user_authtoken']
                global.device_token = data['device_token'],
                    global.event_id = data['event_id']

                   



            }
            else if (type == 'broadcast') {
                global.type = 'broadcast'
                global.gotoFriendScreen = false,
                    global.gotoChatScreen = false

            }

        }

        this.goToScreen();

    }


    async componentWillMount() {

        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
            const action = notificationOpen.action;
            const notification: Notification = notificationOpen.notification;
            console.log('d notifivation' + JSON.stringify(notification.data))
            this.resolveData(notification.data)
            firebase.notifications().removeDeliveredNotification(notification.notificationId);

        });

        await navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log('Position -> ', position);
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    // latitude: 30.7046,
                    // longitude: 76.7179,
                    error: null,
                });
                // let formData = new FormData()
                // formData.append('authtoken', this.state.mytoken)
                // formData.append('lat', position.coords.latitude)
                // formData.append('long', position.coords.longitude)
                // console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
                // axios.post(APIURLCONSTANTS.UPDATE_POSITION, formData, null)
                //     .then(ApiUtils.checkStatus)
                //     .then(res => {
                //         console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))

                //     })
                //     .catch(error => {
                //     });
                this.sendLocationToserver(position)

                this.navigateToMyLocation();

                this.fitToMarkersToMapGps(position.coords.latitude, position.coords.longitude);
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );
        let tempCoords = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        };
        let region = {
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        }
        this.setState(region)




        // this.fitToMarkersToMap();





    }

    async getLocation() {
        await navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log('Position -> ', position);
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    // latitude: 30.7046,
                    // longitude: 76.7179,
                    error: null,
                });
                // let formData = new FormData()
                // formData.append('authtoken', this.state.mytoken)
                // formData.append('lat', position.coords.latitude)
                // formData.append('long', position.coords.longitude)
                // console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
                // axios.post(APIURLCONSTANTS.UPDATE_POSITION, formData, null)
                //     .then(ApiUtils.checkStatus)
                //     .then(res => {
                //         console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))

                //     })
                //     .catch(error => {
                //     });
                this.sendLocationToserver(position)

                this.fitToMarkersToMapGps(position.coords.latitude, position.coords.longitude);
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );

        let region = {
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        }
        this.setState(region)
        this.navigateToMyLocation();
    }

    checkGps = () => {
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
            .then(data => {
                this.getLocation();

                //  - "already-enabled" if the location services has been already enabled
                //  - "enabled" if user has clicked on OK button in the popup
            }).catch(err => {
                // Alert.alert(
                //     'GPS is disabled',
                //     'Enable GPS or Exit App',
                //     [
                //       {text: 'Exit App', onPress: () =>RNExitApp.exitApp()},
                //       {text: 'Enable GPS', onPress: () => this.checkGps()},
                //     ],
                //     {cancelable: false},
                //   );
                // alert(JSON.stringify(err))
                // The user has not accepted to enable the location services or something went wrong during the process
                // "err" : { "code" : "ERR00|ERR01|ERR02", "message" : "message"}
                // codes : 
                //  - ERR00 : The user has clicked on Cancel button in the popup
                //  - ERR01 : If the Settings change are unavailable
                //  - ERR02 : If the popup has failed to open
            });
    }

    fitToMarkersToMapGps = (lat, long) => {

        let tempCoords = {
            latitude: lat,
            longitude: long
        };
        if (tempCoords.latitude != '' && tempCoords.latitude != 0 && tempCoords.longitude != '' && tempCoords.longitude != 0) {
            this._map.animateToCoordinate(tempCoords, 1000)
        }


    }

    fitToMarkersToMap = () => {

        let tempCoords = {
            latitude: this.state.latitude,
            longitude: this.state.longitude
        };
        if (tempCoords.latitude != '' && tempCoords.latitude != 0 && tempCoords.longitude != '' && tempCoords.longitude != 0) {
            this._map.animateToCoordinate(tempCoords, 1000)
        }


    }

    async componentDidMount() {
        this.animation.addListener(({ value }) => {
            console.log('coordinate')
            let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
            if (index >= this.state.markers.length) {
                index = this.state.markers.length - 1;
            }
            if (index <= 0) {
                index = 0;
            }

            clearTimeout(this.regionTimeout);
            this.regionTimeout = setTimeout(() => {
                if (this.index !== index) {
                    this.index = index;
                    // const { coordinate } = this.state.markers[index];
                    console.log('coordinate')
                    let coordinate = {
                        latitude: JSON.parse(this.state.markers[index]['geometry']['location']['lat']),
                        longitude: JSON.parse(this.state.markers[index]['geometry']['location']['lng']),
                        latitudeDelta: this.state.region.latitudeDelta,
                        longitudeDelta: this.state.region.longitudeDelta,
                    }
                    this._map.animateToRegion(
                        coordinate,
                        350
                    );
                }
            }, 10);
        });

        this.fitToMarkersToMap();
        // this._map.animateToCoordinate(tempCoords, 1000)

        this.watchId = navigator.geolocation.watchPosition(
            (position) => {
                // let formData = new FormData()
                // formData.append('authtoken', this.state.mytoken)
                // formData.append('lat', position.coords.latitude)
                // formData.append('long', position.coords.longitude)
                // console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
                // axios.post(APIURLCONSTANTS.UPDATE_POSITION, formData, null)
                //     .then(ApiUtils.checkStatus)
                //     .then(res => {
                //         console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))

                //     })
                //     .catch(error => {
                //     });
                this.sendLocationToserver(position)
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 },
        );


    }


    sendLocationToserver = (position) => {
        let formData = new FormData()
        formData.append('authtoken', this.state.mytoken)
        formData.append('lat', position.coords.latitude)
        formData.append('long', position.coords.longitude)
        console.log('SEND_FRIEND_REQUEST dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.UPDATE_POSITION, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('form SEND_FRIEND_REQUEST' + JSON.stringify(res))

            })
            .catch(error => {
            });
    }

    toggleSearchOption = () => {
        LayoutAnimation.configureNext(CustomLayoutSpring);
        this.setState({
            isRadiusSearch: true
        })
    }

    toggleSearch = () => {
        LayoutAnimation.configureNext(CustomLayoutSpring);
        this.setState({
            isRadiusSearch: false,
            isSubmitclicked: false
        })
    }

    updateSearch = search => {
        this.setState({ search });
    };

    componentDidUpdate() {

    }

    renderItemSearch = ({ item, index }) => {
        return (
            <SearchListItem
                // latitudeO={item['geometry']['location']['lat']}
                // latitudeO={item['geometry']['location']['lng']}
                from={'search'}
                latitude={this.state.latitude}
                longitude={this.state.longitude}
                myToken={this.state.mytoken}
                navigation={this.props.navigation}
                item={item}
                index={index}
                onPress={this.searchOnPress}

            />
        )

    }
    renderItemSearchRadius = ({ item, index }) => {
        return (
            <SearchListItem
                // latitudeO={item['geometry']['location']['lat']}
                // latitudeO={item['geometry']['location']['lng']}
                from={'radius'}
                latitude={this.state.latitude}
                longitude={this.state.longitude}
                myToken={this.state.mytoken}
                navigation={this.props.navigation}
                item={item}
                index={index}
                onPress={this.searchOnPress}

            />
        )

    }


    searchOnPress = (item) => {
        this.setState({
            modalVisible: false,
            radiusModelVisible: false
        })

        const { navigation } = this.props;
        navigation.navigate('BarDetails', { placeId: item['place_id'], mytoken: this.state.mytoken })

    }

    onEndReached = (isLoading) => {
        if (!isLoading) {
            console.log('next_page_token'+this.state.next_page_token)
            if (this.state.next_page_token == '') {
                this.setState({
                    isLoadingRadiusSearch: false
                })
                return;
            }
            this.setState({
                isLoadingRadiusSearch:true,
            })
            // this.refs.loading.show();
            axios.get("https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=" + this.state.next_page_token + "&&key=" + APIURLCONSTANTS.KEY)
                .then(res => {
                    this.setState({
                        isLoadingRadiusSearch:false
                    })
                    console.log('next_page_token=inside'+res['data']['next_page_token'])
                    console.log('next_page_token== result' + JSON.stringify(res['data']['results']))
                    if (res.data.results.length > 0) {
                        this.setState({ markers: [...this.state.markers, ...res['data']['results']] });
                    } 
                    this.setState({
                        next_page_token: res['data']['next_page_token']!=undefined ? res['data']['next_page_token'] : ''
                    })
                    console.log('next_page_token== length' + JSON.stringify(this.state.markers.length))
                    console.log('next_page_token== result length' + JSON.stringify(res.data.results.length))
                }).catch(err => {
                    this.setState({
                        isLoadingRadiusSearch:false
                    })
                });

        }

    }

    onPaginate = () => {

        this.retriveNewsData()
    }

    retriveNewsData = () => {



    }

    navigateToMyLocation = () => {

        let region = {
            latitude: this.state.latitude,
            longitude: this.state.longitude
        }
        // alert(JSON.stringify(region))
        this._map.animateToCoordinate(region, 1000)
    }
    paginationWaitingView = () => {
        return (
            <View style={styles.paginationView}>
                <LoadingSpinner height={DEVICE_HEIGHT * .03} width={DEVICE_WIDTH} text="Loading ..." />
            </View>
        )
    }

    renderFooterSearch = (isLoading) => {
        if (isLoading) {
            return this.paginationWaitingView()
        }
        return null
    }


    _handleTextInput = (event) => {
        // alert('')
        const query = event.nativeEvent.text;
        this.setState({ searchText: query });
        if (!query) this.setState({ searchText: '' });
        this.setState({
            isLoadingSearch: true
        })
        setTimeout(() => {
            this.setState({
                placeSearchResult: []
            })
            
            let search = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=" + query + "&inputtype=textquery&fields=formatted_address,geometry,icon,id,name,permanently_closed,photos,place_id,plus_code,scope,types,user_ratings_total&key=" + APIURLCONSTANTS.KEY
            // https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + this.state.latitude + "," + this.state.longitude + "&radius=" + 50000 + "&keyword=" + query + "&type=night_club&&key=" + APIURLCONSTANTS.KEY
            if (query.length) {
                axios.get("https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + query + "&types=establishment&key=" + APIURLCONSTANTS.KEY + "&sessiontoken=" + this.state.mytoken)
                    .then(res => {
                        console.log('predictions' + JSON.stringify(res['data']['predictions']))
                        if (res.data.predictions.length > 0) {
                            this.setState({
                                placeSearchResult: res['data']['predictions'],
                                // pageToken: res['data']['next_page_token'] ? res['data']['next_page_token'] : ''
                            })
                            // console.log(JSON.stringify(res['data']))
                            // console.log('next_page_token' + JSON.stringify(res['data']['next_page_token']))
                        } else {
                            // alert('No Clubs found please try again!')
                        }
                        this.setState({
                            isLoadingSearch: false
                        })

                    }).catch(err => {
                        this.setState({
                            isLoadingSearch: false
                        })
                    });
            }
        }, 500);
    }

    // shouldComponentUpdate(prevProps, prevState) {
    //     // if (prevState.sliderValue != this.state.sliderValue) {
    //     //     return false;
    //     // } else {
    //     //     return true;
    //     // }
    //     return true;
    // }

    isshowHide = () => {
        LayoutAnimation.configureNext(CustomLayoutSpring);
        this.setState({
            isShow: !this.state.isShow
        })
    }

    onSubmitRadius = () => {
        // this.props.navigation.navigate('Room');
        // return;
        // this._panel.show();
        this.setState({
            radiusLoading: true
        })
        this.refs.loading.show();
        axios.get("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + this.state.latitude + "," + this.state.longitude + "&radius=" + this.state.sliderValue * 1000 + "&type=night_club"+"&&key=" + APIURLCONSTANTS.KEY)
            .then(res => {
                this.refs.loading.close();
                this.setState({
                    radiusLoading: false,
                    // radiusModelVisible:true
                })
                // console.log('length' + JSON.stringify(res.data))
                this.setState({
                    next_page_token: res['data']['next_page_token'] ? res['data']['next_page_token'] : ''
                })

                if (res.data.results.length > 0) {
                    this.setState({
                        markers: res['data']['results']
                    })
                    let region = {
                        latitude: JSON.parse(res['data']['results']['0']['geometry']['location']['lat']),
                        longitude: JSON.parse(res['data']['results']['0']['geometry']['location']['lng'])
                    }
                    this._map.animateToCoordinate(region, 1000)
                    LayoutAnimation.configureNext(CustomLayoutSpring);
                    this.setState({
                        isShow: !this.state.isShow,
                        isSubmitclicked: true
                    })


                } else {
                    // alert('No Clubs found please try again!')
                }
            }).catch(err => {
                this.refs.loading.close();
                this.setState({
                    radiusLoading: false
                },()=>{
                    // alert('No Clubs found please try again!')
                })
                
                // alert(JSON.stringify(err))
                // console.log('next page', err);
                // eslint-disable-line  
            });
    }


    renderContent = () => {
        return (



            <FlatList
                style={{}}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    paddingBottom: DEVICE_HEIGHT * .1,
                    paddingTop: DEVICE_HEIGHT * .02,
                }}
                ref={(ref) => { this.searchRadius = ref; }}
                data={this.state.markers}
                keyExtractor={(item, index) => `${index} - ${item}`}
                renderItem={this.renderItemSearchRadius}
                extraData={this.state}
                ListFooterComponent={() => this.renderFooterSearch(this.state.isLoadingRadiusSearch)}
                onEndReachedThreshold={0.1}
                onEndReached={() => this.onEndReached(this.state.isLoadingRadiusSearch)}
                numColumns={1}
            />

        )
    }
    change(value) {
        this.setState(() => {
            return {
                sliderValue: parseFloat(value),
            };
        });
    }

    openSlide = () => {
        // this._panel.show();
        this.setState({
            radiusModelVisible: true
        })
    }

    onClickMarker(value) {
        // alert(JSON.stringify(value))
        // console.log('marker pressed'+value['name'])
        // const { navigation } = this.props;
        // navigation.navigate('BarDetails')
    }

    onMarkerPress = marker => event => {
        // console.log(JSON.stringify(marker))
        const { navigation } = this.props;
        navigation.navigate('BarDetails', { placeId: marker['place_id'], mytoken: this.state.mytoken })
    }

    componentWillUnmount() {
        this.notificationOpenedListener();
        navigator.geolocation.clearWatch(this.watchId);
        // GPSState.removeListener()
    }




    openDrawer = () => {
        this.props.navigation.openDrawer();
    };


    renderItemFunc = ({ item, index }) => {
        return (
            <CrouselItem
            // latitudeO={item['geometry']['location']['lat']}
            // latitudeO={item['geometry']['location']['lng']}
            // from={'search'}
            latitude={this.state.latitude}
            longitude={this.state.longitude}
            myToken={this.state.mytoken}
            navigation={this.props.navigation}
            item={item}
            index={index}
            onPress={this.onMarkerPress(item)}

        />
            // <View style={{
            //     width: CARD_WIDTH,
            //     // height: CARD_HEIGHT,

            //     minHeight: CARD_HEIGHT,
            //     justifyContent: 'center',
            //     alignContent: 'center'

            // }}>
            //     <TouchableOpacity
            //         onPress={this.onMarkerPress(item)}
            //         style={styles.popular}>

            //         {item['photos'] ?
            //             <Image
            //                 source={{ uri: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + item['photos'][0]['photo_reference'] + "&key=" + APIURLCONSTANTS.KEY }}
            //                 style={{ height: CARD_HEIGHT / 1.5, width: CARD_WIDTH }}
            //                 resizeMode="cover"
            //             />
            //             :
            //             <Image
            //                 source={{ uri: item['icon'] }}
            //                 style={{ height: CARD_HEIGHT / 1.5, width: CARD_WIDTH }}
            //                 resizeMode="cover"
            //             />
            //         }


            //         <View style={{
            //             marginLeft: DEVICE_WIDTH * .02,
            //             marginTop: -DEVICE_HEIGHT * .03,
            //             zIndex: 100,
            //             shadowOpacity: 0.3,
            //             shadowRadius: 3,
            //             shadowColor: '#000',
            //             shadowOffset: { width: 0, height: 5 },
            //             elevation: 3,
            //             justifyContent: 'center',
            //             height: DEVICE_HEIGHT * .05,
            //             width: DEVICE_HEIGHT * .05,
            //             borderRadius: DEVICE_HEIGHT * .05 / 2,
            //             backgroundColor: colors.white

            //         }}>

            //             <Image
            //                 source={{ uri: item['icon'] }}
            //                 resizeMode={'contain'}
            //                 style={styles.roundCircleImage}>
            //             </Image>
            //         </View>
            //         <View style={{ paddingHorizontal: DEVICE_WIDTH * .02, paddingTop: DEVICE_HEIGHT * .01 }}>
            //             <Text style={{ fontFamily: fonts.bold, color: colors.textColor, fontSize: DEVICE_HEIGHT * .02 }}>
            //                 {item['name']}
            //             </Text>
            //             <Text numberOfLines={1} style={{ marginTop: DEVICE_HEIGHT * .01, fontFamily: fonts.light, color: colors.textColor, fontSize: DEVICE_HEIGHT * .02 }}>
            //                 {item['vicinity']}
            //             </Text>

            //             {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            //                 <Rating
            //                     type='custom'
            //                     ratingColor={colors.blueColor}
            //                     ratingBackgroundColor={colors.gray}
            //                     // ratingColor='#000'
            //                     style={{ alignSelf: 'flex-start' }}
            //                     imageSize={(DEVICE_HEIGHT * .02 * .8)}
            //                     readonly
            //                 />
            //                 <Image style={{
            //                     height: DEVICE_HEIGHT * .02 * .8,
            //                     width: DEVICE_HEIGHT * .01
            //                 }} source={bookmark}>
            //                 </Image>
            //             </View> */}
            //         </View>
            //     </TouchableOpacity>
            // </View>
        )

    }


    render() {
        let _this = this;
        const interpolations = this.state.markers.map((marker, index) => {
            const inputRange = [
                (index - 1) * CARD_WIDTH,
                index * CARD_WIDTH,
                ((index + 1) * CARD_WIDTH),
            ];
            const scale = this.animation.interpolate({
                inputRange,
                outputRange: [1, 1.5, 1],
                extrapolate: "clamp",
            });
            const opacity = this.animation.interpolate({
                inputRange,
                outputRange: [0.35, 1, 0.35],
                extrapolate: "clamp",
            });
            const sizeText = this.animation.interpolate({
                inputRange,
                outputRange: [1, 1, 1],
                extrapolate: "clamp",
            });
            return { scale, opacity, sizeText };
        });

        // if (showLive) {
        //     return (
        //         <LiveView onCancel={this.handleCancel} channel={channel} uid={uid} role={role} />
        //     )
        // } else {
        return (
            // <ScrollView contentContainerStyle={{flex:1,backgroundColor:'transperent',alignContent:'center'}}>
            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                {/* <NavigationEvents onDidFocus={() =>alert()} /> */}

                <MapView
                    onMapReady={this.fitToMarkersToMap}
                    ref={component => this._map = component}
                    // zoomControlEnabled
                    showsScale
                    loadingEnable={true}
                    loadingIndicatorColor="#666666"
                    loadingBackgroundColor="#eeeeee"
                    provider={PROVIDER_GOOGLE}
                    style={{ ...StyleSheet.absoluteFillObject }}
                    initialRegion={this.state.region}
                    showsUserLocation={true}
                    fitToElements
                    fitToSuppliedMarkers={this.state.markers}
                >
                    {!!this.state.latitude && !!this.state.longitude &&
                        <MapView.Marker
                            // onPress={this.onMarkerPress(marker)}
                            coordinate={{ "latitude": this.state.latitude, "longitude": this.state.longitude }}

                        >
                            <View style={{ justifyContent: 'center' }}>
                                <Text  allowFontScaling={false} style={{ alignSelf: 'center', fontFamily: fonts.bold, fontSize: DEVICE_HEIGHT * .04 * .7, color: colors.primaryColor }}>
                                    {'Me'}
                                </Text>
                                <Image source={my_location} style={{ alignSelf: 'center', height: DEVICE_HEIGHT * .07, width: DEVICE_HEIGHT * .07 }}>

                                </Image>

                            </View>
                        </MapView.Marker>

                    }
                    {this.state.markers.length > 0 ? this.state.markers.map((marker, index) => {
                        const scaleStyle = {
                            transform: [
                                {
                                    scale: interpolations[index].scale,
                                },
                            ],
                        };
                        const opacityStyle = {
                            opacity: interpolations[index].opacity,
                        };
                        const sizeText = {
                            transform: [
                                {
                                    scale: interpolations[index].sizeText,
                                },
                            ],

                        };

                        return (

                            <MapView.Marker
                                onPress={this.onMarkerPress(marker)}
                                key={index}
                                showCallout={true}
                                // onCalloutPress={this.onClickMarker}
                                coordinate={{
                                    latitude: JSON.parse(marker['geometry']['location']['lat']),
                                    longitude: JSON.parse(marker['geometry']['location']['lng'])
                                }}
                            >
                                <Animated.View style={[styles.markerWrap, opacityStyle]}>
                                    <Animated.Text  allowFontScaling={false} style={[sizeText, { marginBottom: DEVICE_HEIGHT * .01, alignSelf: 'center', fontFamily: fonts.bold, fontSize: DEVICE_HEIGHT * .03, color: colors.primaryColor }]}>
                                        {marker['name']}
                                    </Animated.Text>
                                    <Animated.View style={[scaleStyle]} >
                                        <AnimatedTouchableOpacity
                                            style={{ justifyContent: 'center', }}>
                                            {/* <Animated.Image source={{ uri: marker['icon'] }} style={{alignSelf: 'center', height: DEVICE_HEIGHT * .04, width: DEVICE_HEIGHT * .04 }}>
                                        </Animated.Image> */}
                                            <Animated.Image source={pin} style={{ alignSelf: 'center', height: DEVICE_HEIGHT * .04, width: DEVICE_HEIGHT * .04 }}>
                                            </Animated.Image>
                                        </AnimatedTouchableOpacity>
                                    </Animated.View>
                                    {/* <View style={styles.marker} /> */}
                                </Animated.View>
                                {/* <MapView.Callout>
                                <TouchableOpacity
                                    style={{ justifyContent: 'center' }}>
                                    <Text style={{ alignSelf: 'center', fontFamily: fonts.bold, fontSize: DEVICE_HEIGHT * .02 * .7, color: colors.primaryColor }}>
                                        {marker['name']}
                                    </Text>
                                    <Image source={{ uri: marker['icon'] }} style={{ alignSelf: 'center', height: DEVICE_HEIGHT * .04, width: DEVICE_HEIGHT * .04 }}>

                                    </Image>

                                </TouchableOpacity>
                            </MapView.Callout> */}

                            </MapView.Marker>
                        )

                    }) : null}


                </MapView>



                <View style={styles.headerBackground}>
                    <View style={styles.dashBoardRow}>
                        {this.state.isFromDrawer ? (
                            <TouchableOpacity
                                onPress={this.openDrawer}
                                style={styles.menuTouch}
                            >
                                <Image
                                    source={menu}
                                    style={styles.menuIcon}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        ) : (
                                <TouchableOpacity
                                    onPress={this.onClickBack}
                                    style={styles.menuTouch}
                                >
                                    <Text  allowFontScaling={false} style={styles.backText}>Back</Text>
                                </TouchableOpacity>
                            )}
                        <View style={styles.dashBoardView}>
                            <Text allowFontScaling={false}  style={styles.header}>Home</Text>
                        </View>

                        {this.state.markers.length > 0 ?

                            <MyTouchable onPress={this.openSlide} style={styles.notiTouch}>
                                <Image
                                    source={list}
                                    style={{
                                        tintColor: colors.white,
                                        height: DEVICE_HEIGHT * 0.05,
                                        width: DEVICE_WIDTH * 0.05
                                    }}
                                    resizeMode="contain"
                                />
                            </MyTouchable>
                            :

                            <MyTouchable style={styles.notiTouch}>
                                <View style={{

                                    height: DEVICE_HEIGHT * 0.05,
                                    width: DEVICE_WIDTH * 0.05
                                }}>

                                </View>
                            </MyTouchable>}

                    </View>
                </View>


                {this.state.isRadiusSearch && this.state.isSubmitclicked ?
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={this.isshowHide}
                        style={[styles.touchableButton, { position: 'absolute', right: DEVICE_WIDTH * .08, top: DEVICE_HEIGHT * .15, width: DEVICE_WIDTH * .2, backgroundColor: this.state.isRadiusSearch ? colors.primaryColor : colors.white }]}>
                        <Text  allowFontScaling={false} style={styles.touchableText}>
                            {this.state.isShow ? 'Hide' : 'Show'}
                        </Text>

                    </TouchableOpacity> :
                    null}



                <View style={styles.buttonContainer}>
                    {this.state.isShow ?
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={this.toggleSearchOption} style={[styles.touchableButton, { backgroundColor: this.state.isRadiusSearch ? colors.primaryColor : colors.white }]}>
                                <Text  allowFontScaling={false} style={styles.touchableText}>
                                    Bar/Club Near me
           </Text>

                            </TouchableOpacity>

                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={this.toggleSearch} style={[styles.touchableButton, { backgroundColor: this.state.isRadiusSearch ? colors.white : colors.primaryColor }]}>
                                <Text  allowFontScaling={false} style={styles.touchableText}>
                                    Bar/Club Name
           </Text>

                            </TouchableOpacity>

                        </View>
                        :


                        null}


                    {this.state.isRadiusSearch ?

                        <View>
                            {this.state.isShow ?
                                <View>

                                    <Text  allowFontScaling={false} style={{ marginTop: DEVICE_HEIGHT * .03, alignSelf: 'center', fontSize: DEVICE_HEIGHT * .02, color: colors.primaryColor, fontFamily: fonts.bold }}>
                                        {String(this.state.sliderValue) + ' Km'}
                                    </Text>
                                    <Slider
                                        style={{ width: DEVICE_WIDTH * .85, alignSelf: 'center' }}
                                        step={1}
                                        maximumValue={50}
                                        minimumTrackTintColor={colors.primaryColor}
                                        maximumTrackTintColor={colors.gray}
                                        thumbTintColor={colors.primaryColor}
                                        onValueChange={this.change.bind(this)}
                                        value={this.state.sliderValue}
                                    />
                                </View>
                                :
                                null}
                        </View>

                        :
                        <View style={{ marginBottom: !this.state.isRadiusSearch ? DEVICE_HEIGHT * .2 : DEVICE_HEIGHT * .03, alignSelf: 'center', marginTop: DEVICE_HEIGHT * .03 }}>
                            <TouchableOpacity
                                style={{
                                    justifyContent: 'center',
                                    borderRadius: 5,
                                    backgroundColor: colors.white, width: DEVICE_WIDTH * .85, height: DEVICE_HEIGHT * .05
                                }}
                                onPress={this.openModel}>
                                <Text  allowFontScaling={false} style={{ paddingLeft: DEVICE_WIDTH * .03, color: '#d3d3d3', fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.normal }}>
                                    Search
                                </Text>
                            </TouchableOpacity>

                        </View>

                    }
                    <View style={{
                        ...ifIphoneX(
                            {
                                marginBottom: this.state.isShow ? 0 : getBottomSpace() + DEVICE_HEIGHT * 0.1
                            },
                            {
                                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                                marginBottom:
                                    this.state.isShow ? 0 : Platform.OS == "ios"
                                        ? getBottomSpace() + DEVICE_HEIGHT * 0.1
                                        : DEVICE_HEIGHT * 0.1
                            }
                        )
                    }}>
                        {this.state.isRadiusSearch ?



                            <View>
                                {this.state.markers.length > 0 ?

                                    <AnimatedCrousal
                                        data={this.state.markers}
                                        renderItem={this.renderItemFunc}
                                        sliderWidth={DEVICE_WIDTH}
                                        itemWidth={CARD_WIDTH}
                                        onScroll={Animated.event(
                                            [
                                                {
                                                    nativeEvent: {
                                                        contentOffset: {
                                                            x: this.animation,
                                                        },
                                                    },
                                                },
                                            ],
                                            { useNativeDriver: true }
                                        )}
                                    // firstItem={1}
                                    // enableMomentum={true}
                                    // loop={true}
                                    // loopClonesPerSide={2}
                                    // inactiveSlideScale={1}
                                    // activeSlideAlignment={'start'}
                                    // inactiveSlideOpacity={1}
                                    // autoplay={true}
                                    // autoplayDelay={500}
                                    // autoplayInterval={3000}
                                    // onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index })}
                                    // containerCustomStyle={{paddingHorizontal:40}}
                                    // contentContainerCustomStyle={{ paddingHorizontal: 40 }}
                                    // scrollInterpolator={scrollInterpolators[`scrollInterpolator${refNumber}`]}
                                    // slideInterpolatedStyle={animatedStyles[`animatedStyles${refNumber}`]}
                                    // useScrollView={true}
                                    />

                                    //         <Animated.ScrollView
                                    //         horizontal
                                    //         scrollEventThrottle={1}
                                    //         showsHorizontalScrollIndicator={false}
                                    //         snapToInterval={CARD_WIDTH}
                                    //         onScroll={Animated.event(
                                    //             [
                                    //                 {
                                    //                     nativeEvent: {
                                    //                         contentOffset: {
                                    //                             x: this.animation,
                                    //                         },
                                    //                     },
                                    //                 },  
                                    //             ],
                                    //             { useNativeDriver: true }
                                    //         )}
                                    //         style={styles.scrollView}
                                    //         contentContainerStyle={styles.endPadding}
                                    //     >
                                    //           {this.state.markers.map((marker, index) => (
                                    //             <TouchableOpacity 
                                    //             onPress={this.onMarkerPress(marker)}
                                    //             style={styles.card} key={index}>
                                    //                 {marker['photos']?
                                    //                  <Image
                                    //                  source={{ uri: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+marker['photos'][0]['photo_reference']+ "&key=" + APIURLCONSTANTS.KEY }}
                                    //                  style={styles.cardImage}
                                    //                  resizeMode="cover"
                                    //              />
                                    //                 :
                                    //                 <Image
                                    //                 source={{ uri: marker['icon'] }}
                                    //                 style={styles.cardImage}
                                    //                 resizeMode="cover"
                                    //             />
                                    //             }


                                    //                  <View style={styles.textContent}>
                                    //     <Text numberOfLines={2} style={styles.cardtitle}>{marker['name']}</Text>

                                    //   </View>

                                    //             </TouchableOpacity>
                                    //         ))}
                                    //     </Animated.ScrollView>

                                    :

                                    null}
                            </View>
                            :
                            null
                        }
                    </View>


                    {this.state.isRadiusSearch ?
                        <View>
                            {this.state.isShow ?
                                <View style={{ marginTop: DEVICE_HEIGHT * .02, marginBottom: DEVICE_HEIGHT * .05, alignSelf: 'center' }}>
                                    <BlueButton

                                        onPress={this.onSubmitRadius}
                                        // onPress={this.handleJoin}
                                        style={{
                                            borderRadius: 5,
                                            backgroundColor: colors.primaryColor,
                                            width: DEVICE_WIDTH * .35, height: DEVICE_HEIGHT * .05
                                        }}
                                        textStyles={{ fontSize: DEVICE_HEIGHT * .03, color: colors.white }}>
                                        SUBMIT
                    </BlueButton>
                                </View> :
                                null}

                        </View>
                        : null}



                    <TouchableOpacity onPress={this.navigateToMyLocation} style={{ position: 'absolute', left: 20, bottom: 20 }}>
                        <Image source={center} style={{ alignSelf: 'center', height: DEVICE_HEIGHT * .08, width: DEVICE_HEIGHT * .08 }}></Image>
                    </TouchableOpacity>




                </View>



                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setState({
                            modalVisible: false
                        })
                    }}>
                    <View style={{
                        height: DEVICE_HEIGHT,
                        backgroundColor: 'rgba(255, 255, 255, .8)',
                        ...ifIphoneX(
                            {
                                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                            },
                            {
                                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                                paddingTop:
                                    Platform.OS == "ios"
                                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                                        : DEVICE_HEIGHT * 0.02
                            }
                        )
                    }}>
                        <View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                            <View style={{ borderWidth: 1, borderColor: colors.primaryColor, alignSelf: 'center', borderRadius: 5, backgroundColor: colors.white, height: DEVICE_HEIGHT * 0.06, width: DEVICE_WIDTH * .75 }}>
                                <TextInput
                                    // onChangeText={(searchText) => this.setState({ searchText: String.prototype.trim.call(searchText) })}
                                    allowFontScaling={false} 
                                    style={{
                                        paddingHorizontal: DEVICE_WIDTH * .04,
                                        fontFamily: fonts.normal,
                                        fontSize: DEVICE_HEIGHT * 0.03,
                                        height: DEVICE_HEIGHT * 0.06,
                                        color: colors.black
                                    }}
                                    placeholder={"Search your club here"}
                                    autoCorrect={false}
                                    autoCapitalize={"none"}
                                    autoFocus={true}
                                    returnKeyType={'search'}
                                    value={this.state.searchText}
                                    onChange={this._handleTextInput}
                                    // returnKeyType={"done"}
                                    placeholderTextColor={colors.gray}
                                    underlineColorAndroid="transparent"
                                />

                            </View>
                            <TouchableHighlight style={{ justifyContent: 'center', alignSelf: 'center' }} onPress={() => {
                                this.setState({
                                    modalVisible: false
                                })
                            }}>
                                <Text 
                                 allowFontScaling={false} 
                                style={{
                                    alignSelf: 'center',
                                    marginLeft: DEVICE_WIDTH * .02,
                                    fontFamily: fonts.normal,
                                    fontSize: DEVICE_HEIGHT * 0.03,
                                    // height: DEVICE_HEIGHT * 0.06,
                                    color: colors.primaryColor
                                }}>
                                    Close

                                </Text>
                            </TouchableHighlight>

                        </View>
                        <FlatList
                            style={{ marginTop: DEVICE_HEIGHT * .01 }}
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{
                                paddingBottom: DEVICE_HEIGHT * .03,
                                paddingTop: DEVICE_HEIGHT * .03,
                            }}
                            ref={(ref) => { this.searchPlacesList = ref; }}
                            data={this.state.placeSearchResult}
                            keyExtractor={(item, index) => `${index} - ${item}`}
                            renderItem={this.renderItemSearch}
                            extraData={this.state}
                            // ListFooterComponent={() => this.renderFooterSearch(this.state.isLoadingSearch)}
                            // onEndReachedThreshold={0.1}
                            // onEndReached={() => this.onEndReached(this.state.isLoadingSearch)}
                            numColumns={1}
                        // refreshing={this.state.refreshing}
                        // onRefresh={this.onRefresh}
                        />

                    </View>
                </Modal>


                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.radiusModelVisible}
                    onRequestClose={() => {
                        this.setState({
                            radiusModelVisible: false
                        })
                    }}>
                    <View style={{
                        height: DEVICE_HEIGHT,
                        backgroundColor: 'rgba(255, 255, 255, 1)',
                        ...ifIphoneX(
                            {
                                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                            },
                            {
                                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                                paddingTop:
                                    Platform.OS == "ios"
                                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                                        : 0
                            }
                        )
                    }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: DEVICE_WIDTH, height: DEVICE_HEIGHT * .06, backgroundColor: colors.primaryColor }}>
                            <Text  allowFontScaling={false} style={{ textAlign: 'center', flexGrow: 10, alignSelf: 'center', fontFamily: fonts.bold, color: colors.white, fontSize: DEVICE_HEIGHT * .04 }}>
                                Select Place
                         </Text>
                            <TouchableHighlight style={{ right: 5, position: 'absolute', alignSelf: 'center', paddingRight: 10 }} onPress={() => {
                                this.setState({
                                    radiusModelVisible: false
                                })
                            }}>
                                <Text 
                                 allowFontScaling={false} 
                                style={{
                                    alignSelf: 'center',
                                    marginLeft: DEVICE_WIDTH * .02,
                                    fontFamily: fonts.normal,
                                    fontSize: DEVICE_HEIGHT * 0.03,
                                    // height: DEVICE_HEIGHT * 0.06,
                                    color: colors.white
                                }}>
                                    Map

                                </Text>
                            </TouchableHighlight>

                        </View>
                        {this.renderContent()}

                    </View>
                </Modal>




                <Loading ref="loading"
                    backgroundColor='#ffffffF2'
                    borderRadius={5}
                    size={70}
                    imageSize={40}
                    indicatorColor='gray'
                />

            </View>
        );
    }
    // }
}

const styles = StyleSheet.create({
    roundCircleImage: { alignSelf: 'center', height: DEVICE_HEIGHT * .3, width: DEVICE_HEIGHT * .03 },
    popular: {
        paddingBottom: DEVICE_HEIGHT * .02,

        alignContent: 'center',
        // justifyContent: 'center',
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 5 },
        elevation: 3,
        minHeight: DEVICE_HEIGHT * .25,
        width: CARD_WIDTH, borderRadius: 5,
        backgroundColor: colors.white,
        overflow: 'hidden',
        zIndex: 99,
    },
    cardtitle: {
        fontFamily: fonts.medium,
        fontSize: DEVICE_HEIGHT * .02,
        marginTop: DEVICE_HEIGHT * .01,
    },
    cardDescription: {
        fontSize: 12,
        color: "#444",
    },
    textContent: {
        // flex: 1,
    },
    paginationView: {
        flex: 0,
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT * .05,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, card: {
        alignSelf: 'center',
        padding: 10,
        elevation: 2,
        backgroundColor: "#FFF",
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: "hidden",
    },

    marker: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: "rgba(130,4,150, 0.9)",
    },
    ring: {
        width: DEVICE_HEIGHT * .03,
        height: DEVICE_HEIGHT * .03,
        borderRadius: DEVICE_HEIGHT * .03 / 2,
        backgroundColor: "rgba(130,4,150, 0.3)",
        position: "absolute",
        borderWidth: 1,
        borderColor: "rgba(130,4,150, 0.5)",
    },
    markerWrap: {
        // height: DEVICE_HEIGHT * .07,
        // width: DEVICE_HEIGHT * .07,
        alignItems: "center",
        justifyContent: "center",
    },
    cardImage: {
        flex: 3,
        width: "100%",
        height: "100%",
        alignSelf: "center",
    },

    scrollView: {
        marginTop: DEVICE_HEIGHT * .05,
        height: CARD_HEIGHT + 10,
        bottom: 30,
        left: 0,
        right: 0,
        paddingVertical: 10,
    },
    endPadding: {
        // paddingRight: DEVICE_WIDTH - CARD_WIDTH,
    },
    touchableText: { fontFamily: fonts.normal, fontSize: DEVICE_HEIGHT * .03, color: colors.gray },

    touchableButton: {
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        height: DEVICE_HEIGHT * .05,
        backgroundColor: colors.white,
        width: DEVICE_WIDTH * .45
    },

    buttonContainer: {
        position: 'absolute',
        bottom: 0,
        width: DEVICE_WIDTH,
        // height: DEVICE_HEIGHT * .35,
        // backgroundColor: '#d3d3d3',
    },
    dropDownStyle: {
        marginLeft: -DEVICE_WIDTH * 0.05,
        width: DEVICE_WIDTH * 0.8,
        height: DEVICE_HEIGHT * 0.5,
        borderRadius: (DEVICE_HEIGHT * 0.03) / 2,
        overflow: "hidden",
        marginTop: DEVICE_HEIGHT * 0.03
    },
    dropDownTextStyle: {
        fontSize: DEVICE_HEIGHT * 0.03,
        fontFamily: fonts.normal,
        alignSelf: "flex-start",
        color: colors.inputText
    },
    dropDownMainStyle: {
        paddingLeft: DEVICE_WIDTH * 0.05,
        justifyContent: "center",
        alignSelf: "center",
        marginTop: DEVICE_HEIGHT * 0.02,
        width: DEVICE_WIDTH * 0.8,
        borderRadius: (DEVICE_HEIGHT * 0.07) / 2,
        height: DEVICE_HEIGHT * 0.07,
        backgroundColor: colors.inputField
    },
    dropDownTextStyleStyle: {
        paddingVertical: DEVICE_HEIGHT * 0.02,
        fontSize: (DEVICE_HEIGHT * 0.03) * .9,
        color: colors.inputText,
        fontFamily: fonts.normal
    },
    downArrowView: {
        position: "absolute",
        top: DEVICE_HEIGHT * 0.04,
        right: DEVICE_WIDTH * 0.1 + 20
    },
    downArrow: { width: DEVICE_HEIGHT * 0.03, height: DEVICE_HEIGHT * 0.03 },
    textInput: {
        textAlignVertical: "top",
        paddingVertical: DEVICE_HEIGHT * 0.02,
        paddingHorizontal: DEVICE_HEIGHT * 0.02,
        marginTop: DEVICE_HEIGHT * 0.05,
        alignSelf: "center",
        borderRadius: (DEVICE_HEIGHT * 0.07) / 2,
        width: DEVICE_WIDTH * 0.8,
        height: DEVICE_HEIGHT * 0.25,
        backgroundColor: colors.inputField,
        fontSize: DEVICE_HEIGHT * 0.03,
        fontFamily: fonts.normal,
        color: colors.inputText
    },
    uploadImage: {
        marginTop: DEVICE_HEIGHT * .01,
        fontFamily: fonts.light,
        color: colors.black,
        fontSize: DEVICE_HEIGHT * 0.02
    },
    uploadImageView: {
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        marginTop: 5
    },
    blueText: {
        textAlign: "center",
        alignItems: "center",
        alignContent: "center",
        fontFamily: fonts.normal,
        color: colors.blueColor,
        fontSize: DEVICE_HEIGHT * 0.02
    },
    blueTextView: {
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        marginTop: DEVICE_HEIGHT * 0.1
    },

    profilePic: {

        marginTop: DEVICE_HEIGHT * .01,
        height: DEVICE_HEIGHT * 0.17,
        width: DEVICE_HEIGHT * 0.17,
        borderRadius: (DEVICE_HEIGHT * 0.17) / 2
    },
    iconsTouchable: {
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 5
        },
        elevation: 3,
        alignSelf: "center",
        alignContent: "center"
    },
    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.1 + 20 : DEVICE_HEIGHT * 0.1,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        justifyContent: "center",
        marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03
    },

    img_background: {
        flexDirection: "column",
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT,
        alignItems: "center",
        alignContent: "center"
    },

    rowView: {
        justifyContent: "space-around",
        flexDirection: "row",
        width: DEVICE_WIDTH,
        height: (DEVICE_HEIGHT - DEVICE_HEIGHT * 0.22 - 60) / 4
    },
    notiIcon: {
        height: DEVICE_HEIGHT * 0.1,
        width: DEVICE_WIDTH * 0.1,
        zIndex: 1
    },
    notiText: {
        color: colors.white,
        fontSize: 10,
        alignSelf: "center"
    },
    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        height: DEVICE_HEIGHT * 0.05,
        width: DEVICE_WIDTH * 0.05
    },

    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03,
        alignSelf: "center",
        alignContent: "center"
    }
});
const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode)
};

const mapStateToProps = state => ({
    login: state.login
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Dashboard);
