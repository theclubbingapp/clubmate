import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Text,
    TouchableOpacity,
    BackHandler, ActivityIndicator,
    Keyboard,
    Alert,
    FlatList,
    AsyncStorage, Linking,
    ScrollView, Slider, Modal,
    Platform, LayoutAnimation,
    ImageBackground, KeyboardAvoidingView,
    Dimensions, UIManager
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { SearchBar } from 'react-native-elements';
import ModalDatePicker from "react-native-datepicker-modal";
import DateTimePicker from 'react-native-modal-datetime-picker';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import back from "../../assets/back.png";
import {
    getStatusBarHeight,
    getBottomSpace,
    ifIphoneX
} from "react-native-iphone-x-helper";
import BlueButton from '../common/BlueButton'
import { fonts, colors } from "../../theme";
import menu from "../../assets/menu.png";
import list from "../../assets/list.png";
import demo from "../../assets/package_demo.png"
import all_persons from "../../assets/all_persons.png"
import { Dropdown } from 'react-native-material-dropdown';
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import { Rating, AirbnbRating } from 'react-native-elements';
import Loading from 'react-native-whc-loading'
import Slideshow from '../common/SlideShow'
import { TextInputLayout } from 'rn-textinputlayout';
import { CheckBox } from 'react-native-elements'
import DeviceInfo from 'react-native-device-info';
import my_location from "../../assets/my_location.png";
var CustomLayoutSpring = {
    duration: 400,
    create: {
        type: LayoutAnimation.Types.spring,
        property: LayoutAnimation.Properties.scaleXY,
        springDamping: 0.7,
    },
    update: {
        type: LayoutAnimation.Types.spring,
        springDamping: 0.7,
    },
};
class BarDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            minimumDate: new Date(),
            isFromDrawer: false,
            isDateTimePickerVisible: false,
            isTimePickerVisible: false,
            time: 'HH:MM',
            date: 'DD-MM-YYYY',
            getWhen: '',
            type: '',
            placeId: this.props.navigation.state.params['placeId'],
            isLoading: false,
            isLoadingRegestration: false,
            response: '',
            mytoken: this.props.navigation.state.params['mytoken'],
            name: '',
            place: '',
            city: '',
            interval: null,
            position: 1,
            dataSource: [],
            openAddMembers: false,
            number: '',
            username: '',
            maleOrFemale: '',
            eventCount: '',
            isBroadcast: false,
            offeringADrink: [
                { id: 1, value: "Offering Drink", title: "Offering", isChecked: false },
                { id: 2, value: "Requesting Drink", title: "Requesting", isChecked: false },
                { id: 3, value: "None", title: "None", isChecked: true },
            ],
            isLoadingCount: false,
            requestaDrinkText: 'None',
            latGoogle: '',
            lngGoogle: ''


        };
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        }
        console.log('place details' + this.state.placeId)
    }
    async componentWillMount() {
        this.setState({
            placeId: this.props.navigation.state.params['placeId']
        })
        this.callPlaceDetails();
        this.getCount();
        this.setState({
            interval: setInterval(() => {
                this.setState({
                    position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
                });
            }, 2000)
        });
    }
    getCount = () => {
        let formData = new FormData()
        formData.append('club_place_id', this.state.placeId)
        formData.append('authtoken', this.state.mytoken)
        formData.append('timezone', DeviceInfo.getTimezone())
        console.log('event count formData' + JSON.stringify(formData))
        this.setState({
            isLoadingCount: true
        })
        axios.post(APIURLCONSTANTS.EVENT_COUNT, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    isLoadingCount: false
                })
                console.log('event count' + JSON.stringify(res))
                this.setState({
                    eventCount: res['data']['data'][0]
                })

            })
            .catch(error => {
                this.setState({
                    isLoadingCount: false
                })
            });
    }


    linkToGoogleMap = () => {
        const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
        const latLng = `${this.state.latGoogle},${this.state.lngGoogle}`;
        const label = this.state.response['data']['result']['name'];
        const url = Platform.select({
            ios: `${scheme}${label}@${latLng}`,
            android: `${scheme}${latLng}(${label})`
        });


        Linking.openURL(url);
    }

    // handleOfferingADrinkInElement = (name, isChecked) => {
    //     let offeringADrink = this.state.offeringADrink
    //     offeringADrink.forEach(item => {
    //         if (item.value === name)
    //             item.isChecked = !isChecked
    //     })
    //     this.setState({ offeringADrink: offeringADrink })
    // }


    handleOfferingADrinkInElement = (name, isChecked) => {
        let offeringADrink = this.state.offeringADrink
        offeringADrink.forEach(item => {
            if (item.value === name) {
                item.isChecked = !isChecked
            } else {
                item.isChecked = false;
            }

            this.setState({
                requestaDrinkText: name
            })

        })
        this.setState({ offeringADrink: offeringADrink })
    }
    // handleRequestingADrinkInElement = (name, isChecked) => {
    //     let offeringADrink = this.state.requestingADrink
    //     offeringADrink.forEach(item => {
    //         if (item.value === name)
    //             item.isChecked = !isChecked
    //     })
    //     this.setState({ requestingADrink: offeringADrink })
    // }
    callPlaceDetails = () => {
        this.setState({
            isLoading: true
        })
        console.log('place details' + 'Strt')
        var Url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + this.props.navigation.state.params['placeId'] + "&fields=address_component,adr_address,alt_id,formatted_address,geometry,icon,id,name,permanently_closed,photo,place_id,plus_code,scope,type,url,user_ratings_total,utc_offset,vicinity,rating&key=" + APIURLCONSTANTS.KEY + "&sessiontoken=" + this.state.mytoken
        axios.get(Url)
            .then(res => {
                this.setState({
                    isLoading: false
                })
                console.log('place details bar details' + JSON.stringify(res))
                if (res['data']['result'] != null && res['data']['result'] != undefined && res['data']['result'] != '') {
                    this.setState({
                        response: res
                    })
                    let data = [];
                    for (var i = 0; i < res['data']['result']['photos'].length; i++) {
                        data.push({
                            title: '',
                            caption: '',
                            url: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + res['data']['result']['photos'][i]['photo_reference'] + "&key=" + APIURLCONSTANTS.KEY,
                        }
                        )
                    }
                    if (res['data']['result']['geometry']['location'] != null) {
                        this.setState({
                            latGoogle: res['data']['result']['geometry']['location']['lat'],
                            lngGoogle: res['data']['result']['geometry']['location']['lng']
                        })
                    }

                    this.setState({
                        dataSource: data
                    })
                    console.log('imagesssss' + JSON.stringify(data))
                }
            }).catch(err => {
                // this.refs.loading.close();
                this.setState({
                    isLoading: false
                })
            });
    }
    componentDidMount() {
    }
    componentDidUpdate() {
    }
    addMemeber = () => {
        this.setState({
            openAddMembers: true
        })
    }
    componentWillUnmount() {
        clearInterval(this.state.interval);
    }
    onClickBack = () => {
        this.props.navigation.goBack();
    }
    openDrawer = () => {
        this.props.navigation.openDrawer();
    };
    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
    _handleDatePicked = (date) => {
        let timing = "";
        let allTime = new Date(date);
        let month = String(allTime.getMonth() + 1)

        var tempMonth = ''
        var tempDate = ''
        console.log('month length' + JSON.stringify(month.length))

        if (month.length == 1) {
            tempMonth = '0' + month
            console.log('month picked with zero' + JSON.stringify(tempMonth))
        } else {
            tempMonth = month
        }
        if (String(allTime.getDate()).length == 1) {
            tempDate = '0' + allTime.getDate()
            console.log('tempDate with zero picked' + JSON.stringify(tempDate))
        } else {
            tempDate = allTime.getDate()
        }
        console.log('date picked' + JSON.stringify(allTime))



        timing = tempDate + "-" + tempMonth + "-" + allTime.getFullYear();
        this.setState({
            date: timing
        })
        this._hideDateTimePicker();
    };
    _showTimePicker = () => this.setState({ isTimePickerVisible: true });
    _hideTimePicker = () => this.setState({ isTimePickerVisible: false });
    _handlePicked = (time) => {
        console.log('A date has been picked: ', time);
        let timing = "";
        let allTime = new Date(time);
        var tempHours = ''
        var tempMinutes = ''

        if (String(allTime.getHours()).length == 1) {
            tempHours = '0' + allTime.getHours()
        } else {
            tempHours = allTime.getHours()
        }
        if (String(allTime.getMinutes()).length == 1) {
            tempMinutes = '0' + allTime.getMinutes()
        } else {
            tempMinutes = allTime.getMinutes()
        }

        timing = tempHours + ":" + tempMinutes
        this.setState({
            time: timing
        })
        this._hideTimePicker();
    };
    goToUserList = () => {
        const { navigation } = this.props;
        navigation.navigate('UserList', { placeId: this.state.placeId, mytoken: this.state.mytoken });
    }
    getValue = ({ item, index }) => {
        this.setState({
            getWhen: item
        })
    }
    getValueType = ({ item, index }) => {
        this.setState({
            type: item
        })
    }
    retry = () => {
        this.callPlaceDetails();
    }

    handleBroadcastElement = (isChecked) => {
        let formData = new FormData();
        formData.append('authtoken', this.state.mytoken)
        axios.post(APIURLCONSTANTS.PREMIUMSTATUS, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('premium', JSON.stringify(res['data']))
                if (res['data']['premium_status'] == true || res['data']['premium_status'] == 'true') {
                    this.setState({ isBroadcast: !isChecked })
                    // alert('Your entry will be sent to all users nearby')
                    Alert.alert("Broadcast", "Your entry will be sent to all users nearby");
                } else {
                    Alert.alert("Broadcast", 'Only premium user can use this feature');
                    // alert('Only premium user can use this feature')
                }
            })
            .catch(error => {
                console.log('response error' + JSON.stringify(error))
            });
    }


    registerForEvent = () => {
        let time = new Date();
        let goingtime = '';
        let tempHour = ''
        let tempMinute = ''
        if (String(time.getHours()).length == 1) {
            tempHour = '0' + time.getHours()
        } else {
            tempHour = time.getHours()
        }
        if (String(time.getMinutes()).length == 1) {
            tempMinute = '0' + time.getMinutes()

        } else {
            tempMinute = time.getMinutes()
        }
        goingtime = tempHour + ":" + tempMinute
        let formData = new FormData()
        formData.append('group_type', this.state.type)

        if (this.state.type == '') {
            Alert.alert('Please select group type')
            return;
        }

        if (this.state.getWhen == 'Today') {
            goingtime = this.state.time;
            if (goingtime != 'HH:MM') {
                formData.append('authtoken', this.state.mytoken)
                formData.append('type', 'Today')
                formData.append('club_place_id', this.state.placeId)
                let month = time.getMonth() + 1
                var tempMonth = ''
                var tempDate = ''

                if (String(month).length == 1) {
                    tempMonth = '0' + month
                } else {
                    tempMonth = month
                }
                if (String(time.getDate()).length == 1) {
                    tempDate = '0' + time.getDate()
                } else {
                    tempDate = time.getDate()
                }
                formData.append('date', tempDate + "-" + tempMonth + "-" + time.getFullYear())
                formData.append('time', goingtime)
            } else {
                Alert.alert('Please select Time')
                return;
            }
        }
        if (this.state.getWhen == 'Custom') {
            goingtime = this.state.time;
            if (this.state.date == 'DD-MM-YYYY') {
                Alert.alert('Please select Date')
                return;
            }
            if (this.state.time == 'HH:MM') {
                Alert.alert('Please select Time')
                return;
            }
            formData.append('authtoken', this.state.mytoken)
            formData.append('type', 'Custom')
            formData.append('club_place_id', this.state.placeId)
            formData.append('date', this.state.date)
            formData.append('time', goingtime)
        }
        if (this.state.getWhen == 'Now') {
            formData.append('authtoken', this.state.mytoken)
            formData.append('type', 'Now')
            formData.append('club_place_id', this.state.placeId)
            let month = time.getMonth() + 1

            var tempMonth = ''
            var tempDate = ''

            if (String(month).length == 1) {
                tempMonth = '0' + month
            } else {
                tempMonth = month
            }
            if (String(time.getDate()).length == 1) {
                tempDate = '0' + time.getDate()
            } else {
                tempDate = time.getDate()
            }

            formData.append('date', tempDate + "-" + tempMonth + "-" + time.getFullYear())
            formData.append('time', goingtime)
        }
        if (this.state.getWhen == '') {
            Alert.alert('Please select Date and Time')
            return;
        }


        formData.append('club_name', this.state.response['data']['result']['name'] + ' ' + this.state.response['data']['result']['vicinity'])
        formData.append('latitude', this.state.response['data']['result']['geometry']['location']['lat'])
        formData.append('longitude', this.state.response['data']['result']['geometry']['location']['lng'])
        formData.append('offering_a_drink', JSON.stringify(this.state.offeringADrink))
        formData.append('request_a_drink', this.state.requestaDrinkText)
        formData.append('multiple_notification', this.state.isBroadcast ? 1 : 2)
        formData.append('timezone', DeviceInfo.getTimezone())
        // formData.append('request_a_drink', JSON.stringify(this.state.requestingADrink))

        console.log('formData====' + JSON.stringify(formData))
        this.setState({
            isLoadingRegestration: true
        })
        axios.post(APIURLCONSTANTS.REGISTERFOREVENT, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('resgestration' + JSON.stringify(res))
                this.setState({
                    isLoadingRegestration: false
                })
                Alert.alert(
                    'Congratulations',
                    'Entry Registered Successfully',
                    [
                        {
                            text: 'OK', onPress: () => {
                                console.log('my sending type' + this.state.type)
                                // this.props.navigation.navigate('AddMembers', {
                                //     authtoken: this.state.mytoken,
                                //     registration_id: res['data']['data']['register_id'],
                                //     group_type: res['data']['data']['group_type'],
                                //     placeId: this.state.placeId

                                // })
                                if (this.state.type == 'Group') {
                                    this.props.navigation.navigate('AddMembers', {
                                        isFromDrawer: false,
                                        authtoken: this.state.mytoken,
                                        registration_id: res['data']['data']['register_id'],
                                        group_type: res['data']['data']['group_type'],
                                        placeId: this.state.placeId

                                    })
                                } else {
                                    this.goToUserList();
                                }
                                this.setState({
                                    time: 'HH:MM',
                                    date: 'DD-MM-YYYY',
                                    getWhen: '',
                                    type: '',
                                })

                            }
                        },
                    ],
                    { cancelable: true },
                );
            })
            .catch(error => {
                console.log('registered' + JSON.stringify(error))
                Alert.alert('Something went wrong!!')
                this.setState({
                    isLoadingRegestration: false
                })
            });
    }
    Capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
    render() {
        let data = [{
            value: 'Today',
        }, {
            value: 'Now',
        }, {
            value: 'Custom',
        }];
        let maleOrFemale = [{
            value: 'Male',
        }, {
            value: 'Female',
        }];
        let dataType = [{
            value: 'Single',
        }, {
            value: 'Group',
        }];
        let typeText = '';
        if (this.state.response != '') {
            if (this.state.response['data']['result']['types'].length > 0) {
                let itemType = this.state.response['data']['result']['types'][0]
                let days = String(itemType).split('_');

                if (days != null && days != '') {
                    for (let i = 0; i < days.length; i++) {
                        typeText = typeText + days[i] + ' '
                    }
                }
            }
        }

        return (
            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                <View style={styles.headerBackground}>
                    <View style={styles.dashBoardRow}>
                        {this.state.isFromDrawer ? (
                            <TouchableOpacity
                                onPress={this.openDrawer}
                                style={styles.menuTouch}
                            >
                                <Image
                                    source={menu}
                                    style={styles.menuIcon}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        ) : (
                                <TouchableOpacity
                                    onPress={this.onClickBack}
                                    style={styles.menuTouch}
                                >
                                    <Image
                                        source={back}
                                        style={styles.menuIcon}
                                        resizeMode="contain"
                                    />
                                </TouchableOpacity>
                            )}
                        <View style={styles.dashBoardView}>
                            <Text allowFontScaling={false}  style={styles.header}>Club</Text>
                        </View>
                        <TouchableOpacity onPress={this.goToHistory} style={styles.notiTouch}>
                            {/* {this.state.response['data']['result']['photos'].length > 0 ? <Image
         source={{ uri: "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + this.state.response['data']['result']['photos']['0']['photo_reference'] + "&key=" + APIURLCONSTANTS.KEY }}
         style={{
         tintColor: colors.white,
         height: DEVICE_HEIGHT * 0.05,
         width: DEVICE_WIDTH * 0.05
         }}
         resizeMode="contain"
         /> : */}
                            <Image
                                source={list}
                                style={{
                                    tintColor: colors.white,
                                    height: DEVICE_HEIGHT * 0.05,
                                    width: DEVICE_WIDTH * 0.05
                                }}
                                resizeMode="contain"
                            />
                            {/* } */}
                        </TouchableOpacity>
                    </View>
                </View>
                {this.state.isLoading ?
                    <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                        <ActivityIndicator
                            style={{ alignSelf: 'center' }}
                            size={'large'}
                            color={colors.primaryColor}
                        ></ActivityIndicator>
                    </View>
                    :
                    <ScrollView
                        contentContainerStyle={{ paddingBottom: DEVICE_HEIGHT * .03, alignContent: 'center' }}
                    >
                        {this.state.response == '' ?
                            <View style={{ height: DEVICE_HEIGHT, alignSelf: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                                <TouchableOpacity style={{ alignSelf: 'center' }} onPress={this.retry}>
                                    <Text  allowFontScaling={false} style={{ color: colors.primaryColor, fontSize: DEVICE_HEIGHT * .05 }}>
                                        Retry
   </Text>
                                </TouchableOpacity>
                            </View>
                            :
                            <View>
                                {this.state.response['data']['result']['photos'] ?
                                    <View>
                                        {this.state.response['data']['result']['photos'].length > 0 ?
                                            <Slideshow
                                                height={DEVICE_HEIGHT * .5}
                                                dataSource={this.state.dataSource}
                                                position={this.state.position}
                                                onPositionChanged={position => this.setState({ position })} />
                                            :
                                            <ImageBackground source={demo} style={{ width: DEVICE_WIDTH, height: DEVICE_HEIGHT * .3 }}>
                                            </ImageBackground>
                                        }
                                    </View>
                                    :
                                    <ImageBackground source={demo} style={{ width: DEVICE_WIDTH, height: DEVICE_HEIGHT * .3 }}>
                                    </ImageBackground>
                                }
                                <View style={{ paddingHorizontal: DEVICE_WIDTH * .07, paddingTop: DEVICE_HEIGHT * .02 }}>
                                    <Text  allowFontScaling={false} style={{ fontSize: DEVICE_HEIGHT * .04, color: colors.textColor, fontFamily: fonts.normal }}>
                                        {this.state.response['data']['result']['name']}
                                    </Text>

                                    <View style={{ marginTop: DEVICE_HEIGHT * .03, flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <View style={{}}>
                                            <Text allowFontScaling={false} 
                                                numberOfLines={3}
                                                style={{ lineHeight: (DEVICE_HEIGHT * .03) * 1, width: DEVICE_WIDTH * .6, fontSize: (DEVICE_HEIGHT * .03) * .9, color: colors.textColor, fontFamily: fonts.bold }}>
                                                {this.state.response['data']['result']['vicinity']}
                                            </Text>

                                            <Text
                                                numberOfLines={2}
                                                style={{ marginTop: DEVICE_HEIGHT * .01 / 2, lineHeight: (DEVICE_HEIGHT * .03) * 1, width: DEVICE_WIDTH * .6, fontSize: (DEVICE_HEIGHT * .03) * .9, color: colors.textColor, fontFamily: fonts.bold }}>
                                                {'Place type: ' + this.Capitalize(typeText)}
                                            </Text>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Rating
                                                    imageSize={(DEVICE_HEIGHT * .03) * .6}
                                                    readonly
                                                    startingValue={this.state.response['data']['result']['rating']}
                                                    style={{ alignSelf: 'center' }}
                                                />
                                                <Text
                                                    numberOfLines={2}
                                                    style={{ marginLeft: DEVICE_WIDTH * .03, lineHeight: (DEVICE_HEIGHT * .03) * 1, fontSize: (DEVICE_HEIGHT * .03) * .9, color: colors.textColor, fontFamily: fonts.bold }}>
                                                    {"( " + this.state.response['data']['result']['user_ratings_total'] + " )"}
                                                </Text>
                                            </View>

                                            <TouchableOpacity onPress={this.linkToGoogleMap} style={{ flexDirection: 'row', marginTop: DEVICE_HEIGHT * .01 }}>
                                                <Image
                                                    source={my_location}
                                                    style={{ height: (DEVICE_HEIGHT * .03) * 1, width: (DEVICE_HEIGHT * .03) * 1 }}
                                                >

                                                </Image>
                                                <Text
                                                    numberOfLines={1}
                                                    style={{ lineHeight: (DEVICE_HEIGHT * .03) * 1, fontSize: (DEVICE_HEIGHT * .03) * .9, color: colors.textColor, fontFamily: fonts.bold }}>
                                                    {'Navigate'}
                                                </Text>

                                            </TouchableOpacity>

                                            {/* <Text style={{ marginTop: DEVICE_HEIGHT * (.01) * .8, fontSize: (DEVICE_HEIGHT * .03) * .4, color: colors.textColor, fontFamily: fonts.normal }}>
         Mohali (India)</Text> */}
                                        </View>
                                        <View style={{ marginTop: DEVICE_HEIGHT * .01 }}>
                                            <View style={{ justifyContent: 'flex-start', flexDirection: 'row', }}>
                                                <View>
                                                    {this.state.isLoadingCount ?
                                                        <View style={{ height: DEVICE_HEIGHT * 0.05, marginBottom: DEVICE_HEIGHT * .01 }}>

                                                            <ActivityIndicator
                                                                style={{ alignSelf: 'center' }}
                                                                size={'small'}
                                                                color={colors.primaryColor}
                                                            ></ActivityIndicator>
                                                        </View>
                                                        :
                                                        <Text style={{ alignSelf: 'center', marginBottom: DEVICE_HEIGHT * .01, height: DEVICE_HEIGHT * 0.05, fontSize: (DEVICE_HEIGHT * .03) * .7, color: colors.textColor, fontFamily: fonts.bold }}>
                                                            {this.state.eventCount}</Text>
                                                    }

                                                </View>
                                                <View style={{ marginLeft: 5, height: DEVICE_HEIGHT * 0.05, marginTop: -DEVICE_HEIGHT * .02 }}>
                                                    <Image
                                                        source={all_persons}
                                                        style={{
                                                            height: DEVICE_HEIGHT * 0.05,
                                                            width: DEVICE_WIDTH * 0.05
                                                        }}
                                                        resizeMode="contain"
                                                    />
                                                </View>
                                            </View>
                                            <View style={{ alignSelf: 'center', marginTop: -DEVICE_HEIGHT * .02 }}>
                                                <BlueButton
                                                    onPress={this.goToUserList}
                                                    style={{
                                                        borderRadius: 5,
                                                        backgroundColor: colors.primaryColor,
                                                        width: DEVICE_WIDTH * .25, height: DEVICE_HEIGHT * .04
                                                    }}
                                                    textStyles={{ fontSize: DEVICE_HEIGHT * .03, color: colors.white }}>
                                                    View all
   </BlueButton>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                <View style={{ justifyContent: 'center', alignContent: 'center', alignSelf: 'center', marginTop: DEVICE_HEIGHT * .03 }}>
                                    <Text style={{ fontSize: (DEVICE_HEIGHT * .03) * 1, color: colors.black, fontFamily: fonts.bold }}>
                                        Make an Entry</Text>
                                    <Dropdown
                                        containerStyle={{ width: DEVICE_WIDTH * .85 }}
                                        // itemTextStyle={{fontFamily:fonts.normal,color:colors.primaryColor}}
                                        // itemColor={colors.gray}
                                        selectedItemColor={colors.primaryColor}
                                        // textColor={colors.primaryColor}
                                        // value={data[0].value}
                                        baseColor={colors.primaryColor}
                                        // textColor={colors.primaryColor}
                                        fontSize={DEVICE_HEIGHT * .02}
                                        label='Type'
                                        data={dataType}
                                        onChangeText={(item) => {
                                            LayoutAnimation.configureNext(CustomLayoutSpring)
                                            this.setState({ type: item })
                                        }}
                                    />
                                    <Dropdown
                                        containerStyle={{ width: DEVICE_WIDTH * .85 }}
                                        // itemTextStyle={{fontFamily:fonts.normal,color:colors.primaryColor}}
                                        // itemColor={colors.gray}
                                        selectedItemColor={colors.primaryColor}
                                        // textColor={colors.primaryColor}
                                        // value={data[0].value}
                                        baseColor={colors.primaryColor}
                                        // textColor={colors.primaryColor}
                                        fontSize={DEVICE_HEIGHT * .02}
                                        label='Select'
                                        data={data}
                                        onChangeText={(item) => {
                                            LayoutAnimation.configureNext(CustomLayoutSpring)
                                            this.setState({ getWhen: item })
                                        }}
                                    />
                                    {this.state.getWhen == 'Custom' ?
                                        <View style={{ justifyContent: 'center', marginTop: DEVICE_HEIGHT * .02 }}>
                                            <TouchableOpacity style={[styles.modelDatePickerStyle, { justifyContent: 'flex-end' }]} onPress={this._showDateTimePicker}>
                                                <Text style={[styles.modelDatePickerStringStyle]}>{String(this.state.date)}</Text>
                                            </TouchableOpacity>
                                            <DateTimePicker
                                                datePickerModeAndroid={'spinner'}
                                                minimumDate={this.state.minimumDate}
                                                isVisible={this.state.isDateTimePickerVisible}
                                                onConfirm={this._handleDatePicked}
                                                onCancel={this._hideDateTimePicker}
                                            />
                                            <View style={{ width: DEVICE_WIDTH * .85, height: StyleSheet.hairlineWidth, backgroundColor: colors.primaryColor }}>
                                            </View>
                                        </View>
                                        :
                                        null
                                    }
                                    {this.state.getWhen == 'Today' || this.state.getWhen == 'Custom' ?
                                        <View style={{ justifyContent: 'center', marginTop: DEVICE_HEIGHT * .02 }}>
                                            <TouchableOpacity style={[styles.modelDatePickerStyle, { justifyContent: 'flex-end' }]} onPress={this._showTimePicker}>
                                                <Text style={[styles.modelDatePickerStringStyle]}>{String(this.state.time)}</Text>
                                            </TouchableOpacity>
                                            <DateTimePicker
                                                datePickerModeAndroid={'spinner'}
                                                is24Hour={true}
                                                mode='time'
                                                isVisible={this.state.isTimePickerVisible}
                                                onConfirm={this._handlePicked}
                                                onCancel={this._hideTimePicker}
                                            />
                                            <View style={{ width: DEVICE_WIDTH * .85, height: StyleSheet.hairlineWidth, backgroundColor: colors.primaryColor }}>
                                            </View>
                                        </View>
                                        :
                                        null
                                    }

                                    {/* {this.state.type == 'Group' ?
                                        <View style={{ marginTop: DEVICE_HEIGHT * .05, alignSelf: 'flex-start' }}>
                                            <BlueButton
                                                onPress={this.addMemeber}
                                                style={{
                                                    backgroundColor: colors.primaryColor,
                                                    width: DEVICE_WIDTH * .2, height: DEVICE_HEIGHT * .04
                                                }}
                                                textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                                                Add Member
                                         </BlueButton>
                                        </View> :
                                        null
                                    } */}
                                    <View style={{ alignSelf: 'flex-start', marginLeft: -DEVICE_WIDTH * .05 }}>
                                        <CheckBox
                                            title={'Broadcast'}
                                            containerStyle={{ borderColor: 'transparent', padding: 0, marginTop: DEVICE_HEIGHT * .03 }}
                                            checked={this.state.isBroadcast}
                                            checkedColor={colors.primaryColor}
                                            onPress={() => this.handleBroadcastElement(this.state.isBroadcast)}
                                            size={DEVICE_HEIGHT * 0.03 * .8}
                                            textStyle={[styles.modelDatePickerStringStyle,]}
                                        />
                                    </View>
                                    <View style={{ alignSelf: 'flex-start', marginLeft: -DEVICE_WIDTH * .05 }}>
                                        <Text style={[styles.modelDatePickerStringStyle, { fontSize: (DEVICE_HEIGHT * .03) * 1, color: colors.black, fontFamily: fonts.bold }, { marginLeft: DEVICE_WIDTH * .05, marginTop: DEVICE_HEIGHT * .02, marginBottom: DEVICE_HEIGHT * .01 }]}>
                                            Drink
                                </Text>
                                        <FlatList
                                            style={{ flexGrow: 0, width: DEVICE_WIDTH * .90, }}
                                            data={this.state.offeringADrink}
                                            renderItem={({ item }) => (
                                                <CheckBox
                                                    checkedIcon='dot-circle-o'
                                                    uncheckedIcon='circle-o'
                                                    // handleCheckChieldElement={this.handleCheckChieldElement}
                                                    title={item.title}
                                                    containerStyle={{ backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                                    checked={item.isChecked}

                                                    checkedColor={colors.primaryColor}
                                                    onPress={() => { this.handleOfferingADrinkInElement(item.value, item.isChecked) }}
                                                    size={DEVICE_HEIGHT * 0.03 * .8}
                                                    textStyle={[styles.modelDatePickerStringStyle,]}
                                                />
                                            )}
                                            extraData={this.state}
                                            horizontal={true}
                                            scrollEnabled={false}
                                            showsHorizontalScrollIndicator={false}
                                            keyExtractor={(item, index) => index}

                                        />
                                    </View>


                                    <View style={{ marginTop: DEVICE_HEIGHT * .05, alignSelf: 'center' }}>
                                        {this.state.isLoadingRegestration ?
                                            <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .05 }}>
                                                <ActivityIndicator
                                                    style={{ alignSelf: 'center' }}
                                                    size={'small'}
                                                    color={colors.primaryColor}
                                                ></ActivityIndicator>
                                            </View>
                                            :
                                            <BlueButton
                                                onPress={this.registerForEvent}
                                                style={{
                                                    borderRadius: 5,
                                                    backgroundColor: colors.primaryColor,
                                                    width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .05
                                                }}
                                                textStyles={{ fontSize: DEVICE_HEIGHT * .03, color: colors.white }}>
                                                SUBMIT
   </BlueButton>
                                        }
                                    </View>
                                </View>
                            </View>
                        }
                    </ScrollView>
                }
                {/* <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.openAddMembers}
                    onRequestClose={() => {
                        this.setState({
                            openAddMembers: false
                        })
                    }}>
                    <View style={{
                        height: DEVICE_HEIGHT,
                        backgroundColor: 'rgba(255, 255, 255, 1)',
                        ...ifIphoneX(
                            {
                                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                            },
                            {
                                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                                paddingTop:
                                    Platform.OS == "ios"
                                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                                        : DEVICE_HEIGHT * 0.02
                            }
                        )
                    }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: DEVICE_WIDTH, height: DEVICE_HEIGHT * .06, backgroundColor: colors.primaryColor }}>
                            <Text style={{ textAlign: 'center', flexGrow: 10, alignSelf: 'center', fontFamily: fonts.bold, color: colors.white, fontSize: DEVICE_HEIGHT * .04 }}>
                                Add Members
                       </Text>
                            <TouchableHighlight style={{ right: 5, position: 'absolute', alignSelf: 'center', paddingRight: 10 }} onPress={() => {
                                this.setState({
                                    openAddMembers: false
                                })
                            }}>
                                <Text style={{
                                    alignSelf: 'center',
                                    marginLeft: DEVICE_WIDTH * .02,
                                    fontFamily: fonts.normal,
                                    fontSize: DEVICE_HEIGHT * 0.03,
                                    // height: DEVICE_HEIGHT * 0.06,
                                    color: colors.white
                                }}>
                                    Close
      </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={{ marginTop: DEVICE_HEIGHT * .05, alignSelf: 'center' }}>
                            <TextInputLayout
                                hintColor={colors.gray}
                                errorColor={colors.black}
                                focusColor={colors.primaryColor}
                                style={styles.mainInputLayout}
                            >
                                <TextInput
                                    onChangeText={(username) =>
                                        this.setState({ username: username })}
                                    style={styles.inputLayout}
                                    placeholder={"Name"}
                                    autoCorrect={false}
                                    autoCapitalize={"none"}
                                    returnKeyType={"done"}
                                    placeholderTextColor={colors.gray}
                                    underlineColorAndroid="transparent"
                                />
                            </TextInputLayout>
                        </View>
                       
                    </View>
                </Modal> */}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    mainInputLayout: {
        width: DEVICE_WIDTH * .85,
    },
    inputLayout: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.02,
        height: DEVICE_HEIGHT * 0.06,
        color: colors.black
    },
    modelDatePickerStyle: {
        width: DEVICE_WIDTH * .85,
        height: DEVICE_HEIGHT * 0.05,
    }
    ,
    modelDatePickerPlaceholderStyle: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.02,
        color: colors.gray,
    }
    ,
    modelDatePickerStringStyle: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.03 * .8,
        color: colors.textColor,
    },
    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.1 + 20 : DEVICE_HEIGHT * 0.1,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        justifyContent: "center",
        // marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03
    },
    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.03,
        width: DEVICE_WIDTH * 0.03
    },
    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03,
        alignSelf: "center",
        alignContent: "center"
    }
});
const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode)
};
const mapStateToProps = state => ({
    login: state.login
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BarDetails);