import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableHighlight,
    Text,
    TouchableOpacity,
    BackHandler,
    Keyboard,
    Alert,
    FlatList,
    AsyncStorage, ActivityIndicator,
    ScrollView,
    Platform,Modal,
    ImageBackground, KeyboardAvoidingView,
    Dimensions
} from "react-native";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import { getPackages } from "../../actions";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import ModalDropdown from "react-native-modal-dropdown";
import {
    getStatusBarHeight,
    getBottomSpace,
    ifIphoneX
} from "react-native-iphone-x-helper";
import CardView from 'react-native-cardview'
import { fonts, colors } from "../../theme";
import * as Animatable from 'react-native-animatable';
const AnimatableFlatList = Animatable.createAnimatableComponent(FlatList);
import LoadingSpinner from '../common/loadingSpinner/index';
import PackageItemLoading from "./PackageItemLoading";
import UserListItem from "./userListItem";
import back from "../../assets/back.png";
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import RazorpayCheckout from 'react-native-razorpay';
import moment from 'moment'
class Packages extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFromDrawer: false,
            avatarSource: null,
            data: [],
            isLoading: false,
            page: 1,
            refreshing: false,
            number: '',
            username: '',
            detailVisible:false,
            itemName:'',
            itemPrice:'',
            itemTaxId:'',
            active_plns:'',
            expiry_date:'',
            totalCalls:'',
            totalUsers:'',
            
        };
    }

    componentWillMount() {
        this.getNumber();
        try {
            AsyncStorage.getItem(keys.token).then((value) => {
                console.log("token value" + JSON.stringify(value))
                this.setState({
                    mytoken: value,
                })

                let formData = new FormData()
                formData.append('authtoken', this.state.mytoken)
                this.setState({
                    isLoading: true
                })
                console.log('form dtaa' + JSON.stringify(formData))
                axios.post(APIURLCONSTANTS.GETPLANS, formData, null)
                    .then(ApiUtils.checkStatus)
                    .then(res => {
                        console.log('response dtaa' + JSON.stringify(res))
                        this.setState({
                            isLoading: false,

                        })
                        if (res['data']['data'].length > 0) {
                            this.setState({
                                data: res['data']['data']
                            })
                            this.setState({
                                active_plns:res['data']['active_plns'],
                                expiry_date:res['data']['expiry_date'],
                                totalCalls:res['data']['total_calls'],
                                totalUsers:res['data']['total_users'],
                            })
                        }

                    })
                    .catch(error => {
                        console.log('response error' + JSON.stringify(error))
                        this.setState({
                            isLoading: false
                        })

                    });

            });
        } catch (err) {
            console.log("token getting" + JSON.stringify(err))
            this.setState({ mytoken: '' })
        }
    }

    async getNumber() {
        let number = await AsyncStorage.getItem(keys.phone);
        let username = await AsyncStorage.getItem(keys.username);
        this.setState({
            number: number,
            username: username

        })

        if (this.state.number == null || this.state.number == '') {
            this.getNumber();
        }

        console.log('my token is valid' + this.state.mytoken)
    }

    componentDidMount() {
        this.mounted = true


    }

    componentWillReceiveProps(nextProps) {



    }

    componentDidUpdate() { }






    onClickBack = () => {
        this.props.navigation.goBack();
    };

    openDrawer = () => {
        this.props.navigation.openDrawer();
    };

    retry = () => {
        let formData = new FormData()
        formData.append('authtoken', this.state.mytoken)
        this.setState({
            isLoading: true,
            data: ''
        })
        console.log('form dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.GETPLANS, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('response dtaa' + JSON.stringify(res))
                this.setState({
                    isLoading: false,

                })
                if (res['data']['data'].length > 0) {
                    this.setState({
                        data: res['data']['data']
                    })
                    this.setState({
                        active_plns:res['data']['active_plns'],
                        expiry_date:res['data']['expiry_date'],
                        totalCalls:res['data']['total_calls'],
                        totalUsers:res['data']['total_users'],
                    })
                }

            })
            .catch(error => {
                console.log('response error' + JSON.stringify(error))
                this.setState({
                    isLoading: false
                })

            });
    }

    paymentFinal = (subscriptionplanmaster_id, amount, payment_id) => {
        let formData = new FormData()
        this.setState({
            isLoading: true,
        })
        formData.append('authtoken', this.state.mytoken)
        formData.append('subscriptionplanmaster_id', subscriptionplanmaster_id)
        formData.append('amount', amount)
        formData.append('payment_id', payment_id)
        formData.append('transaction_type', 2)
        console.log('form dtaa' + JSON.stringify(formData))
        axios.post(APIURLCONSTANTS.PAYMENT, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                console.log('response dtaa' + JSON.stringify(res))
                this.setState({
                    isLoading: false,
                })
                this.retry();
            })
            .catch(error => {
                this.setState({
                    isLoading: false,
                })
                console.log('response error' + JSON.stringify(error))


            });




    }

    payment = (item, index) => {

        if(item.price==0||item.price=='0'){
            // alert('This is free plan')
            Alert.alert("This is free plan");
            return;
        }

        if(item.id==this.state.active_plns){
        //    alert('Already active')
           Alert.alert("Already active");
           return;
        }
        
        var options = {
            description: item.description,
            // image: 'https://i.imgur.com/3g7nmJC.png',
            currency: 'INR',
            key: APIURLCONSTANTS.RAZORPAY,
            amount: item.price + '00',
            name: item.name,
            external: {
                wallets: ['paytm']
            },
            // order_id:'order_6JUYuvmgCLfgjY',
            prefill: {
                email: '',
                contact: this.state.number,
                name: this.state.username
            },
            theme: { color: colors.primaryColor }
        }
        RazorpayCheckout.open(options).then((data) => {
            this.setState({
                detailVisible:true
            })
            this.setState({
                itemPrice:item.price,
                itemTaxId:data.razorpay_payment_id,
                itemName:item.name
            })
            this.paymentFinal(item.id, item.price+'00', data.razorpay_payment_id)
        }).catch((error) => {
            alert(error.description);
        });
        RazorpayCheckout.onExternalWalletSelection(data => {
            alert('External Wallet Selected:' + JSON.stringify(data));
        });


    }


    // renderItem = ({ item, index }) => {
    //     return (
    //         <TouchableOpacity onPress={() => this.payment(item, index)} style={{ marginTop: DEVICE_HEIGHT * .03 }}>
    //             <CardView
    //                 style={{ paddingVertical: DEVICE_HEIGHT * .02, paddingHorizontal: DEVICE_WIDTH * .04, alignSelf: 'center', backgroundColor: colors.white, width: DEVICE_WIDTH * .90 }}
    //                 cardElevation={2}
    //                 cardMaxElevation={2}
    //                 cornerRadius={5}>
    //                 {item.id==this.state.active_plns?
    //                 <View>
    // <Text style={{ alignSelf: 'flex-start', fontSize: DEVICE_HEIGHT * .02,
    //                 fontFamily: fonts.bold, 
    //                 color: colors.green }}>
    //                       Active
    //                   </Text>
    //                   <Text style={{ alignSelf: 'flex-start', fontSize: DEVICE_HEIGHT * .02,
    //                 fontFamily: fonts.bold, 
    //                 color: colors.green }}>
    //                       {'Valid till: '+this.state.expiry_date}
    //                   </Text>
    //                 </View>
                
                      
    //                  :
                       
    //                  null}
                     
    //                 <View style={{ alignSelf: 'center' }}>
    //                     <Text style={{ alignSelf: 'center', fontSize: DEVICE_HEIGHT * .05, fontFamily: fonts.bold, color: colors.primaryColor }}>
    //                         {item.name}
    //                     </Text>
    //                     <Text style={{ textAlign:'center',alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.bold, color: colors.primaryColor }}>
    //                         {'Chat with ' + item.allowedUsers + ' users' + ' and ' + item.allowedCalls + ' allowed call'}
    //                     </Text>
    //                     {item.alloweBroadcast=='yes'?
    //                       <Text style={{ textAlign:'center',alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.bold, color: colors.primaryColor }}>
    //                       {'Unlimited Broadcast'}
    //                        </Text>:
    //                          null}
                      
    //                     <Text style={{ alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.bold, color: colors.primaryColor }}>
    //                         {'Rs. ' + item.price}
    //                     </Text>
    //                     <Text style={{  textAlign:'center',alignSelf: 'center', marginTop: DEVICE_HEIGHT * .03, fontSize: DEVICE_HEIGHT * .02 * .7, fontFamily: fonts.bold, color: colors.textColor }}>
    //                         {item.description}
    //                     </Text>
    //                 </View>
    //             </CardView>
    //         </TouchableOpacity>


    //     )

    // }

    renderItem = ({ item, index }) => {
        let date=moment(this.state.expiry_date, ["DD-MM-YYYY"]).format("ddd DD-MMM-YYYY")
        return (
            <TouchableOpacity onPress={() => this.payment(item, index)} style={{ marginTop: DEVICE_HEIGHT * .03 }}>
                <CardView
                    style={{ paddingVertical: DEVICE_HEIGHT * .02, paddingHorizontal: DEVICE_WIDTH * .04, alignSelf: 'center', backgroundColor: colors.white, width: DEVICE_WIDTH * .90 }}
                    cardElevation={2}
                    cardMaxElevation={2}
                    cornerRadius={5}>
<View style={{flexDirection:'row',justifyContent:'space-between'}}>



                    {item.id==this.state.active_plns?
                    <View>
    <Text allowFontScaling={false}  style={{ alignSelf: 'flex-start', fontSize: DEVICE_HEIGHT * .02,
                    fontFamily: fonts.bold, 
                    color: colors.green }}>
                          Active
                      </Text>
                      <Text allowFontScaling={false}  style={{ alignSelf: 'flex-start', fontSize: DEVICE_HEIGHT * .02,
                    fontFamily: fonts.bold, 
                    color: colors.green }}>
                          {'Valid till: '+date}
                      </Text>
                    </View>
                     :
                       
                     null}

           {item.id==this.state.active_plns?
                    <View>
                <Text allowFontScaling={false}  style={{ alignSelf: 'flex-start', fontSize: DEVICE_HEIGHT * .02,
                    fontFamily: fonts.bold, 
                    color: colors.green }}>
                         {'Call Used: '+this.state.totalCalls}
                      </Text>
                      <Text  allowFontScaling={false} style={{ alignSelf: 'flex-start', fontSize: DEVICE_HEIGHT * .02,
                    fontFamily: fonts.bold, 
                    color: colors.green }}>
                          {'Chat Used: '+this.state.totalUsers}
                      </Text>
                    </View>
                     :
                       
                     null}
                     </View>
                     
                    <View style={{ alignSelf: 'center' }}>
                         <Text  allowFontScaling={false} style={{ alignSelf: 'center', fontSize: DEVICE_HEIGHT * .05, fontFamily: fonts.bold, color: colors.primaryColor }}>
                            {item.name}
                         </Text>
                         {(item.allowedUsers=='Unlimited'||item.allowedUsers=='unlimited')?
                          <Text  allowFontScaling={false} style={{ textAlign:'center',alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.bold, color: colors.primaryColor }}>
                          {'Unlimited Chats'}
                       </Text>
                         :
                        null}

                       {(item.allowedUsers!='Unlimited'&&item.allowedUsers!='unlimited')?
                          <Text  allowFontScaling={false} style={{ textAlign:'center',alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.bold, color: colors.primaryColor }}>
                          {'Chat upto ' + item.allowedUsers + ' users'}
                       </Text>
                         :
                        null}
                         {/* <Text style={{ textAlign:'center',alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.bold, color: colors.primaryColor }}>
                            {'Chat upto ' + item.allowedUsers + ' users'}
                         </Text> */}
                         {(item.allowedCalls=='Unlimited' || item.allowedCalls=='unlimited')?
                         <Text  allowFontScaling={false} style={{ textAlign:'center',alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.bold, color: colors.primaryColor }}>
                         {'Unlimited Audio Calls'}
                         </Text>
                         :
                         null}
                         {(item.allowedCalls!='Unlimited' && item.allowedCalls!='unlimited'&&(item.allowedCalls=='0'||item.allowedCalls==0))?
                         <Text  allowFontScaling={false} style={{ textAlign:'center',alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.bold, color: colors.primaryColor }}>
                         {'No Audio Calls'}
                         </Text>
                         :
                         null}
                         {(item.allowedCalls!='Unlimited' && item.allowedCalls!='unlimited'&&(item.allowedCalls!='0'||item.allowedCalls!=0))?
                         <Text  allowFontScaling={false} style={{ textAlign:'center',alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.bold, color: colors.primaryColor }}>
                         {item.allowedCalls+' Audio Calls'}
                         </Text>
                         :
                         null}
                         
                        
                        {item.alloweBroadcast=='yes'?
                          <Text  allowFontScaling={false} style={{ textAlign:'center',alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.bold, color: colors.primaryColor }}>
                          {'Broadcast* Your Request'}
                           </Text>:
                              <Text  allowFontScaling={false} style={{ textAlign:'center',alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.bold, color: colors.primaryColor }}>
                              {'No Broadcasting'}
                               </Text>}
                      
                        <Text  allowFontScaling={false} style={{ alignSelf: 'center', marginTop: DEVICE_HEIGHT * .01, fontSize: DEVICE_HEIGHT * .03, fontFamily: fonts.bold, color: colors.primaryColor }}>
                            {'Rs. ' + item.price}
                        </Text>
                        <Text  allowFontScaling={false} style={{  textAlign:'center',alignSelf: 'center', marginTop: DEVICE_HEIGHT * .03, fontSize: DEVICE_HEIGHT * .02 * .7, fontFamily: fonts.bold, color: colors.textColor }}>
                            {item.description}
                        </Text>
                    </View>
                </CardView>
            </TouchableOpacity>


        )

    }

    onRefresh = () => {
        console.log('onRefresh()')
        if (this.mounted) {

        }
    }
    render() {
        const { packages: {
            response, isLoading, isSuccess
        } } = this.props
        return (
            <View style={{ backgroundColor: colors.white, ...StyleSheet.absoluteFillObject }}>
                <View style={{
                    flex: 1, ...ifIphoneX({
                        paddingBottom: getBottomSpace()
                    }, {
                            paddingBottom: getBottomSpace()

                        })
                }}>
                    {this.state.isLoading ?

                        <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                            <ActivityIndicator
                                style={{ alignSelf: 'center' }}
                                size={'large'}
                                color={colors.primaryColor}
                            ></ActivityIndicator>
                        </View>
                        :

                        <View style={{ flex: 1 }}>
                            {this.state.data == '' ?
                                <View style={{ height: DEVICE_HEIGHT, alignSelf: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                                    <TouchableOpacity style={{ alignSelf: 'center' }} onPress={this.retry}>
                                        <Text  allowFontScaling={false} style={{ color: colors.primaryColor, fontSize: DEVICE_HEIGHT * .05 }}>
                                            Retry
                                  </Text>
                                    </TouchableOpacity>
                                </View>
                                :

                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={{ paddingBottom: 10, paddingTop: 10 }}
                                    ref={(ref) => { this.listView = ref; }}
                                    data={this.state.data}
                                    keyExtractor={(item, index) => `${index} - ${item}`}
                                    renderItem={this.renderItem}
                                    extraData={this.state}
                                    //ListFooterComponent={() => this.renderFooter(isLoading)}
                                    // onEndReachedThreshold={0.1}
                                    // onEndReached={() => this.onEndReached(this.state.isLoading)}
                                    numColumns={1}
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.onRefresh}
                                />
                            }
                        </View>

                    }




                </View>

                <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.detailVisible}
                        onRequestClose={() => {
                            this.setState({
                                detailVisible:false
                            })
                        }}>
                        <View style={{
                            height: DEVICE_HEIGHT,
                            backgroundColor: 'rgba(255, 255, 255, 1)',
                            ...ifIphoneX(
                                {
                                    paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                                },
                                {
                                    // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                                    paddingTop:
                                        Platform.OS == "ios"
                                            ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                                            : 0
                                }
                            )
                            }}>
                               <View style={{flexDirection:'row',justifyContent:'space-between',width:DEVICE_WIDTH,height:DEVICE_HEIGHT*.06,backgroundColor:colors.primaryColor}}>
                         <Text  allowFontScaling={false} style={{textAlign:'center',flexGrow:10,alignSelf:'center',fontFamily:fonts.bold,color:colors.white,fontSize:DEVICE_HEIGHT*.04}}>
                                 Payment Successful
                         </Text>
                         <TouchableHighlight style={{right:5,position:'absolute',alignSelf: 'center',paddingRight:10}} onPress={() => {
                                  this.setState({
                                    detailVisible:false
                                  })
                                }}>
                                    <Text  allowFontScaling={false} style={{
                                        alignSelf: 'center',
                                        marginLeft: DEVICE_WIDTH * .02,
                                        fontFamily: fonts.normal,
                                        fontSize: DEVICE_HEIGHT * 0.03,
                                        // height: DEVICE_HEIGHT * 0.06,
                                        color: colors.white
                                    }}>
                                        Close
    
                                </Text>
                                </TouchableHighlight>

                             </View>
                            
                             <View style={{ marginTop: DEVICE_HEIGHT * .01, paddingHorizontal: DEVICE_WIDTH * .03, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}></View>


                             <View style={{ marginTop: DEVICE_HEIGHT * .02, paddingHorizontal: DEVICE_WIDTH * .03, alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                        <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                <Text style={styles.labelText}>Item Name</Text>
                            </View>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, justifyContent: 'center', alignContent: 'flex-start' }} >
                            <Text  allowFontScaling={false} style={styles.labelText}>{this.state.itemName}</Text>
                            </View>
                         
                        </View>
                        <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>
                  
                        <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                <Text  allowFontScaling={false} style={styles.labelText}>Item Price</Text>
                            </View>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, justifyContent: 'center',alignContent: 'flex-start'  }} >
                            <Text  allowFontScaling={false} style={styles.labelText}>{this.state.itemPrice}</Text>
                            </View>
                         
                        </View>
                        <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>

                        <View style={{ width: DEVICE_WIDTH * .95, flexDirection: 'row', height: DEVICE_HEIGHT * 0.06, }}>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 4, justifyContent: 'center', alignContent: 'flex-start' }}>
                                <Text  allowFontScaling={false} style={styles.labelText}>Tranasaction Id</Text>
                            </View>
                            <View style={{ height: DEVICE_HEIGHT * 0.06, flex: 6, justifyContent: 'center',alignContent: 'flex-start' }} >
                            <Text  allowFontScaling={false} style={styles.labelText}>{this.state.itemTaxId}</Text>
                            </View>
                         
                        </View>
                        <View style={{ width: DEVICE_WIDTH * .95, height: StyleSheet.hairlineWidth, backgroundColor: colors.gray }}></View>

                    </View>



                             </View>
                             </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({


    paginationView: {
        flex: 0,
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT * .05,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerBackground: {
        width: DEVICE_WIDTH,
        height:
            Platform.OS == "ios" ? DEVICE_HEIGHT * 0.1 + 20 : DEVICE_HEIGHT * 0.1,
        paddingHorizontal: DEVICE_WIDTH * 0.05,
        backgroundColor: colors.primaryColor,
        ...ifIphoneX(
            {
                paddingTop: getStatusBarHeight() + DEVICE_HEIGHT * 0.02
            },
            {
                // paddingTop: getStatusBarHeight() + DEVICE_HEIGHT*.02,
                paddingTop:
                    Platform.OS == "ios"
                        ? getStatusBarHeight() + DEVICE_HEIGHT * 0.02
                        : DEVICE_HEIGHT * 0.02
            }
        )
    },
    dashBoardRow: {
        flexDirection: "row",
        height: DEVICE_HEIGHT * 0.07,
        justifyContent: "space-between"
    },
    dashBoardView: {
        justifyContent: "center",
        marginTop: DEVICE_HEIGHT * 0.01
    },
    notiTouch: {
        justifyContent: "center",
        // marginTop: -(DEVICE_HEIGHT * 0.01)
    },
    backText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03
    },




    menuTouch: {
        justifyContent: "center"
    },
    menuIcon: {
        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.03,
        width: DEVICE_WIDTH * 0.03
    },

    header: {
        // marginLeft: 10,
        fontFamily: fonts.bold,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * 0.03,
        alignSelf: "center",
        alignContent: "center"
    }
});
const mapDispatchToProps = {
    dispatchGetPackages: () => getPackages(),
}

const mapStateToProps = state => ({
    packages: state.packages
})

export default connect(mapStateToProps, mapDispatchToProps)(Packages)
