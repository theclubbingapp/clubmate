import React, { Component } from 'react';
import {
    Platform,
    Text, ScrollView,
    View, FlatList,
    TextInput, Animated, TouchableWithoutFeedback,
    StyleSheet, ImageBackground,
    TouchableOpacity, TouchableHighlight,
    ActivityIndicator,
    Image,
    Modal, Dimensions
} from 'react-native';

const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import { fonts, colors } from '../../theme'
import CardView from 'react-native-cardview'
import moment from "moment";
export default class UserListItem extends Component {
    state = {
        otherMembers: ['+99999999', '+9999999999', '+99999999999', '+99999999999', '+99999999999']
    }
    componentDidMount() {
    }
    render() {
        const { navigation, item, index, onPress } = this.props;
        let _this = this;
        let offering_a_drink = '';
        if (item.offering_a_drink != "") {
            console.log(item.offering_a_drink)
            if (JSON.parse(item.offering_a_drink)[0]['isChecked']) {
                offering_a_drink = 'Offering Drink'
            }
        }
        let request_a_drink = '';
        if (item.offering_a_drink != "") {
            // console.log(item.offering_a_drink)
            if(JSON.parse(item.offering_a_drink).length>1){
                if (JSON.parse(item.offering_a_drink)[1]['isChecked']) {
                    request_a_drink = 'Requesting Drink'
                }
            }
            
        }
        let date=moment(item.date, ["DD-MM-YYYY"]).format("ddd DD-MMM-YYYY")
        let time=moment(item.time, ["HH:mm"]).format("hh:mm A")
        return (
            <TouchableOpacity onPress={() => onPress(item, index)} style={[styles.touchable, { marginTop: index > 0 ? 10 : 0 }]}>
                <CardView
                    style={{ backgroundColor: colors.white, }}
                    cardElevation={2}
                    cardMaxElevation={2}
                    cornerRadius={5}>
                    <View style={styles.touchableview}>
                        <View style={styles.imageMainView}>
                            <Image source={{ uri: item.image }}
                                style={styles.checkImage}
                            // resizeMode='contain'
                            />
                        </View>
                        <View style={styles.numberMainView}>
                            <View style={{ flexDirection: 'column' }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text  allowFontScaling={false} style={styles.numberText}>{item.full_name.toUpperCase()}</Text>
                                    <Text  allowFontScaling={false} style={styles.othermembersNum}>
                                        {item['group_type'] == 'Group' ? 'Group' : ''}
                                    </Text>
                                </View>
                                {item.mambers.length > 0 ?
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 4 }}>
                                        {item.mambers.length > 0 ?
                                            <Text  allowFontScaling={false} style={styles.othermembers}>{'Other Members : '}
                                                {item.mambers.map(function (item, i) {
                                                    return <Text  allowFontScaling={false} style={styles.othermembersNum}>
                                                        {i == _this.state.otherMembers.length - 1 ? item.member_name : item.member_name + ', '}
                                                    </Text>
                                                })}
                                            </Text> :
                                            null
                                        }
                                    </View> : null}
                                <Text  allowFontScaling={false} style={[styles.dateText, { paddingLeft: DEVICE_WIDTH * .02, color: colors.primaryColor, paddingRight: DEVICE_WIDTH * .02, flex: 2 }]}>{item.mambers.length > 0 ? 'Group' : 'Single'}</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text  allowFontScaling={false} style={styles.dateText}>{date}</Text>
                                    <Text  allowFontScaling={false} style={styles.timeText}>{time}</Text>
                                </View>
                                {offering_a_drink != '' ?
                                    <Text  allowFontScaling={false} style={[styles.dateText,{color: colors.primaryColor,}]}>{offering_a_drink}</Text>
                                    :
                                    null}
                                {request_a_drink != '' ?
                                    <Text  allowFontScaling={false} style={[styles.dateText,{color: colors.primaryColor,}]}>{request_a_drink}</Text>
                                    :
                                    null}


                            </View>
                        </View>
                        {/* <View style={styles.rupeeMainView}>
                            <Text style={styles.rupeeText}>{(index % 2) == 0 ? "Pending" : "Resolved"}</Text>
                        </View> */}
                    </View>
                </CardView>
            </TouchableOpacity >

        );
    }
}
const styles = StyleSheet.create({

    est: {
        fontFamily: fonts.bold,
        color: colors.primaryColor,
        fontSize: (DEVICE_HEIGHT * .02) * .7,
    },

    touchable: {
        marginHorizontal: 10,
    },
    touchableview: {
        paddingHorizontal: 5,
        flex: 1,
        flexDirection: 'row',
        paddingVertical: DEVICE_HEIGHT * .01,
        // height: DEVICE_HEIGHT * .12,
        overflow: 'hidden',
        borderRadius: (DEVICE_HEIGHT * .12) / 30,
        width: DEVICE_WIDTH - 20
    },

    imageMainView: {
        // flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        height: DEVICE_HEIGHT * .12,
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 5
        },
        elevation: 3
    },
    checkImage: {
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 5
        },
        elevation: 3,
        borderColor: colors.primaryColor,
        borderWidth: 2,
        height: DEVICE_HEIGHT * 0.12,
        width: DEVICE_HEIGHT * 0.12,
        borderRadius: (DEVICE_HEIGHT * 0.12) / 2,
        // borderRadius:DEVICE_HEIGHT*.10/2,
        // height: DEVICE_HEIGHT * .10,
        // width: DEVICE_HEIGHT * .10,
        alignSelf: 'center',

    },
    numberMainView: {
        paddingHorizontal: DEVICE_WIDTH * .02,
        alignContent: 'center',
        justifyContent: 'center',
        flex: 8,
        height: DEVICE_HEIGHT * .12,
    },
    numberText: {
        // textTransform: 'capitalize',
        fontFamily: fonts.bold,
        color: colors.black,
        fontSize: (DEVICE_HEIGHT * .03) * .9,
    },
    othermembers: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .02) * .7,
    },
    othermembersNum: {
        fontFamily: fonts.bold,
        color: colors.primaryColor,
        fontSize: (DEVICE_HEIGHT * .02) * .7,
    },
    dateText: {
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .6,
    },
    timeText: {
        marginLeft: DEVICE_WIDTH * .1,
        fontFamily: fonts.bold,
        color: colors.textColor,
        fontSize: (DEVICE_HEIGHT * .03) * .6,
    },
    rupeeMainView: {
        backgroundColor: colors.blueColor,
        flex: 3,
        borderRadius: (DEVICE_HEIGHT * .04) / 2,
        height: (DEVICE_HEIGHT * .05) * .9,
        alignContent: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        // paddingRight: 10
    },
    rupeeText: {
        fontFamily: fonts.bold,
        alignSelf: 'center',
        color: colors.white,
        fontSize: DEVICE_WIDTH * .04,
    }

});
