import React, {
    Component
} from "react";
import {
    StyleSheet,
    View,
    Image, AsyncStorage,
    TextInput, Alert,
    Text, TouchableOpacity,
    BackHandler,
    ScrollView, Platform, PermissionsAndroid,
    ImageBackground, Dimensions, KeyboardAvoidingView, ActivityIndicator

} from "react-native";

import {
    connect
} from "react-redux";
import {
    Actions
} from "react-native-router-flux";
import Background from "../../assets/login_background.png";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import loginLogo from "../../assets/app_logo.png";
import { getStatusBarHeight, getBottomSpace, ifIphoneX } from 'react-native-iphone-x-helper'
import { fonts, colors } from '../../theme'
import { fakeLogin, loginCall } from "../../actions";
import { TextInputLayout } from 'rn-textinputlayout';
import BlueButton from '../common/BlueButton'
import Loading from 'react-native-whc-loading'
import FlashMessage from "react-native-flash-message";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { showMessage, hideMessage } from "react-native-flash-message";
import DeviceInfo from 'react-native-device-info';
import keys from '../../preference'
import firebase from 'react-native-firebase';
import CountryPicker, {
    getAllCountries
} from 'react-native-country-picker-modal'
const NORTH_AMERICA = ['CA', 'MX', 'US']

class LoginScreen extends Component {
    constructor(props) {
        super(props);
        let userLocaleCountryCode = DeviceInfo.isEmulator ? "Simulator" : DeviceInfo.getDeviceCountry();
        // let userLocaleCountryCode = "+91";
        const userCountryData = getAllCountries()
            .filter(country => NORTH_AMERICA.includes(country.cca2))
            .filter(country => country.cca2 === userLocaleCountryCode)
            .pop()
        let callingCode = null

        let cca2 = userLocaleCountryCode
        if (!cca2 || !userCountryData) {
            cca2 = 'IN'
            callingCode = '91'
        } else {
            callingCode = userCountryData.callingCode
        }
        this.state = {
            cca2,
            callingCode: callingCode,
            loading: false,
            number: '',
            password: '',
            ostype: '',
            devicetoken: '',
            imei: '',
            isLocationPermission: false,


        }

        // this.getToken();
        this.checkPermission();
    }
    componentDidMount() {
        Platform.OS == 'ios' ? this.setState({
            ostype: 2
        }) : this.setState({
            ostype: 1
        })
        this.setState({
            imei: DeviceInfo.getUniqueID()
        })


        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }


    componentDidUpdate() {

        // if (this.props.loginResponseData != undefined && this.props.loginResponseData != '') {
        //   //this.props.clearLoginRecord();
        // }
    }


    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);

    }

    componentWillMount() {
        this.requestLocationPermission();
        // try {
        //     AsyncStorage.getItem(keys.fcmToken).then((value) => {
        //         this.setState({
        //             devicetoken: value
        //         })


        //     });
        //     // this.setState({ isLoading: false })
        // } catch (err) {
        //     ref.setState({ devicetoken: '' })
        // }
    }

    async requestLocationPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'Location',
                    'message': 'Allow Clubmate to access to your location '
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.setState({
                    isLocationPermission: true
                })
            } else {
                this.setState({
                    isLocationPermission: false
                })
            }
        } catch (err) {
            console.warn(err)
        }
    }

    async getToken() {
        let fcmToken = await AsyncStorage.getItem(keys.fcmToken);
        console.log('fcmtoke' + fcmToken);
        this.setState({
            devicetoken: fcmToken
        })
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                this.setState({
                    devicetoken: fcmToken
                })
                console.log('fcmtoke' + fcmToken);
                await AsyncStorage.setItem(keys.fcmToken, fcmToken);
            }
        }
    }

    goToRegister = () => {
        Actions.RegisterScreen();
    }
    goToForgotPassword = () => {
        Actions.ForgotPassword();
    }

    onClickRegister = () => {
        Actions.Register();
    }

    onClickBack = () => {
        Actions.pop();
    }
    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            // if (Platform.OS == 'ios') {
            //    await firebase.messaging().ios.registerForRemoteNotifications()
            // }
            this.getToken();
        } else {
            this.requestPermission();
        }
    }


    //2
    async requestPermission() {
        try {
            // await firebase.messaging().requestPermission();
            firebase.messaging().requestPermission()
                .then(() => {
                    if (Platform.OS == 'ios') {
                        firebase.messaging().ios.registerForRemoteNotifications()
                    }

                })
                .catch(error => {

                });
            this.getToken();
            // User has authorised

        } catch (error) {
            // User has rejected permissions
            console.log('permission rejected');
        }
    }

    onPressLogin = () => {
        const { callingCode, number, password, ostype, devicetoken, imei } = this.state;

        if (Platform.OS != 'ios') {
            if (!this.state.isLocationPermission) {
                this.requestLocationPermission();
                return;
            }
        }

        if(devicetoken!=null && devicetoken!='' && devicetoken!=undefined){

        }else{
            Alert.alert('No Device Token')
            return;
        }



        if (callingCode.length == 0) {
            // this.refs.refLogin.showMessage({
            //     message: 'Please select country code',
            //     type: "danger",
            //     position: 'bottom'
            // });
            Alert.alert('Please select country code')
            return;
        }
        if (number.length == 0) {
            Alert.alert('Please enter number')
            // this.refs.refLogin.showMessage({
            //     message: 'Please enter number',
            //     type: "danger",
            //     position: 'bottom'
            // });
            return;
        }

        let loginNumber = '+' + callingCode + number
        this.props.dispatchConfirmUserLogin(loginNumber, password, devicetoken, ostype, imei)

    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.login.isSuccess && !nextProps.login.isLoading) {
            // this.refs.refLogin.showMessage({
            //     message: nextProps.login.loginResponseData,
            //     type: "danger",
            //     position: 'bottom'
            // });
            Alert.alert(nextProps.login.loginResponseData)
        }
    }

    onBackPress() {
        if (Actions.state.index === 1) {
            BackHandler.exitApp();
            return false;
        }
        Actions.pop();
        return true;
    }
    render() {
        const { login: {
            loginResponseData, isLoading, isSuccess, isLoggedIn
        } } = this.props
        return (
            // <ScrollView contentContainerStyle={{flex:1,backgroundColor:'transperent',alignContent:'center'}}>
            <KeyboardAwareScrollView
                enableOnAndroid={true}
                contentContainerStyle={{ flexGrow: 1 }}
                style={{}}
                resetScrollToCoords={{ x: 0, y: 0 }}

                scrollEnabled={true}
            >
                {/* <ScrollView> */}

                <ImageBackground source={Background} style={styles.img_background}>
                    <Loading ref="loading" />
                    <Image resizeMode={'contain'}
                        style={styles.loginLogo}
                        source={loginLogo}>
                    </Image>

                    <View style={styles.login}>
                        <Text  allowFontScaling={false} style={styles.loginText}>
                            LOGIN
                    </Text>
                    </View>

                    <View style={{ marginTop: DEVICE_HEIGHT * .1, flexDirection: 'row', justifyContent: 'center' }}>

                        <View style={{ alignSelf: 'center', marginRight: DEVICE_WIDTH * .02, marginTop: DEVICE_HEIGHT * .02 }}>

                            <CountryPicker
                                // styles={}
                                // touchFlag={{height:0}}
                                // styles={styles.modalContainer}
                                flagType='flat'
                                closeable={true}
                                filterable={true}
                                showCallingCode={true}
                                onChange={value => {
                                    this.setState({ cca2: value.cca2, callingCode: value.callingCode })
                                }}
                                cca2={this.state.cca2}
                                translation="eng"
                            />
                        </View>

                        <TextInputLayout
                            hintColor={colors.gray}
                            errorColor={colors.black}
                            focusColor={colors.primaryColor}
                            style={[styles.mainInputLayout, { width: DEVICE_WIDTH * .75 }]}
                        >
                            <TextInput
                             allowFontScaling={false} 
                                //   editable={!isAuthenticating} 
                                //   selectTextOnFocus={!isAuthenticating}
                                keyboardType='phone-pad'
                                onChangeText={(number) => this.setState({ number: String.prototype.trim.call(number) })}
                                style={styles.inputLayout}
                                placeholder={"Number"}
                                autoCorrect={false}
                                autoCapitalize={"none"}
                                returnKeyType={"done"}
                                placeholderTextColor={colors.gray}
                                underlineColorAndroid="transparent"
                            />
                        </TextInputLayout>

                    </View>


                    <View>

                        <TextInputLayout
                            hintColor={colors.gray}
                            errorColor={colors.black}
                            focusColor={colors.primaryColor}
                            style={styles.mainInputLayout}
                        >
                            <TextInput
                             allowFontScaling={false} 
                                //   editable={!isAuthenticating} 
                                //   selectTextOnFocus={!isAuthenticating}
                                // keyboardType='phone-pad'
                                onChangeText={(password) => this.setState({ password: String.prototype.trim.call(password) })}
                                secureTextEntry={true}
                                style={styles.inputLayout}
                                placeholder={"Password"}
                                autoCorrect={false}
                                autoCapitalize={"none"}
                                returnKeyType={"done"}
                                placeholderTextColor={colors.gray}
                                underlineColorAndroid="transparent"
                            />
                        </TextInputLayout>

                    </View>





                    <View style={{ marginTop: DEVICE_HEIGHT * .06 }}>
                        {isLoading ?
                            <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .06 }}>
                                <ActivityIndicator color={colors.primaryColor} />
                            </View>

                            :
                            <BlueButton onPress={this.onPressLogin} style={{ borderRadius: 5, backgroundColor: colors.primaryColor, width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .06 }} textStyles={{ fontSize: DEVICE_HEIGHT * .03, color: colors.white }}>
                                SUBMIT
                    </BlueButton>}


                    </View>
                    <View style={{ marginTop: DEVICE_HEIGHT * .03, paddingRight: DEVICE_WIDTH * .05, paddingLeft: DEVICE_WIDTH * .05, width: DEVICE_WIDTH, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ alignSelf: 'flex-start' }}>
                            <BlueButton onPress={this.goToForgotPassword} textStyles={{ fontSize: DEVICE_HEIGHT * .03, color: colors.primaryColor }}>
                                Forgot Password
                </BlueButton>
                        </View>
                        <View style={{ alignSelf: 'flex-end' }}>
                            <BlueButton
                                onPress={this.goToRegister}
                                textStyles={{ fontSize: DEVICE_HEIGHT * .03, color: colors.primaryColor }}>
                                New User Register
                </BlueButton>
                        </View>
                    </View>

                    <FlashMessage ref="refLogin" />

                </ImageBackground>
                {/* </ScrollView> */}
            </KeyboardAwareScrollView>
        );

    }
}

const styles = StyleSheet.create({
    mainInputLayout: {
        width: DEVICE_WIDTH * .85,

    },

    inputLayout: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.03,
        height: DEVICE_HEIGHT * 0.06,
        color: colors.black
    },
    loginText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * .05,
        alignSelf: 'center',
        alignContent: 'center'
    },
    login: {
        marginTop: DEVICE_HEIGHT * .03, alignContent: 'center', alignItems: 'center'
    },


    img_background: {

        ...ifIphoneX({
            paddingTop: getStatusBarHeight(),
        }, {
                paddingTop: Platform.OS == 'ios' ? getStatusBarHeight() : 0,

            }),

        flexDirection: 'column',
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT,
        alignItems: 'center', alignContent: 'center',
    },

    loginLogo: {
        marginTop: DEVICE_HEIGHT * .07,
        width: DEVICE_HEIGHT * .3,
        height: DEVICE_HEIGHT * .3,
        justifyContent: 'center',
        alignItems: 'center'
    }



});
// export default LoginScreen;
// const mapStateToProps = ({ login }) => {
//   const { username, password, loginResponseData, isLoading,isLoggedIn } = login;


//   return {
//     username: username,
//     password: password,
//     loginResponseData: loginResponseData,
//     isLoading: isLoading,
//     isLoggedIn:isLoggedIn
//   }
// }
// export default connect(mapStateToProps, { usernameChanged, passwordChanged, showLoading, loginUser, clearLoginRecord })(LoginScreen);




const mapDispatchToProps = {
    dispatchConfirmUserLogin: (number, password, devicetoken, ostype, imei) => loginCall(number, password, devicetoken, ostype, imei),
}

const mapStateToProps = state => ({
    login: state.login
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)