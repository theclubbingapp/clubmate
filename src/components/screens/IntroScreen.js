import React, { Component } from 'react';

import { connect } from 'react-redux';
import AppIntroSlider from 'react-native-app-intro-slider';
import {
	AppRegistry,
	StyleSheet,
	Dimensions,
	View,
	Image,
	Platform,
	AsyncStorage,
	Text
} from 'react-native';

//var isLogin = false;
import { Actions } from 'react-native-router-flux';

import splashImage from "../../assets/app_logo.png";
import introImage from "../../assets/firstIntro.png";
import imagetwo from "../../assets/imagetwo.png";
import imagethree from "../../assets/imagethree.png";
import { colors } from '../../theme';
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import loginLogo from "../../assets/app_logo.png";
// const isLogin = false;
const slides = [
	{
		key: 'somethun',
		title: 'Quick setup, good defaults',
		text: 'ClubMate brings you awesome way to find your club partner online',
		icon: 'ios-images-outline',
		colors: ['#63E2FF', '#B066FE'],
		image:imagetwo
	},
	{
		key: 'somethun1',
		title: 'Super customizable',
		text: 'Choose your club partner based on your likes and dislikes',
		icon: 'ios-options-outline',
		colors: ['#A3A1FF', '#3A3897'],
		image:imagethree
	},
	{
		key: 'somethun2',
		title: 'No need to buy me beer',
		text: 'You can ASK DRINKS or REQUEST DRINKS from your partners',
		icon: 'ios-beer-outline',
		colors: ['#29ABE2', '#4F00BC'],
		image:introImage
	},
];


export default class IntroScreen extends Component {

	componentWillMount() {



	}

	donePress=()=>{
		Actions.reset('LoginScreen');
		
	}

	_renderItem = props => (
		<View
			style={[styles.mainContent, {
				paddingTop: props.topSpacer,
				paddingBottom: props.bottomSpacer,
				width: props.width,
				height: props.height,
			}]}

		>
			<Image resizeMode={'contain'}
				style={styles.loginLogo}
				source={loginLogo}>
			</Image>

			<Image resizeMode={'contain'}
				style={styles.firstIntro}
				source={props.image}>
			</Image>
			<Text  allowFontScaling={false} style={styles.text}>{props.text}</Text>

		</View>
	);

	render() {
		return (
			<AppIntroSlider
				slides={slides}
				renderItem={this._renderItem}
				showSkipButton={true}
				showPrevButton={true}
				onDone={this.donePress}
				onSkip={this.donePress}
				// bottomButton
			/>
		);
	}
}


const styles = StyleSheet.create({
	loginLogo: {
		marginTop: DEVICE_HEIGHT * .05,
		width: DEVICE_HEIGHT * .10,
		height: DEVICE_HEIGHT * .10,
		justifyContent: 'center',
		alignItems: 'center'
	},
	firstIntro: {
		marginTop: DEVICE_HEIGHT * .05,
		width: DEVICE_HEIGHT * .4,
		height: DEVICE_HEIGHT * .4,
		justifyContent: 'center',
		alignItems: 'center'
	},

	mainContent: {
		width: DEVICE_WIDTH, height: DEVICE_HEIGHT,
		alignItems: 'center', alignContent: 'center',
		//   flex: 1,
		//   justifyContent: 'center',
		backgroundColor: colors.primaryColor
	},
	image: {
		width: 320,
		height: 320,
	},
	text: {
		marginTop:DEVICE_HEIGHT*.05,
		color: colors.white,
		backgroundColor: 'transparent',
		textAlign: 'center',
		fontSize:DEVICE_HEIGHT*.02,
		paddingHorizontal: DEVICE_WIDTH*.07,
	},
	title: {
		fontSize: 22,
		color: 'white',
		backgroundColor: 'transparent',
		textAlign: 'center',
		marginBottom: 16,
	}
});