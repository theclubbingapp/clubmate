import React, {
    Component
} from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    Text, TouchableOpacity,
    BackHandler, FlatList,
    ScrollView, Platform,
    ImageBackground, Dimensions, KeyboardAvoidingView, ActivityIndicator

} from "react-native";
import {
    connect
} from "react-redux";
import {
    Actions
} from "react-native-router-flux";
import Background from "../../assets/register_b.png";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import loginLogo from "../../assets/app_logo.png";
import { getStatusBarHeight, getBottomSpace, ifIphoneX } from 'react-native-iphone-x-helper'
import { fonts, colors } from '../../theme'
import { registerCall } from "../../actions";
import { TextInputLayout } from 'rn-textinputlayout';
import BlueButton from '../common/BlueButton'
import ModalDatePicker from "react-native-datepicker-modal";
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { CheckBox } from 'react-native-elements'
import Checkbox from "react-native-custom-checkbox";
import back from "../../assets/back.png";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import FlashMessage from "react-native-flash-message";
import { showMessage, hideMessage } from "react-native-flash-message";
// import console = require("console"); 
// import console = require("console");
import DeviceInfo from 'react-native-device-info';

var radio_props = [
    { label: 'param1', value: 0 },
    { label: 'param2', value: 1 }
];
class RegisterScreenSecond extends Component {
    constructor(props) {
        super(props);
        this.state = {
            intrestedIn: [
                { id: 1, value: "Male", isChecked: false },
                { id: 2, value: "Female", isChecked: false },


            ],

            foodArray: [
                { id: 1, value: "Veg", isChecked: false },
                { id: 2, value: "Non-Veg", isChecked: false },
            ],
            danceArray: [
                { id: 1, value: "Dance", isChecked: false },
                { id: 2, value: "No-Dance", isChecked: false },
            ],




            typesOfDrink: [
                { id: 1, value: "Brandy", isChecked: false },
                { id: 2, value: "Cachaca", isChecked: false },
                { id: 3, value: "Gin", isChecked: false },
                { id: 4, value: "Rum", isChecked: false },
                { id: 5, value: "Schnapps", isChecked: false },
                { id: 6, value: "Tequila", isChecked: false },
                { id: 7, value: "Vodka", isChecked: false },
                { id: 8, value: "Whisky", isChecked: false },
                { id: 9, value: "Beer", isChecked: false }
            ],
            isLoading: false,
            phone: this.props.number,
            password: this.props.password,
            username: this.props.username,
            dateOfBirth: this.props.dateOfBirth,
            gender: this.props.maleFemale,
            // offeringADrink:this.props.offeringADrink,
            food: '',
            dance: '',
            bio: '',
            typeofdrink: [],
            interestedin: [],
            // offeringadrink: this.props.offeringadrink,
            requestingadrink: "0",
            ostype: '',
            devicetoken: '123456',
            imei: ''


        }

        // alert(this.state.dateOfBirth)
    }

    componentWillMount() {
        Platform.OS == 'ios' ? this.setState({
            ostype: 2
        }) : this.setState({
            ostype: 1
        })
        this.setState({
            imei: DeviceInfo.getUniqueID()
        })
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }



    componentDidUpdate() {

        // if (this.props.loginResponseData != undefined && this.props.loginResponseData != '') {
        //   //this.props.clearLoginRecord();
        // }
    }

    OtpScreen = () => {
        const { devicetoken,username, password, phone, imei,ostype,gender, dateOfBirth, foodArray, danceArray, bio,typesOfDrink ,intrestedIn} = this.state;
        this.props.dispatchConfirmUserRegister(devicetoken, username, password,imei, ostype,phone, gender, dateOfBirth, foodArray, danceArray, bio, typesOfDrink, intrestedIn);


        // Actions.OtpScreen();
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);

    }

    componentWillReceiveProps(nextProps) {
        /// close loading ui
        // this.refs.loading.show(false);
        // this.refs.loading.close();
        // alert(JSON.stringify(nextProps))
        console.log("Register main"+JSON.stringify(nextProps));
        if (!nextProps.register.isSuccess && !nextProps.register.isLoading) {
            this.refs.refRegister.showMessage({
                message: nextProps.register.response,
                type: "danger",
                position: 'bottom'
            });
        } else if (nextProps.register.isSuccess && !nextProps.register.isLoading) {
            // Actions.OtpScreen({ number:this.state.number })
            // alert(this.state.number+'before')
            Actions.replace("OtpScreen", { authtoken: nextProps.register.response.authtoken,phone:this.state.phone,username:this.state.username })


        }



    }
    handleIntrestedInElement = (name, isChecked) => {
        let intrestedIn = this.state.intrestedIn
        intrestedIn.forEach(item => {
            if (item.value === name)
                item.isChecked = !isChecked



        })
        this.setState({ intrestedIn: intrestedIn });
        // console.log('intrestedIn'+JSON.stringify(intrestedIn));
    }

    handleDanceElement = (name, isChecked) => {
        let dance = this.state.danceArray
        dance.forEach(item => {
            if (item.value === name) {
                item.isChecked = !isChecked
            } else {
                item.isChecked = false;
            }
            this.setState({
                dance: name == "Dance" ? 1 : 2
            })

        })
        this.setState({ danceArray: dance })
    }

    onClickRegister = () => {
        Actions.Register();
    }

    onClickBack = () => {
        Actions.pop();
    }

    onPressLogin = () => {
        this.props.dispatchConfirmUserLogin("ss")

    }

    handleFoodElement = (name, isChecked) => {
        let food = this.state.foodArray
        food.forEach(item => {
            if (item.value === name) {
                item.isChecked = !isChecked
            } else {
                item.isChecked = false;
            }

            this.setState({
                food: name == "Veg" ? 1 : 2
            })

        })
        this.setState({ foodArray: food })
    }

    onBackPress() {
        if (Actions.state.index === 1) {
            BackHandler.exitApp();
            return false;
        }
        Actions.pop();
        return true;
    }
    handleCheckChieldElement = (name, isChecked) => {
        let typesOfDrink = this.state.typesOfDrink
        typesOfDrink.forEach(typesOfDrink => {
            if (typesOfDrink.value === name)
                typesOfDrink.isChecked = !isChecked


        })
        this.setState({ typesOfDrink: typesOfDrink })
    }

    render() {

        const { register: {
            response, isLoading, isSuccess
        } } = this.props
        return (
            // <ScrollView contentContainerStyle={{flex:1,backgroundColor:'transperent',alignContent:'center'}}>
            <ImageBackground source={Background} style={styles.img_background}>
                <TouchableOpacity onPress={this.onClickBack} style={styles.backTouchable}>
                    <Image resizeMode={'contain'}
                        style={styles.back}
                        source={back}>
                    </Image>
                </TouchableOpacity>
                <Image resizeMode={'contain'}
                    style={styles.loginLogo}
                    source={loginLogo}>
                </Image>
                {/* <ScrollView
                    style={{ flex: 1 }}
                    showsVerticalScrollIndicator={false}
                // enableOnAndroid={true}
                // // contentContainerStyle={{ flexGrow: 1 }}
                // resetScrollToCoords={{ x: 0, y: 0 }}
                // scrollEnabled={false}
                > */}
                <KeyboardAwareScrollView
                enableOnAndroid={true}
                contentContainerStyle={{ flexGrow: 1 }}
                style={{}}
                resetScrollToCoords={{ x: 0, y: 0 }}

                scrollEnabled={true}
            >

                    <View style={{ paddingLeft: DEVICE_WIDTH * .07, marginTop: DEVICE_HEIGHT * .03, alignSelf: 'flex-start', justifyContent: 'flex-start', alignContent: 'flex-start', alignItems: 'flex-start' }}>
                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle, { marginBottom: 10 }]}>
                            Food
                                </Text>
                        <View style={{ width: DEVICE_WIDTH * .85, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <FlatList
                                style={{ height: DEVICE_HEIGHT * 0.06, flexGrow: 0, alignContent: 'center' }}
                                contentContainerStyle={{}}
                                data={this.state.foodArray}
                                scrollEnabled={false}
                            showsHorizontalScrollIndicator={false}
                                renderItem={({ item }) => (
                                    <CheckBox
                                        // handleCheckChieldElement={this.handleCheckChieldElement}
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        title={item.value}
                                        containerStyle={{ width: (DEVICE_WIDTH * .85) / 2, backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                        checked={item.isChecked}
                                        checkedColor={colors.primaryColor}
                                        onPress={() => this.handleFoodElement(item.value, item.isChecked)}
                                        size={DEVICE_HEIGHT * 0.02}
                                        textStyle={[styles.modelDatePickerStringStyle,]}



                                    // {...typesOfDrink} 
                                    />
                                )}
                                extraData={this.state}
                                horizontal={true}
                                //Setting the number of column
                                // numColumns={3}
                                keyExtractor={(item, index) => index}

                            />
                        </View>
                    </View>

                    <View style={{ width: DEVICE_WIDTH * .85, paddingLeft: DEVICE_WIDTH * .07, marginTop: DEVICE_HEIGHT * .03, alignSelf: 'flex-start', justifyContent: 'flex-start', alignContent: 'flex-start', alignItems: 'flex-start' }}>
                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle, { marginBottom: 10 }]}>
                            Dance
                                </Text>
                        <View style={{ width: DEVICE_WIDTH * .85, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <FlatList
                                style={{ height: DEVICE_HEIGHT * 0.06, flexGrow: 0, alignContent: 'center' }}
                                contentContainerStyle={{}}
                                data={this.state.danceArray}
                                scrollEnabled={false}
                            showsHorizontalScrollIndicator={false}
                                renderItem={({ item }) => (
                                    <CheckBox
                                        // handleCheckChieldElement={this.handleCheckChieldElement}
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        title={item.value}
                                        containerStyle={{ width: (DEVICE_WIDTH * .85) / 2, backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                        checked={item.isChecked}
                                        checkedColor={colors.primaryColor}
                                        onPress={() => this.handleDanceElement(item.value, item.isChecked)}
                                        size={DEVICE_HEIGHT * 0.02}
                                        textStyle={[styles.modelDatePickerStringStyle,]}




                                    />
                                )}
                                extraData={this.state}
                                horizontal={true}
                                //Setting the number of column
                                // numColumns={3}
                                keyExtractor={(item, index) => index}

                            />
                        </View>
                    </View>
                    <View style={{ paddingLeft: DEVICE_WIDTH * .07, marginTop: DEVICE_HEIGHT * .03, alignSelf: 'flex-start', justifyContent: 'flex-start', alignContent: 'flex-start', alignItems: 'flex-start' }}>
                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle, { marginBottom: 10 }]}>
                            Type of Drink
                                </Text>

                        <FlatList
                            style={{ flexGrow: 0, alignContent: 'center' }}
                            contentContainerStyle={{}}
                            data={this.state.typesOfDrink}
                            scrollEnabled={false}
                            showsHorizontalScrollIndicator={false}
                            renderItem={({ item }) => (
                                <CheckBox
                                    // handleCheckChieldElement={this.handleCheckChieldElement}
                                    // checkedIcon='dot-circle-o'
                                    // uncheckedIcon='circle-o'
                                    title={item.value}
                                    containerStyle={{backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                    checked={item.isChecked}
                                    checkedColor={colors.primaryColor}
                                    onPress={() => this.handleCheckChieldElement(item.value, item.isChecked)}
                                    size={DEVICE_HEIGHT * 0.02}
                                    textStyle={[styles.modelDatePickerStringStyle,]}



                                // {...typesOfDrink} 
                                />
                            )}
                            extraData={this.state}
                            //Setting the number of column
                            numColumns={3}
                            keyExtractor={(item, index) => index}

                        />

                    </View>




                    <View style={{ paddingLeft: DEVICE_WIDTH * .07, marginTop: DEVICE_HEIGHT * .03, alignSelf: 'flex-start', justifyContent: 'flex-start', alignContent: 'flex-start', alignItems: 'flex-start' }}>
                        <Text  allowFontScaling={false} style={[styles.modelDatePickerStringStyle, { marginBottom: 10 }]}>
                            Intrested In
                                </Text>

                        <FlatList
                            style={{ flexGrow: 0 }}
                            data={this.state.intrestedIn}
                            renderItem={({ item }) => (
                                <CheckBox
                                    handleCheckChieldElement={this.handleCheckChieldElement}
                                    title={item.value}
                                    containerStyle={{ width: (DEVICE_WIDTH * .85) / 2, backgroundColor: 'transparent', borderColor: 'transparent', padding: 0, marginTop: 2 }}
                                    checked={item.isChecked}
                                    checkedColor={colors.primaryColor}
                                    onPress={() => this.handleIntrestedInElement(item.value, item.isChecked)}
                                    size={DEVICE_HEIGHT * 0.02}
                                    textStyle={[styles.modelDatePickerStringStyle,]}
                                />
                            )}
                            extraData={this.state}
                            //Setting the number of column
                            numColumns={3}
                            keyExtractor={(item, index) => index}

                        />

                        {/* <Checkbox
                    checked={true}
                    style={{
                      backgroundColor: colors.white,
                      color: colors.blueColor,
                      borderRadius: 2,
                      borderColor: colors.blueColor
                    }}
                    // onChange={(name, checked) => _myFunction(name, checked)}
                  /> */}

                    </View>

                    <View>
                        <TextInput
                         allowFontScaling={false} 
                            onChangeText={(bio) => this.setState({ bio: String.prototype.trim.call(bio) })}

                            style={styles.textInput}
                            underlineColorAndroid="transparent"
                            autoCapitalize="none"
                            selectionColor={colors.primaryColor}
                            placeholderTextColor={colors.textColor}
                            // keyboardType="email-address"
                            returnKeyType="default"
                            multiline={true}
                            maxLength={60}
                            placeholder="Catchline"
                        />

                    </View>

                    <View style={{ marginTop: DEVICE_HEIGHT * .06,marginBottom: DEVICE_HEIGHT * .06, alignSelf: 'center' }}>

                        {isLoading ?
                            <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .06 }}>
                                <ActivityIndicator color={colors.primaryColor} />
                            </View>

                            :
                            <BlueButton onPress={this.OtpScreen} style={{ backgroundColor: colors.primaryColor, width: DEVICE_WIDTH * .85, height: DEVICE_HEIGHT * .06 }} textStyles={{ fontSize: DEVICE_HEIGHT * .03, color: colors.white }}>
                                SUBMIT
                </BlueButton>}

                    </View>


                {/* </ScrollView> */}
                </KeyboardAwareScrollView>
                <FlashMessage ref="refRegister" />

            </ImageBackground>
        );

    }
}

const styles = StyleSheet.create({

    backTouchable: {
        position: 'absolute',
        alignSelf: 'flex-start',
        left: DEVICE_WIDTH * .05,
        ...ifIphoneX({
            top: getStatusBarHeight() + DEVICE_HEIGHT * .04,
        }, {
                top: Platform.OS == 'ios' ? +DEVICE_HEIGHT * .04 : DEVICE_HEIGHT * .04,

            }),

    },
    back: {
        // marginTop: DEVICE_HEIGHT * .07,
        justifyContent: 'center',
        alignItems: 'center',
        tintColor: colors.white,

        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.03,
        width: DEVICE_WIDTH * 0.03
    },

    textInput: {
        paddingHorizontal: DEVICE_HEIGHT * 0.01,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: colors.gray,
        textAlignVertical: "top",
        marginTop: DEVICE_HEIGHT * 0.04,
        alignSelf: "center",
        width: DEVICE_WIDTH * 0.85,
        height: DEVICE_HEIGHT * 0.15,
        // backgroundColor: colors.inputField,
        fontSize: DEVICE_HEIGHT * 0.03,
        fontFamily: fonts.normal,
        color: colors.inputText
    },

    mainInputLayout: {
        width: DEVICE_WIDTH * .85,

    },
    modelDatePickerStyle: {
        width: DEVICE_WIDTH * .85,
        height: DEVICE_HEIGHT * 0.04,
    }
    ,
    modelDatePickerPlaceholderStyle: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.02,
        color: colors.gray,
    }
    ,
    modelDatePickerStringStyle: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.03,
        color: colors.textColor,
    }
    ,
    inputLayout: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.02,
        height: DEVICE_HEIGHT * 0.06,
        color: colors.black
    },
    loginText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * .05,
        alignSelf: 'center',
        alignContent: 'center'
    },
    login: {
        marginTop: DEVICE_HEIGHT * .1, alignContent: 'center', alignItems: 'center'
    },


    img_background: {

        ...ifIphoneX({
            paddingTop: getStatusBarHeight(),
        }, {
                paddingTop: Platform.OS == 'ios' ? getStatusBarHeight() : 0,

            }),

        flexDirection: 'column', width: DEVICE_WIDTH, height: DEVICE_HEIGHT, alignItems: 'center', alignContent: 'center',
    },

    loginLogo: {
        marginTop: DEVICE_HEIGHT * .02,
        width: DEVICE_HEIGHT * .06,
        height: DEVICE_HEIGHT * .06,
        justifyContent: 'center',
        alignItems: 'center'
    }



});

const mapDispatchToProps = {
    dispatchConfirmUserRegister: ( devicetoken, username, password,imei, ostype,phone, gender, dateOfBirth, foodArray, danceArray, bio, typesOfDrink, intrestedIn) => registerCall(devicetoken, username, password,imei, ostype,phone, gender, dateOfBirth, foodArray, danceArray, bio, typesOfDrink, intrestedIn),
}

const mapStateToProps = state => ({
    register: state.register
})

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreenSecond)