import React, {
    Component
} from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    Text, TouchableOpacity,
    BackHandler, UIManager,
    ScrollView, Platform, ActivityIndicator, LayoutAnimation,
    ImageBackground, Dimensions, KeyboardAvoidingView, Alert

} from "react-native";
import {
    connect
} from "react-redux";
import {
    Actions
} from "react-native-router-flux";
import Background from "../../assets/register_b.png";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import loginLogo from "../../assets/app_logo.png";
import { getStatusBarHeight, getBottomSpace, ifIphoneX } from 'react-native-iphone-x-helper'
import { fonts, colors } from '../../theme'
import { sendOtp, verifyOtp } from "../../actions";
import { TextInputLayout } from 'rn-textinputlayout';
import BlueButton from '../common/BlueButton'
import ModalDatePicker from "react-native-datepicker-modal";
import { CheckBox } from 'react-native-elements'
import back from "../../assets/back.png";
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import CodeInput from 'react-native-confirmation-code-input';
import FlashMessage from "react-native-flash-message";
import { showMessage, hideMessage } from "react-native-flash-message";
import CountDown from 'react-native-countdown-component';
import APIURLCONSTANTS from "../../ApiUrlList";
import axios from 'axios';
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
import ApiUtils from '../../ApiUtils';


import firebaseService from '../../service/firebase'
const FIREBASE_REF_MESSAGES = firebaseService.database().ref('Users')
var radio_props = [
    { label: 'param1', value: 0 },
    { label: 'param2', value: 1 }
];
var CustomLayoutSpring = {
    duration: 400,
    create: {
        type: LayoutAnimation.Types.spring,
        property: LayoutAnimation.Properties.scaleXY,
        springDamping: 0.7,
    },
    update: {
        type: LayoutAnimation.Types.spring,
        springDamping: 0.7,
    },
};
class OtpScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            maleFemale: [
                { label: 'Male', value: 0 },
                { label: 'Female', value: 1 }
            ],
            loading: false,
            loadingResend: false,
            loginUser: '',
            password: '',
            isCustomer: false,
            userType: '',
            authtoken: this.props.authtoken,
            phone: this.props.phone,
            username: this.props.username,
            code: '',
            countdown: 60 * 1,
            showResend: false,
            isSendOtp: true,
            isverifyCode: false

        }
        // alert(this.state.number)
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }
    componentDidMount() {
        // console.log("Otp componentDidMount")
        // this.props.dispatchsendOtp(this.state.number);
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }


    componentDidUpdate() {

        // if (this.props.loginResponseData != undefined && this.props.loginResponseData != '') {
        //   //this.props.clearLoginRecord();
        // }
    }


    resendOtp = () => {




        this.setState({
            isSendOtp: false
        })
        LayoutAnimation.configureNext(CustomLayoutSpring);
        this.setState({
            countdown: 60 * 1,
        })

        const formData = new FormData()
        formData.append('authtoken', this.state.authtoken)
        console.log('RESEND_OTP' + JSON.stringify(formData));
        axios.post(APIURLCONSTANTS.RESEND_OTP, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    loadingResend: false,
                    showResend: false,
                })

                Alert.alert(
                    'Resend OTP',
                    'Otp sent successfully',
                    [
                        { text: 'OK', onPress: () => null },
                    ],
                    { cancelable: true },
                );

            })
            .catch(error => {
                this.setState({
                    loadingResend: false
                })
                if (error.response) {
                    this.refs.refRegister.showMessage({
                        message: error.response.data.msg,
                        type: "danger",
                        position: 'bottom'
                    });
                    // dispatch(verifyOtpFail(error.response.data.msg))
                } else if (error.request) {
                    this.refs.refRegister.showMessage({
                        message: error.request,
                        type: "danger",
                        position: 'bottom'
                    });

                } else {
                    this.refs.refRegister.showMessage({
                        message: error.message,
                        type: "danger",
                        position: 'bottom'
                    });


                }

            });



        // this.props.dispatchsendOtp(this.state.number);
    }


    componentWillUnmount() {
        //Api Hit//
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    _onFulfill = (code) => {
        // alert(code)
        // if(code)
        this.setState({
            code: code
        })


    }


    submit = () => {
        // LayoutAnimation.configureNext(CustomLayoutSpring);
        if (this.state.code.length == 6) {
            let otp = this.state.code
            // this.props.dispatchverifyOtp(this.state.authtoken, otp);
            this.setState({
                loading: true
            })
            const formData = new FormData()
            formData.append('authtoken', this.state.authtoken)
            formData.append('code', otp)
            console.log('verifyotp' + JSON.stringify(formData));
            axios.post(APIURLCONSTANTS.VERIFY_OTP, formData, null)
                .then(ApiUtils.checkStatus)
                .then(res => {
                    this.setState({
                        loading: false
                    })
                    this.addToFirebase();
                    Alert.alert(
                        'OTP Verification',
                        'Otp verified successfully',
                        [
                            {
                                text: 'OK', onPress: () => {




                                    Actions.reset('LoginScreen');
                                }
                            },
                        ],
                        { cancelable: false },
                    );

                })
                .catch(error => {
                    this.setState({
                        loading: false
                    })
                    if (error.response) {
                        this.refs.refRegister.showMessage({
                            message: error.response.data.msg,
                            type: "danger",
                            position: 'bottom'
                        });
                        // dispatch(verifyOtpFail(error.response.data.msg))
                    } else if (error.request) {
                        this.refs.refRegister.showMessage({
                            message: error.request,
                            type: "danger",
                            position: 'bottom'
                        });

                    } else {
                        this.refs.refRegister.showMessage({
                            message: error.message,
                            type: "danger",
                            position: 'bottom'
                        });


                    }

                });

        } else {
            this.refs.refRegister.showMessage({
                message: "Please enter 6 digits OTP",
                type: "danger",
                position: 'bottom'
            });
        }

    }
    onClickBack = () => {
        Actions.pop();
    }

    addToFirebase = () => {
        let createdAt = new Date().getTime()
        let newUSer = {
            phone: this.state.phone,
            name: this.state.username,
            authToken: this.state.authtoken,
            createdAt: createdAt,
        }
        FIREBASE_REF_MESSAGES.push().set(newUSer, (error) => {
            if (error) {
            } else {
            }
        })
    }


    async componentWillReceiveProps(nextProps) {
    }

    onFinishCountdown = () => {
        this.setState({
            showResend: true
        })

    }

    onBackPress() {
        if (Actions.state.index === 1) {
            BackHandler.exitApp();
            return false;
        }
        Actions.pop();
        return true;
    }
    render() {
        return (
            // <ScrollView contentContainerStyle={{flex:1,backgroundColor:'transperent',alignContent:'center'}}>
            <ImageBackground source={Background} style={styles.img_background}>
                <TouchableOpacity onPress={this.onClickBack} style={styles.backTouchable}>
                    <Image resizeMode={'contain'}
                        style={styles.back}
                        source={back}>
                    </Image>
                </TouchableOpacity>
                <Image resizeMode={'contain'}
                    style={styles.loginLogo}
                    source={loginLogo}>
                </Image>


                <Text 
                 allowFontScaling={false} 
                style={{
                    marginTop: DEVICE_HEIGHT * .08,
                    fontFamily: fonts.normal,
                    fontSize: DEVICE_HEIGHT * 0.03,
                    color: colors.primaryColor,
                }}>
                    Please enter OTP
                </Text>

                <View style={{ marginTop: DEVICE_HEIGHT * .1, height: DEVICE_HEIGHT * .2 }}>
                    <CodeInput
                        keyboardType="numeric"
                        activeColor={colors.primaryColor}
                        inactiveColor={colors.black}
                        ref="codeInputRef1"
                        secureTextEntry
                        className={'border-circle'}
                        codeLength={6}
                        space={DEVICE_WIDTH * .05}
                        size={DEVICE_HEIGHT * .06}
                        // inputPosition='left'
                        onFulfill={(code) => this._onFulfill(code)}
                    />

                </View>

                {this.state.showResend ? null :
                    <CountDown
                        until={this.state.countdown}
                        size={DEVICE_HEIGHT * .06}
                        onFinish={() => this.onFinishCountdown()}
                        digitStyle={{ backgroundColor: 'transparent' }}
                        digitTxtStyle={{ color: colors.primaryColor }}
                        timeToShow={['S']}
                        timeLabels={{ m: null, s: null }}
                    />

                }



                {this.state.loadingResend ?
                    <View style={{
                        width: DEVICE_WIDTH * .45,
                        height: DEVICE_HEIGHT * .03
                    }}>
                        <ActivityIndicator
                            size={'small'} color={colors.primaryColor} />
                    </View>

                    :
                    <View>
                        {!this.state.showResend ?

                            <TouchableOpacity
                                disabled={true}
                                // onPress={onPress}
                                style={[{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    // borderRadius: 20,
                                    // paddingLeft:20,
                                    // paddingRight:20,
                                    shadowOpacity: 0.3,
                                    shadowRadius: 3,
                                    shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 5 },
                                    elevation: 3
                                }, {
                                    backgroundColor: 'transparent',
                                    width: DEVICE_WIDTH * .45,
                                    height: DEVICE_HEIGHT * .03
                                },]}>
                                <Text 
                                 allowFontScaling={false} 
                                style={[{
                                    fontFamily: fonts.normal, fontSize: DEVICE_HEIGHT * .03,
                                    color: colors.gray
                                }]}>
                                    Resend OTP
                                </Text>
                            </TouchableOpacity>
                            :
                            <BlueButton
                                onPress={this.resendOtp}
                                style={{
                                    backgroundColor: 'transparent',
                                    width: DEVICE_WIDTH * .45,
                                    height: DEVICE_HEIGHT * .03
                                }}
                                textStyles={{
                                    fontSize: DEVICE_HEIGHT * .02,
                                    color: colors.primaryColor
                                }}>
                                Resend OTP
                            </BlueButton>
                        }


                    </View>
                }
                <View style={{ marginTop: DEVICE_HEIGHT * .06 }}>

                    {this.state.loading ?
                        <View style={{ width: DEVICE_WIDTH * .85, height: DEVICE_HEIGHT * .06 }}>
                            <ActivityIndicator size={'small'} color={colors.primaryColor} />
                        </View>

                        :
                        <BlueButton onPress={this.submit} style={{ backgroundColor: colors.primaryColor, width: DEVICE_WIDTH * .85, height: DEVICE_HEIGHT * .06 }} textStyles={{ fontSize: DEVICE_HEIGHT * .03, color: colors.white }}>
                            SUBMIT
                        </BlueButton>
                    }


                </View>
                <FlashMessage ref="refRegister" />
            </ImageBackground>
        );

    }
}

const styles = StyleSheet.create({
    backTouchable: {
        position: 'absolute',
        alignSelf: 'flex-start',
        left: DEVICE_WIDTH * .05,
        ...ifIphoneX({
            top: getStatusBarHeight() + DEVICE_HEIGHT * .04,
        }, {
                top: Platform.OS == 'ios' ? +DEVICE_HEIGHT * .04 : DEVICE_HEIGHT * .04,

            }),

    },
    back: {
        // marginTop: DEVICE_HEIGHT * .07,
        // width: DEVICE_HEIGHT * .04,
        // height: DEVICE_HEIGHT * .04,
        justifyContent: 'center',
        alignItems: 'center',
        tintColor: colors.white,

        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.03,
        width: DEVICE_WIDTH * 0.03
    },


    mainInputLayout: {
        width: DEVICE_WIDTH * .85,

    },
    modelDatePickerStyle: {
        width: DEVICE_WIDTH * .85,
        height: DEVICE_HEIGHT * 0.04,
    }
    ,
    modelDatePickerPlaceholderStyle: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.02,
        color: colors.gray,
    }
    ,
    modelDatePickerStringStyle: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.02,
        color: colors.textColor,
    }
    ,
    inputLayout: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.02,
        height: DEVICE_HEIGHT * 0.06,
        color: colors.black
    },
    loginText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * .05,
        alignSelf: 'center',
        alignContent: 'center'
    },
    login: {
        marginTop: DEVICE_HEIGHT * .1, alignContent: 'center', alignItems: 'center'
    },


    img_background: {

        ...ifIphoneX({
            paddingTop: getStatusBarHeight(),
        }, {
                paddingTop: Platform.OS == 'ios' ? getStatusBarHeight() : 0,

            }),

        flexDirection: 'column', width: DEVICE_WIDTH, height: DEVICE_HEIGHT, alignItems: 'center', alignContent: 'center',
    },

    loginLogo: {
        marginTop: DEVICE_HEIGHT * .02,
        width: DEVICE_HEIGHT * .06,
        height: DEVICE_HEIGHT * .06,
        justifyContent: 'center',
        alignItems: 'center'
    }



});
const mapDispatchToProps = {
    dispatchsendOtp: number => sendOtp(number),
    dispatchverifyOtp: (authtoken, otp) => verifyOtp(authtoken, otp),
}

const mapStateToProps = state => ({
    register: state.register,
    sendotp: state.sendotp,
    verifyotp: state.verifyotp
})

export default connect(mapStateToProps, mapDispatchToProps)(OtpScreen)