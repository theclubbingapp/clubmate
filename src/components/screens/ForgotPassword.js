import React, {
    Component
} from "react";
import {
    StyleSheet,
    View,
    Image,
    TextInput,
    Text, TouchableOpacity, ActivityIndicator,
    BackHandler,
    ScrollView, Platform, Alert,
    ImageBackground, Dimensions, KeyboardAvoidingView

} from "react-native";
import {
    connect
} from "react-redux";
import {
    Actions
} from "react-native-router-flux";
import Background from "../../assets/login_background.png";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import loginLogo from "../../assets/app_logo.png";
import back from "../../assets/back.png";
import { getStatusBarHeight, getBottomSpace, ifIphoneX } from 'react-native-iphone-x-helper'
import { fonts, colors } from '../../theme'
import { fakeLogin } from "../../actions";
import { TextInputLayout } from 'rn-textinputlayout';
import BlueButton from '../common/BlueButton'
import Dialog, { ScaleAnimation, DialogTitle, DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
import axios from 'axios';
import APIURLCONSTANTS from "../../ApiUrlList";
import ApiUtils from '../../ApiUtils';
import keys from "../../preference";
import FlashMessage from "react-native-flash-message";
import DeviceInfo from 'react-native-device-info';
import CountryPicker, {
    getAllCountries
} from 'react-native-country-picker-modal'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
const NORTH_AMERICA = ['CA', 'MX', 'US']
class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        let userLocaleCountryCode = DeviceInfo.isEmulator ? "Simulator" : DeviceInfo.getDeviceCountry();
        // let userLocaleCountryCode = "+91";
        const userCountryData = getAllCountries()
            .filter(country => NORTH_AMERICA.includes(country.cca2))
            .filter(country => country.cca2 === userLocaleCountryCode)
            .pop()
        let callingCode = null

        let cca2 = userLocaleCountryCode
        if (!cca2 || !userCountryData) {
            cca2 = 'IN'
            callingCode = '91'
        } else {
            callingCode = userCountryData.callingCode
        }
        this.state = {
            cca2,
            callingCode: callingCode,
            loading: false,
            loginUser: '',
            password: '',
            username: '',
            isCustomer: false,
            userType: '',
            title: '',
            message: '',
            visible: false,
            number: '',
            isLoadingForgot: false,

        }
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }


    componentDidUpdate() {

        // if (this.props.loginResponseData != undefined && this.props.loginResponseData != '') {
        //   //this.props.clearLoginRecord();
        // }
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);

    }

    onClickOk = () => {

        this.setState({ visible: false });
        Actions.pop();
    }


    onClickSubmit = () => {

        const { callingCode, number, password, ostype, devicetoken, imei } = this.state;
        if (callingCode.length == 0) {
            this.refs.refForgot.showMessage({
                message: 'Please select country code',
                type: "danger",
                position: 'bottom'
            });
            return;
        }



        if (this.state.number == 0) {
            this.refs.refForgot.showMessage({
                message: "Please enter mobile number",
                type: "danger",
                position: 'bottom'
            });
            return;
        }
        let loginNumber = '+' + callingCode + number


        let formData = new FormData()
        formData.append('phone_number', loginNumber)
        this.setState({
            isLoadingForgot: true
        })
        axios.post(APIURLCONSTANTS.FORGOTPASSWORD, formData, null)
            .then(ApiUtils.checkStatus)
            .then(res => {
                this.setState({
                    isLoadingForgot: false
                })
                let myToken = res['data']['data']['authtoken'];
                console.log(JSON.stringify(res) + '==' + JSON.stringify(myToken))
                Alert.alert(
                    'Forgot Password',
                    'OTP has been sent to your registered mobile number',
                    [
                        {
                            text: 'Ok', onPress: () => {
                                // Actions.pop();
                                Actions.replace("ChangePasswordWithOtp", { myToken: myToken })

                            }
                        },
                    ],
                    { cancelable: false }
                )
            })
            .catch(error => {
                console.log('eoorrrr' + JSON.stringify(error))
                this.setState({
                    isLoadingForgot: false
                })

                if (error.response) {
                    Alert.alert(
                        'Forgot Password',
                        error.response.data.msg,
                        [
                            {
                                text: 'Ok', onPress: () => {
    
                                }
                            },
                        ],
                        { cancelable: false }
                    )
                } else if (error.request) {
                    Alert.alert(
                        'Forgot Password',
                        error.request._response,
                        [
                            {
                                text: 'Ok', onPress: () => {
    
                                }
                            },
                        ],
                        { cancelable: false }
                    )
                } else {
                    // console.log('Error', error.message);
                    Alert.alert(
                        'Forgot Password',
                        error.message,
                        [
                            {
                                text: 'Ok', onPress: () => {
    
                                }
                            },
                        ],
                        { cancelable: false }
                    )
                    
                }


                


            });




    }

    onClickBack = () => {
        Actions.pop();
    }


    onBackPress() {
        if (Actions.state.index === 1) {
            BackHandler.exitApp();
            return false;
        }
        Actions.pop();
        return true;
    }
    render() {
        return (
            // <ScrollView contentContainerStyle={{flex:1,backgroundColor:'transperent',alignContent:'center'}}>
            <KeyboardAwareScrollView
            enableOnAndroid={true}
            contentContainerStyle={{ flexGrow: 1 }}
            style={{}}
            resetScrollToCoords={{ x: 0, y: 0 }}

            scrollEnabled={true}
        >
            <ImageBackground source={Background} style={styles.img_background}>
               
                <TouchableOpacity onPress={this.onClickBack} style={styles.backTouchable}>
                    <Image resizeMode={'contain'}
                        style={styles.back}
                        source={back}>
                    </Image>
                </TouchableOpacity>
                

                <Image resizeMode={'contain'}
                    style={styles.loginLogo}
                    source={loginLogo}>
                </Image>
                <View style={styles.login}>
                    <Text  allowFontScaling={false} style={styles.loginText}>
                        FORGOT PASSWORD
                </Text>
                </View>

                <View style={{ marginTop: DEVICE_HEIGHT * .1, flexDirection: 'row', justifyContent: 'center' }}>

                    <View style={{ alignSelf: 'center', marginRight: DEVICE_WIDTH * .02, marginTop: DEVICE_HEIGHT * .02 }}>

                        <CountryPicker
                            // styles={}
                            // touchFlag={{height:0}}
                            // styles={styles.modalContainer}
                            flagType='flat'
                            closeable={true}
                            filterable={true}
                            showCallingCode={true}
                            onChange={value => {
                                this.setState({ cca2: value.cca2, callingCode: value.callingCode })
                            }}
                            cca2={this.state.cca2}
                            translation="eng"
                        />
                    </View>

                    <TextInputLayout
                        hintColor={colors.gray}
                        errorColor={colors.black}
                        focusColor={colors.primaryColor}
                        style={[styles.mainInputLayout, { width: DEVICE_WIDTH * .75 }]}
                    >
                        <TextInput
                         allowFontScaling={false} 
                            //   editable={!isAuthenticating} 
                            //   selectTextOnFocus={!isAuthenticating}
                            keyboardType='phone-pad'
                            onChangeText={(number) => this.setState({ number: String.prototype.trim.call(number) })}
                            style={styles.inputLayout}
                            placeholder={"Number"}
                            autoCorrect={false}
                            autoCapitalize={"none"}
                            returnKeyType={"done"}
                            placeholderTextColor={colors.gray}
                            underlineColorAndroid="transparent"
                        />
                    </TextInputLayout>

                </View>


                <View style={{ marginTop: DEVICE_HEIGHT * .06 }}>
                    {this.state.isLoadingForgot ?
                        <View style={{ width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .06 }}>
                            <ActivityIndicator color={colors.primaryColor} size={'small'} style={{ alignSelf: 'center' }}>

                            </ActivityIndicator>
                        </View> :
                        <BlueButton onPress={this.onClickSubmit} style={{ borderRadius: 5, backgroundColor: colors.primaryColor, width: DEVICE_WIDTH * .3, height: DEVICE_HEIGHT * .06 }} textStyles={{ fontSize: DEVICE_HEIGHT * .02, color: colors.white }}>
                            SUBMIT
                     </BlueButton>
                    }


                </View>

                <FlashMessage ref="refForgot" />
                
            </ImageBackground>
            </KeyboardAwareScrollView>
        );

    }
}

const styles = StyleSheet.create({

    actionButtonText: {
        color: colors.primaryColor,
        fontSize: DEVICE_HEIGHT * (0.02) * .8,
        fontFamily: fonts.normal
    },


    dialogText: {
        color: colors.black,
        fontSize: DEVICE_HEIGHT * 0.02,
        fontFamily: fonts.normal

    },
    mainInputLayout: {
        width: DEVICE_WIDTH * .85,

    },

    inputLayout: {
        fontFamily: fonts.normal,
        fontSize: DEVICE_HEIGHT * 0.02,
        height: DEVICE_HEIGHT * 0.06,
        color: colors.black
    },
    loginText: {
        fontFamily: fonts.normal,
        color: colors.white,
        fontSize: DEVICE_HEIGHT * .05,
        alignSelf: 'center',
        alignContent: 'center'
    },
    login: {
        marginTop: DEVICE_HEIGHT * .1, alignContent: 'center', alignItems: 'center'
    },


    img_background: {

        ...ifIphoneX({
            paddingTop: getStatusBarHeight(),
        }, {
                paddingTop: Platform.OS == 'ios' ? getStatusBarHeight() : 0,

            }),

        flexDirection: 'column',
        width: DEVICE_WIDTH, height: DEVICE_HEIGHT,
        alignItems: 'center', alignContent: 'center',
    },

    loginLogo: {
        marginTop: DEVICE_HEIGHT * .07,
        width: DEVICE_HEIGHT * .2,
        height: DEVICE_HEIGHT * .2,
        justifyContent: 'center',
        alignItems: 'center'
    }
    ,

    backTouchable: {
        position: 'absolute',
        alignSelf: 'flex-start',
        left: DEVICE_WIDTH * .05,
        ...ifIphoneX({
            top: getStatusBarHeight() + DEVICE_HEIGHT * .03,
        }, {
                top: Platform.OS == 'ios' ? +DEVICE_HEIGHT * .03 : DEVICE_HEIGHT * .03,

            }),

    },
    // back: {
    //     // marginTop: DEVICE_HEIGHT * .07,
    //     width: DEVICE_HEIGHT * .04,
    //     height: DEVICE_HEIGHT * .04,
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     tintColor: colors.white,
    // }
    back: {
        marginTop: DEVICE_HEIGHT * .02,
        justifyContent: 'center',
        alignItems: 'center',
        tintColor: colors.white,

        tintColor: colors.white,
        height: DEVICE_HEIGHT * 0.03,
        width: DEVICE_WIDTH * 0.03
    },



});
// export default LoginScreen;
// const mapStateToProps = ({ login }) => {
//   const { username, password, loginResponseData, isLoading,isLoggedIn } = login;


//   return {
//     username: username,
//     password: password,
//     loginResponseData: loginResponseData,
//     isLoading: isLoading,
//     isLoggedIn:isLoggedIn
//   }
// }
// export default connect(mapStateToProps, { usernameChanged, passwordChanged, showLoading, loginUser, clearLoginRecord })(LoginScreen);


const mapDispatchToProps = {
    dispatchConfirmUserLogin: authCode => fakeLogin(authCode),
}

const mapStateToProps = state => ({
    login: state.login
})

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)